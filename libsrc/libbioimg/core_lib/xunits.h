﻿/*****************************************************************************
 Uints and their conversions

 Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>
 Copyright (c) 2018 Center for Bio-Image Informatics, UCSB <http://bioimage.ucsb.edu>

 History:
   2018-01-03 13:31 - First creation

 Ver : 1
*****************************************************************************/

#ifndef BIM_XUNITS
#define BIM_XUNITS

#include <cmath>
#include <limits>
#include <map>
#include <vector>

namespace bim {

static const std::map<std::string, std::string> length_unit_names = {
    // metric units
    { "yotometers", "ym" },
    { "yotometer", "ym" },
    { "ym", "ym" },
    { "zeptometers", "zm" },
    { "zeptometer", "zm" },
    { "zm", "zm" },
    { "attometers", "am" },
    { "attometer", "am" },
    { "am", "am" },
    { "femtometers", "fm" },
    { "femtometer", "fm" },
    { "fm", "fm" },
    { "picometers", "pm" },
    { "picometer", "pm" },
    { "pm", "pm" },
    { "nanometers", "nm" },
    { "nanometer", "nm" },
    { "nm", "nm" },
    { "microns", "µm" },
    { "micron", "µm" },
    { "um", "µm" },
    { "µm", "µm" },
    { "Âµm", "µm" },
    { "micrometers", "µm" },
    { "micrometer", "µm" },
    { "milimeters", "mm" },
    { "milimeter", "mm" },
    { "mm", "mm" },
    { "centimeters", "cm" },
    { "centimeter", "cm" },
    { "cm", "cm" },
    { "decimeters", "dm" },
    { "decimeter", "dm" },
    { "dm", "dm" },
    { "meters", "m" },
    { "meter", "m" },
    { "m", "m" },
    { "decameters", "dam" },
    { "decameter", "dam" },
    { "dam", "dam" },
    { "hectometers", "hm" },
    { "hectometer", "hm" },
    { "hm", "hm" },
    { "kilometers", "km" },
    { "kilometer", "km" },
    { "km", "km" },
    { "megameters", "Mm" },
    { "megameter", "Mm" },
    { "Mm", "Mm" },
    { "gigameters", "Gm" },
    { "gigameter", "Gm" },
    { "Gm", "Gm" },
    { "terameters", "Tm" },
    { "terameter", "Tm" },
    { "Tm", "Tm" },
    { "petameters", "Pm" },
    { "petameter", "Pm" },
    { "Pm", "Pm" },
    { "exameters", "Em" },
    { "exameter", "Em" },
    { "Em", "Em" },
    { "zettameters", "Zm" },
    { "zettameter", "Zm" },
    { "Zm", "Zm" },
    { "yottameters", "Ym" },
    { "yottameter", "Ym" },
    { "Ym", "Ym" },

    // imperial units
    { "inch", "in" },
    { "inches", "in" },
    { "in", "in" },
    { "foot", "ft" },
    { "feet", "ft" },
    { "ft", "ft" },
    { "yard", "yd" },
    { "yards", "yd" },
    { "yd", "yd" },
    { "mile", "mi" },
    { "miles", "mi" },
    { "mi", "mi" },

    // nautical
    { "fathom", "ftm" },
    { "fathoms", "ftm" },
    { "ftm", "ftm" },
    { "cable", "cb" },
    { "calbles", "cb" },
    { "cb", "cb" },
    { "nautical_mile", "nmi" },
    { "nautical_miles", "nmi" },
    { "nautical mile", "nmi" },
    { "nautical miles", "nmi" },
    { "nmi", "nmi" },
};

static const std::map<std::string, double> length_in_meters = {
    { "ym", 1e-24 },
    { "zm", 1e-21 },
    { "am", 1e-18 },
    { "fm", 1e-15 },
    { "pm", 1e-12 },
    { "nm", 1e-09 },
    { "µm", 1e-06 }, // 0.000001
    { "mm", 1e-03 }, // 0.001
    { "cm", 0.01 },
    { "dm", 0.1 },
    { "m", 1.0 },
    { "dam", 10 },
    { "hm", 100 },
    { "km", 1000 },
    { "Mm", 1e+06 },
    { "Gm", 1e+09 },
    { "Tm", 1e+12 },
    { "Pm", 1e+15 },
    { "Em", 1e+18 },
    { "Zm", 1e+21 },
    { "Ym", 1e+24 },

    // imperial units
    { "in", 0.0254 },
    { "ft", 0.3048 },
    { "yd", 0.9144 },
    { "mi", 1609.34 },

    // nautical
    { "ftm", 1.8288 },
    { "cb", 185.2 },
    { "nmi", 1852.0 },
};

//------------------------------------------------------------------------------
// little math
//------------------------------------------------------------------------------

template<typename T>
inline T from_meters(const double &x, const std::string &units) {
    std::map<std::string, std::string>::const_iterator it = bim::length_unit_names.find(units);
    if (it == bim::length_unit_names.end()) return 0;
    std::string std_unit = (*it).second;

    std::map<std::string, double>::const_iterator it2 = bim::length_in_meters.find(std_unit);
    if (it2 == bim::length_in_meters.end()) return 0;
    double cnv = (*it2).second;

    return (T)(x / cnv);
}

template<typename T>
inline T to_meters(const double &x, const std::string &units) {
    std::map<std::string, std::string>::const_iterator it = bim::length_unit_names.find(units);
    if (it == bim::length_unit_names.end()) return 0;
    std::string std_unit = (*it).second;

    std::map<std::string, double>::const_iterator it2 = bim::length_in_meters.find(std_unit);
    if (it2 == bim::length_in_meters.end()) return 0;
    double cnv = (*it2).second;

    return (T)(x * cnv);
}


} // namespace bim

#endif // BIM_XUNITS
