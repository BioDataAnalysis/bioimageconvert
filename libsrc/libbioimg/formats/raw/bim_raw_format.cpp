/*****************************************************************************
RAW support
Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
Copyright (c) 2021 ViQi Inc

Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
2005-12-01 15:27 - First creation
2007-07-12 21:01 - reading raw
2021-08-25 15:45 - Cellomics C01/DIB support

Ver: 4
*****************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>

#include "bim_raw_format.h"

#include <zlib.h>
#include <znzlib.h>

// Disables Visual Studio 2005 warnings for deprecated code
#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
#pragma warning(disable : 4996)
#endif

using namespace bim;

#define BIM_RAW_FORMAT_RAW 0
#define BIM_RAW_FORMAT_NRRD 1
#define BIM_RAW_FORMAT_MHD 2
#define BIM_RAW_FORMAT_BIMR 3
#define BIM_RAW_FORMAT_BIMZ 4
#define BIM_RAW_FORMAT_CELLOMICS_DIB 5
#define BIM_RAW_FORMAT_CELLOMICS_C01 6


#define BIM_RAW_COMPRESSION_NONE 0
#define BIM_RAW_COMPRESSION_GZIP 1
#define BIM_RAW_COMPRESSION_ZLIB 2

//****************************************************************************
// Bio-Image Micro RAW (BIMR) - can only represent 2D planes
//****************************************************************************

/*
BIMR binary content:
0x00 'BIR1'  - 4 bytes header: BIR1
0x04 WIDTH   - UINT64 image width
0x0C HEIGHT  - UINT64 image height
0x14 SAMPLES - UINT32 number of color channels stored separately
0x18 DEPTH   - UINT16 pixel depth in bits
0x1A FORMAT  - UINT8 pixel format as defined in bim::DataFormat
0x1B ENDIAN  - UINT8 data endianness 0 - little, 1 - big
0x1C PLANES  - UINT32 number of consecutive planes in the same format to represent n-d data
0x20 DATA    - pixel data in defined pixel depth and format stored as channel 0, channel 1, ...
*/

const char BIMR_mgk[4] = { 'B', 'I', 'R', '1' };

#pragma pack(push, 1)
struct BIMR_HEADER {
    uint32 magic = 0;                              // magic: BIR1
    uint64 width = 0;                              // image width
    uint64 height = 0;                             // image height
    uint32 samples = 0;                            // number of color channels
    uint16 depth = 0;                              // number of bits per sample
    uint8 format = bim::DataFormat::FMT_UNDEFINED; // pixel format as defined in bim::DataFormat
    uint8 endian = 0;                              // data endianness 0 - little, 1 - big
    uint32 planes = 0;                             // number of consecutive planes in the same format to represent n-d data
};
#pragma pack(pop)


//****************************************************************************
// Cellomics C01
//****************************************************************************

/*
C01: magic 4 bytes (unknown): 00 00 00 10
magic 1 byte (zlib): 78
zlib compressed DIB stream starting after initial 4 bytes

C01/DIB header (52 bytes) (C01 after decompression):

0000 uint32 magic? 0x28 0x00 0x00 0x00 
0004 uint32 x
0008 uint32 y
0012 uint16 nPlanes
0014 uint16 nBits
0016 uint32 compression
0020 uint32 unknown2
0024 uint32 pixelWidth
0028 uint32 pixelHeight
0032 uint32 colorUsed
0036 uint32 colorImportant
0040 uint32 unknown3
0044 uint32 unknown4
0048 uint32 unknown5

0052 DATA
*/

const char CELLOMICS_C01_magic[5] = {0x00, 0x00, 0x00, 0x10, 0x78};
const char CELLOMICS_DIB_magic[4] = {0x28, 0x00, 0x00, 0x00};

#pragma pack(push, 1)
struct CELLOMICS_HEADER {
    uint32 magic = 0;             // magic: ?
    uint32 width = 0;             // image width
    uint32 height = 0;            // image height
    uint16 samples = 0;           // number of color channels
    uint16 depth = 0;             // number of bits per sample
    uint32 compression = 0;       // 
    uint32 unknown2 = 0;          // 
    uint32 pixelWidthPerMeter = 0;  // pixel width in microns
    uint32 pixelHeightPerMeter = 0; // pixel height in microns
    uint32 colorUsed = 0;           // 
    uint32 colorImportant = 0;    // 
    uint32 unknown3 = 0;          // 
    uint32 unknown4 = 0;          // 
    uint32 unknown5 = 0;          // 

};
#pragma pack(pop)

//****************************************************************************
// I/O supporting compression
//****************************************************************************

BIM_SIZE_T xzread(FormatHandle *fmtHndl, void *buffer, BIM_SIZE_T size, BIM_SIZE_T count) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        return bim::xread(fmtHndl, buffer, size, count);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        return znzread(buffer, size, count, (znzFile)fmtHndl->stream);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_ZLIB) {
        bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
        bim::uint sz = size * count;
        sz = sz < (par->buffer.size() - par->buffer_pos) ? sz : par->buffer.size() - par->buffer_pos - 1;
        memcpy(buffer, &par->buffer[par->buffer_pos], sz);
        par->buffer_pos += sz;
        return sz;
    }
}

BIM_SIZE_T xzwrite(FormatHandle *fmtHndl, void *buffer, BIM_SIZE_T size, BIM_SIZE_T count) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        return bim::xwrite(fmtHndl, buffer, size, count);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        return znzwrite(buffer, size, count, (znzFile)fmtHndl->stream);
    }
    // no ZLIB write supported
}

int xzflush(FormatHandle *fmtHndl) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE)
        return bim::xflush(fmtHndl);

    // no compressed flush supported
    return 0;
}

int xzseek(FormatHandle *fmtHndl, BIM_OFFSET_T offset, int origin) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        int p = bim::xseek(fmtHndl, offset, origin);
        return 0;
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        long p = znzseek((znzFile)fmtHndl->stream, offset, origin);
        //return p != offset;
        return 0;
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_ZLIB) {
        bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
        par->buffer_pos = offset < par->buffer.size() ? offset : par->buffer.size()-1;
        return 0;
    }
}

BIM_SIZE_T xzsize(FormatHandle *fmtHndl) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        return bim::xsize(fmtHndl);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        BIM_SIZE_T p = znztell((znzFile)fmtHndl->stream);
        znzseek((znzFile)fmtHndl->stream, 0, SEEK_END);
        BIM_SIZE_T end_p = znztell((znzFile)fmtHndl->stream);
        znzseek((znzFile)fmtHndl->stream, p, SEEK_SET);
        return end_p;
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_ZLIB) {
        bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
        return par->buffer.size();
    }
}

BIM_OFFSET_T xztell(FormatHandle *fmtHndl) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        return bim::xtell(fmtHndl);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        return znztell((znzFile)fmtHndl->stream);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_ZLIB) {
        bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
        return par->buffer_pos;
    }
}

int _zlib_inflate_buffer(FormatHandle *fmtHndl, std::vector<bim::uint8> &buffer, bim::uint chunk = 16384) {
    z_stream strm;
    std::vector<unsigned char> in(chunk);
    std::vector<unsigned char> out(chunk);

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    int ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    // decompress until deflate stream ends or end of file
    do {
        strm.avail_in = bim::xread(fmtHndl, &in[0], 1, chunk);
        strm.next_in = &in[0];
        if (strm.avail_in == 0)
            break;

        // run inflate() on input until output buffer not full
        do {
            strm.avail_out = chunk;
            strm.next_out = &out[0];
            ret = inflate(&strm, Z_NO_FLUSH);
            //assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
                case Z_NEED_DICT:
                    ret = Z_DATA_ERROR;     // and fall through
                case Z_DATA_ERROR:
                case Z_MEM_ERROR:
                    (void)inflateEnd(&strm);
                    return ret;
            }
            int have = chunk - strm.avail_out;
            
            size_t pos = buffer.size();
            buffer.resize(pos + have);
            memcpy(&buffer[pos], &out[0], have);
        } while (strm.avail_out == 0);
    } while (ret != Z_STREAM_END);

    // clean up and return
    inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

void xzopen(FormatHandle *fmtHndl) {
    bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        bim::xopen(fmtHndl);
    } else if (fmtHndl->io_mode == IO_WRITE) {
        fmtHndl->stream = (void *)znzopen(fmtHndl->fileName, "wb", 1);
    } else if (fmtHndl->io_mode == IO_READ && fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        fmtHndl->stream = (void *)znzopen(fmtHndl->fileName, "rb", 1);
    } else if (fmtHndl->io_mode == IO_READ && fmtHndl->compression == BIM_RAW_COMPRESSION_ZLIB) {
        // we expect C01 files to be pretty small and will use a single buffer for decoding
        // for cases of large files we will need to implement a buffered I/O version
        bim::xopen(fmtHndl);
        if (par->compressed_stream_offset > 0)
            bim::xseek(fmtHndl, par->compressed_stream_offset, SEEK_SET);
        _zlib_inflate_buffer(fmtHndl, par->buffer);
    }
}

int xzclose(FormatHandle *fmtHndl) {
    if (fmtHndl->compression == BIM_RAW_COMPRESSION_NONE) {
        return bim::xclose(fmtHndl);
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_GZIP) {
        int res = Xznzclose((znzFile *)&fmtHndl->stream);
        fmtHndl->stream = NULL;
        return res;
    } else if (fmtHndl->compression == BIM_RAW_COMPRESSION_ZLIB) {
        bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
        if (par) par->buffer.resize(0);
        return bim::xclose(fmtHndl);
    }
}

//****************************************************************************
// Misc
//****************************************************************************

bim::RawParams::RawParams() {
    datastream = 0;
    i = initImageInfo();
    header_offset = 0;
    big_endian = false;
    interleaved = false;
    res.resize(5, 0.0);
}

bim::RawParams::~RawParams() {
    if (datastream) {
        FormatHandle fmtHndl = bim::initFormatHandle();
        fmtHndl.stream = datastream;
        xzclose(&fmtHndl);
    }
}


//****************************************************************************
// format
//****************************************************************************

void mhd2PixelFormat(const std::string &pf, ImageInfo *info) {
    if (pf == "MET_UCHAR") {
        info->depth = 8;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "MET_USHORT") {
        info->depth = 16;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "MET_ULONG") {
        info->depth = 32;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "MET_ULONG_LONG") {
        info->depth = 64;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "MET_CHAR") {
        info->depth = 8;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "MET_SHORT") {
        info->depth = 16;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "MET_LONG") {
        info->depth = 32;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "MET_LONG_LONG") {
        info->depth = 64;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "MET_FLOAT") {
        info->depth = 32;
        info->pixelType = bim::FMT_FLOAT;
    } else if (pf == "MET_DOUBLE") {
        info->depth = 64;
        info->pixelType = bim::FMT_FLOAT;
    }
}

xstring replaceFileName(const xstring &path, const xstring &filename) {
#ifdef BIM_WIN
    xstring div = "\\";
#else
    xstring div = "/";
#endif
    std::vector<xstring> p = path.split(div);
    p[p.size() - 1] = filename;
    return xstring::join(p, div);
}

bool mhdGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    if (fmtHndl->stream == NULL) return false;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    xstring buf(4096, 0); // very large header size, load in one shot
    if (xzseek(fmtHndl, 0, SEEK_SET) != 0) return false;
    xzread(fmtHndl, &buf[0], 1, buf.size() - 1);

    for (const xstring line : buf.replace("\r", "").split("\n")) {
        std::vector<xstring> v = line.split("=");
        if (v.size() > 1)
            par->header.set_value(v[0].strip(" "), v[1].strip(" "));
    }

    if (par->header.get_value("ObjectType") != "Image") return false;

    std::vector<int> dims = par->header.get_value("DimSize").splitInt(" ");
    if (dims.size() < 2) return false;
    info->width = dims[0];
    info->height = dims[1];
    if (dims.size() > 2)
        info->number_z = dims[2];
    else
        info->number_z = 1;
    if (dims.size() > 3)
        info->number_t = dims[3];
    else
        info->number_t = 1;
    info->number_pages = info->number_z * info->number_t;

    par->big_endian = par->header.get_value("ElementByteOrderMSB") == "True";

    par->res = par->header.get_value("ElementSize").splitDouble(" ");
    par->res.resize(5, 0);

    info->samples = par->header.get_value_int("ElementNumberOfChannels", 1);
    mhd2PixelFormat(par->header.get_value("ElementType"), info);
    par->header_offset = par->header.get_value_int("HeaderSize", 0);

    par->datafile = replaceFileName(xstring(fmtHndl->fileName), par->header.get_value("ElementDataFile"));

    return true;
}

void nrrd2PixelFormat(const std::string &pf, ImageInfo *info) {
    if (pf == "uchar" || pf == "unsigned char" || pf == "uint8" || pf == "uint8_t") {
        info->depth = 8;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "ushort" || pf == "unsigned short" || pf == "unsigned short int" || pf == "uint16" || pf == "uint16_t") {
        info->depth = 16;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "uint" || pf == "unsigned int" || pf == "uint32" || pf == "uint32_t") {
        info->depth = 32;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "ulonglong" || pf == "unsigned long long" || pf == "unsigned long long int" || pf == "uint64" || pf == "uint64_t") {
        info->depth = 64;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "signed char" || pf == "int8" || pf == "int8_t") {
        info->depth = 8;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "short" || pf == "short int" || pf == "signed short" || pf == "signed short int" || pf == "int16" || pf == "int16_t") {
        info->depth = 16;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "int" || pf == "signed int" || pf == "int32" || pf == "int32_t") {
        info->depth = 32;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "longlong" || pf == "long long" || pf == "long long int" || pf == "signed long long" || pf == "signed long long int" || pf == "int64" || pf == "int64_t") {
        info->depth = 64;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "float") {
        info->depth = 32;
        info->pixelType = bim::FMT_FLOAT;
    } else if (pf == "double") {
        info->depth = 64;
        info->pixelType = bim::FMT_FLOAT;
    }
}

bool nrrdGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    if (fmtHndl->stream == NULL) return false;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    xstring buf(4096, 0); // very large header size, load in one shot
    if (xzseek(fmtHndl, 0, SEEK_SET) != 0) return false;
    xzread(fmtHndl, &buf[0], 1, buf.size() - 1);

    // find data offset
    par->header_offset = 0;
    for (int i = 0; i < 4096 - 4; ++i) {
        if (memcmp(&buf[i], "\n\n", 2) == 0) {
            par->header_offset = i + 2;
            break;
        }
        if (memcmp(&buf[i], "\r\n\r\n", 4) == 0) {
            par->header_offset = i + 4;
            break;
        }
    }
    buf.resize(par->header_offset, 0);

    for (const xstring line : buf.replace("\r", "").split("\n")) {
        if (line.size() > 0 && line[0] == '#') continue;
        std::vector<xstring> v = line.split(":");
        if (v.size() > 1)
            par->header.set_value(v[0].strip(" "), v[1].strip(" ").lstrip("="));
    }

    std::vector<int> dims = par->header.get_value("sizes").splitInt(" ");
    if (dims.size() < 2) return false;

    // dima: this is a pure hack, there's no true mechanism to identify channels
    if (dims[0] < 10 && dims.size() > 2) {
        par->interleaved = true;
        info->samples = dims[0];
        info->width = dims[1];
        info->height = dims[2];
        if (dims.size() > 3)
            info->number_z = dims[3];
        else
            info->number_z = 1;
        if (dims.size() > 4)
            info->number_t = dims[4];
        else
            info->number_t = 1;
    } else {
        info->samples = 1;
        info->width = dims[0];
        info->height = dims[1];
        if (dims.size() > 2)
            info->number_z = dims[2];
        else
            info->number_z = 1;
        if (dims.size() > 3)
            info->number_t = dims[3];
        else
            info->number_t = 1;
    }
    info->number_pages = info->number_z * info->number_t;

    nrrd2PixelFormat(par->header.get_value("type"), info);

    par->big_endian = par->header.get_value("endian") == "big";

    par->res = par->header.get_value("spacings").splitDouble(" ");
    par->res.resize(5, 0);

    par->units = par->header.get_value("space units").split(" ");

    //par->header_offset = par->header.get_value_int("HeaderSize", 0);

    //par->header.get_value("encoding") == "raw"

    if (par->header.hasKey("data file"))
        par->datafile = replaceFileName(xstring(fmtHndl->fileName), par->header.get_value("data file"));

    return true;
}

bool bimrGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    if (fmtHndl->stream == NULL) return false;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    par->header_offset = sizeof(BIMR_HEADER);
    par->interleaved = false;

    BIMR_HEADER header;
    if (xzseek(fmtHndl, 0, SEEK_SET) != 0) return false;
    if (xzread(fmtHndl, &header, 1, sizeof(BIMR_HEADER)) != sizeof(BIMR_HEADER)) return false;

    info->width = header.width;
    info->height = header.height;
    info->samples = header.samples;
    info->depth = header.depth;
    info->pixelType = (bim::DataFormat)header.format;
    par->big_endian = header.endian;
    info->number_pages = header.planes;

    return true;
}

bool cellomicsGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    if (fmtHndl->stream == NULL) return false;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    par->header_offset = sizeof(CELLOMICS_HEADER);
    par->interleaved = false;

    CELLOMICS_HEADER header;
    if (xzseek(fmtHndl, 0, SEEK_SET) != 0) return false;
    if (xzread(fmtHndl, &header, 1, sizeof(CELLOMICS_HEADER)) != sizeof(CELLOMICS_HEADER)) return false;

    //if (header.compression != 0) return false; // only support internally uncompressed data

    info->width = header.width;
    info->height = header.height;
    info->samples = header.samples;
    info->depth = header.depth;
    //info->pixelType = (bim::DataFormat)header.format;
    info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    par->big_endian = false;
    info->number_pages = 1;

    if (header.pixelWidthPerMeter > 0 && header.pixelHeightPerMeter > 0) {
        par->res.resize(5, 0);
        par->res[0] = 1.0/header.pixelWidthPerMeter * 1000000.0;
        par->res[1] = 1.0/header.pixelHeightPerMeter * 1000000.0;

        par->units.resize(2);
        par->units[0] = "microns";
        par->units[1] = "microns";
    }

    par->header.set_value("pixelWidthPerMeter", header.pixelWidthPerMeter);
    par->header.set_value("pixelHeightPerMeter", header.pixelHeightPerMeter);
    par->header.set_value("compression", header.compression);
    par->header.set_value("colorImportant", header.colorImportant);
    par->header.set_value("colorUsed", header.colorUsed);
    par->header.set_value("unknown1", header.unknown2);
    par->header.set_value("unknown2", header.unknown3);
    par->header.set_value("unknown3", header.unknown4);
    par->header.set_value("unknown4", header.unknown5);

    return true;
}

bool rawGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    *info = initImageInfo();
    info->number_pages = 1;
    info->samples = 1;
    info->imageMode = IM_MULTI;

    if (fmtHndl->subFormat == BIM_RAW_FORMAT_MHD) {
        return mhdGetImageInfo(fmtHndl);
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_NRRD) {
        return nrrdGetImageInfo(fmtHndl);
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_BIMR || fmtHndl->subFormat == BIM_RAW_FORMAT_BIMZ) {
        return bimrGetImageInfo(fmtHndl);
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_CELLOMICS_DIB || fmtHndl->subFormat == BIM_RAW_FORMAT_CELLOMICS_C01) {
        return cellomicsGetImageInfo(fmtHndl);
    }

    par->header_offset = 0;
    par->big_endian = false;
    if (fmtHndl->stream == NULL) return false;

    return true;
}

bool rawWriteImageHeader(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    RawParams *par = (RawParams *)fmtHndl->internalParams;

    ImageBitmap *img = fmtHndl->image;
    if (img == NULL) return false;

    std::string infname = fmtHndl->fileName;
    if (infname.size() <= 1) return false;
    infname += ".info";

    std::string inftext = getImageInfoText(&img->i);


    FILE *stream;
    if ((stream = fopen(infname.c_str(), "wb")) != NULL) {
        fwrite(inftext.c_str(), sizeof(char), inftext.size(), stream);
        fclose(stream);
    }

    return true;
}

//----------------------------------------------------------------------------
// PARAMETERS, INITS
//----------------------------------------------------------------------------

#define BIM_FORMAT_RAW_MAGIC_SIZE 20

int rawValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_FORMAT_RAW_MAGIC_SIZE) return -1;
    unsigned char *mag_num = (unsigned char *)magic;

    if (memcmp(mag_num, "NRRD000", 7) == 0) return BIM_RAW_FORMAT_NRRD;
    if (memcmp(mag_num, BIMR_mgk, 4) == 0) return BIM_RAW_FORMAT_BIMR;
    if (memcmp(mag_num, CELLOMICS_DIB_magic, 4) == 0) return BIM_RAW_FORMAT_CELLOMICS_DIB;
    if (memcmp(mag_num, CELLOMICS_C01_magic, 5) == 0) return BIM_RAW_FORMAT_CELLOMICS_C01;

    if (fileName) {
        xstring filename(fileName);
        filename = filename.toLowerCase();
        if (filename.endsWith(".mhd")) return BIM_RAW_FORMAT_MHD;
        if (filename.endsWith(".bimr.gz")) return BIM_RAW_FORMAT_BIMZ;
        if (filename.endsWith(".bimz")) return BIM_RAW_FORMAT_BIMZ;
    }

    return -1;
}

FormatHandle rawAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void rawCloseImageProc(FormatHandle *fmtHndl);
void rawReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    rawCloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------

void rawCloseImageProc(FormatHandle *fmtHndl) {
    if (!fmtHndl) return;
    xzclose(fmtHndl);
    bim::RawParams *par = (bim::RawParams *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint rawOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) rawCloseImageProc(fmtHndl);
    bim::RawParams *par = new bim::RawParams();
    fmtHndl->internalParams = (void *)par;

    fmtHndl->io_mode = io_mode;
    if (fmtHndl->subFormat == BIM_RAW_FORMAT_BIMZ) {
        fmtHndl->compression = BIM_RAW_COMPRESSION_GZIP;
    }
    if (fmtHndl->subFormat == BIM_RAW_FORMAT_CELLOMICS_C01) {
        fmtHndl->compression = BIM_RAW_COMPRESSION_ZLIB;
        par->compressed_stream_offset = 4;
    }

    xzopen(fmtHndl);
    if (!fmtHndl->stream) {
        rawCloseImageProc(fmtHndl);
        return 1;
    };

    if (io_mode == IO_READ) {
        if (!rawGetImageInfo(fmtHndl)) {
            rawCloseImageProc(fmtHndl);
            return 1;
        };
    }
    return 0;
}

//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint rawGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    return par->i.number_pages;
}


ImageInfo rawGetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    ImageInfo ii = initImageInfo();
    if (fmtHndl == NULL) return ii;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    return par->i;
}

//----------------------------------------------------------------------------
// read
//----------------------------------------------------------------------------

template<typename T>
void read_channel(uint64 W, uint64 H, uint64 samples, uint64 sample, const void *in, void *out) {
    const T *BIM_RESTRICT raw = (const T *)in;
    T *BIM_RESTRICT p = (T *)out;
    raw += sample;
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W*H>BIM_OMP_FOR1)
    for (uint64 x = 0; x < W * H; ++x) {
        T *BIM_RESTRICT pp = p + x;
        const T *BIM_RESTRICT rr = raw + x * samples;
        *pp = *rr;
    } // for x
}

static int read_raw_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    //ImageInfo *info = &par->i;
    ImageBitmap *img = fmtHndl->image;
    ImageInfo *info = &img->i;

    //-------------------------------------------------
    // init the image
    //-------------------------------------------------

    if (!img->bits[0]) {
        if (allocImg(fmtHndl, &par->i, img) != 0) return 1;
    }

    //-------------------------------------------------
    // read image data
    //-------------------------------------------------
    const uint64 plane_size = getImgSizeInBytes(img);
    unsigned int cur_plane = fmtHndl->pageNumber;
    unsigned int header_size = par->header_offset;

    // seek past header size plus number of planes
    if (xzseek(fmtHndl, header_size + plane_size * cur_plane * img->i.samples, SEEK_SET) != 0) return 1;

    if (!par->interleaved || img->i.samples == 1) {
        // read non-interleaved data
        for (uint64 sample = 0; sample < img->i.samples; ++sample) {
            if (xzread(fmtHndl, img->bits[sample], 1, plane_size) != plane_size) return 1;
        }
    } else {
        // read interleaved data
        bim::uint64 buffer_sz = plane_size * img->i.samples;
        std::vector<unsigned char> buffer(buffer_sz);
        unsigned char *buf = (unsigned char *)&buffer[0];
        if (xzread(fmtHndl, buf, 1, buffer_sz) != buffer_sz) return 1;

        for (uint64 s = 0; s < img->i.samples; ++s) {
            // here we only care about the size of pixel and not its format
            if (img->i.depth == 8)
                read_channel<bim::uint8>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
            else if (img->i.depth == 16)
                read_channel<bim::uint16>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
            else if (img->i.depth == 32)
                read_channel<bim::uint32>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
            else if (img->i.depth == 64)
                read_channel<bim::uint64>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
        } // for sample
    }

    // swap endianness
    if (bim::bigendian != (int)par->big_endian)
        for (uint64 sample = 0; sample < img->i.samples; ++sample) {
            if (img->i.depth == 16)
                swapArrayOfShort((uint16 *)img->bits[sample], plane_size / 2);
            else if (img->i.depth == 32)
                swapArrayOfLong((uint32 *)img->bits[sample], plane_size / 4);
            else if (img->i.depth == 64)
                swapArrayOfDouble((float64 *)img->bits[sample], plane_size / 8);
        }

    return 0;
}

static int read_raw_buffer(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageBitmap *img = fmtHndl->image;
    ImageInfo *info = &img->i;

    //-------------------------------------------------
    // init the image
    //-------------------------------------------------

    if (!img->bits[0]) {
        if (allocImg(fmtHndl, &par->i, img) != 0) return 1;
    }

    //-------------------------------------------------
    // read image data
    //-------------------------------------------------
    const uint64 plane_size = getImgSizeInBytes(img);
    unsigned int cur_plane = fmtHndl->pageNumber;

    // seek past header size plus number of planes
    unsigned int buf_offset = plane_size * cur_plane * img->i.samples;

    if (!par->interleaved || img->i.samples == 1) {
        // read non-interleaved data
        for (uint64 sample = 0; sample < img->i.samples; ++sample) {
            if (par->uncompressed.size() < buf_offset + plane_size) return 1;
            memcpy(img->bits[sample], &par->uncompressed[0] + buf_offset, plane_size);
        }
    } else {
        // read interleaved data
        bim::uint64 buffer_sz = plane_size * img->i.samples;
        if (par->uncompressed.size() < buf_offset + buffer_sz) return 1;

        for (uint64 s = 0; s < img->i.samples; ++s) {
            // here we only care about the size of pixel and not its format
            if (img->i.depth == 8)
                read_channel<bim::uint8>(img->i.width, img->i.height, img->i.samples, s, &par->uncompressed[0] + buf_offset, img->bits[s]);
            else if (img->i.depth == 16)
                read_channel<bim::uint16>(img->i.width, img->i.height, img->i.samples, s, &par->uncompressed[0] + buf_offset, img->bits[s]);
            else if (img->i.depth == 32)
                read_channel<bim::uint32>(img->i.width, img->i.height, img->i.samples, s, &par->uncompressed[0] + buf_offset, img->bits[s]);
            else if (img->i.depth == 64)
                read_channel<bim::uint64>(img->i.width, img->i.height, img->i.samples, s, &par->uncompressed[0] + buf_offset, img->bits[s]);
        } // for sample
    }

    // swap endianness
    if (bim::bigendian != (int)par->big_endian)
        for (uint64 sample = 0; sample < img->i.samples; ++sample) {
            if (img->i.depth == 16)
                swapArrayOfShort((uint16 *)img->bits[sample], plane_size / 2);
            else if (img->i.depth == 32)
                swapArrayOfLong((uint32 *)img->bits[sample], plane_size / 4);
            else if (img->i.depth == 64)
                swapArrayOfDouble((float64 *)img->bits[sample], plane_size / 8);
        }

    return 0;
}

static int read_mhd_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    if (!par->datastream) {
        FormatHandle h = bim::initFormatHandle();
        h.io_mode = IO_READ;
        h.fileName = (bim::Filename)par->datafile.c_str();
        xzopen(&h);
        par->datastream = h.stream;
    }
    void *old_stream = fmtHndl->stream;
    fmtHndl->stream = par->datastream;
    int r = read_raw_image(fmtHndl);
    fmtHndl->stream = old_stream;
    return r;
}

// fix buffer deflate bug in zlib
int zlib_uncompress_fix(Bytef *dest, uLongf *destLen, const Bytef *source, uLong sourceLen) {
    z_stream stream;
    int err;

    stream.next_in = (z_const Bytef *)source;
    stream.avail_in = (uInt)sourceLen;
    // Check for source > 64K on 16-bit machine:
    if ((uLong)stream.avail_in != sourceLen) return Z_BUF_ERROR;

    stream.next_out = dest;
    stream.avail_out = (uInt)*destLen;
    if ((uLong)stream.avail_out != *destLen) return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;

    err = inflateInit(&stream);
    if (err != Z_OK) return err;

    err = inflateReset2(&stream, 31);
    if (err != Z_OK) {
        inflateEnd(&stream);
        return err;
    }

    err = inflate(&stream, Z_FINISH);
    if (err != Z_STREAM_END) {
        inflateEnd(&stream);
        if (err == Z_NEED_DICT || (err == Z_BUF_ERROR && stream.avail_in == 0))
            return Z_DATA_ERROR;
        return err;
    }
    *destLen = stream.total_out;

    err = inflateEnd(&stream);
    return err;
}

static int read_nrrd_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    if (par->datafile.size() > 0 && !par->datastream) {
        FormatHandle h = bim::initFormatHandle();
        h.io_mode = IO_READ;
        h.fileName = (bim::Filename)par->datafile.c_str();
        xzopen(&h);
        par->datastream = h.stream;
    }
    void *old_stream = 0;
    if (par->datastream) {
        old_stream = fmtHndl->stream;
        fmtHndl->stream = par->datastream;
    }

    int r = 0;
    if (par->header.get_value("encoding") == "raw") {
        r = read_raw_image(fmtHndl);
    } else if (par->header.get_value("encoding") == "gzip" || par->header.get_value("encoding") == "gz") {
        // decompress the rest of the file in one shot
        if (par->uncompressed.size() < 1) {
            par->uncompressed.resize(info->width * info->height * (info->depth / 8) * info->samples * info->number_pages, 0);

            unsigned int sz = xzsize(fmtHndl) - par->header_offset;
            std::vector<char> in(sz);
            if (xzseek(fmtHndl, par->header_offset, SEEK_SET) != 0) return 1;
            xzread(fmtHndl, &in[0], 1, sz);
            uLongf outsz = par->uncompressed.size();
            if (zlib_uncompress_fix((Bytef *)&par->uncompressed[0], &outsz, (const Bytef *)&in[0], sz) != Z_OK)
                r = 1;
        }
        if (r == 0)
            r = read_raw_buffer(fmtHndl);
    }

    if (old_stream) {
        fmtHndl->stream = old_stream;
    }
    return r;
}

bim::uint rawReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;
    fmtHndl->pageNumber = page;

    if (fmtHndl->subFormat == BIM_RAW_FORMAT_MHD) {
        return read_mhd_image(fmtHndl);
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_NRRD) {
        return read_nrrd_image(fmtHndl);
    }

    return read_raw_image(fmtHndl);
}


//----------------------------------------------------------------------------
// metadata
//----------------------------------------------------------------------------

bim::uint cellomics_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    // parse filename for well and site info
    bim::xstring filename = fmtHndl->fileName;
    filename = filename.toLowerCase();

    filename = filename.split(".c01")[0];
    filename = filename.split(".dib")[0];

    bim::xstring poss = filename.right("_");
    bim::xstring plate = fspath_get_filename(filename.replace(bim::xstring("_") + poss, ""));
    bim::xstring well = bim::xstring(poss.substr(0, 3)).toUpperCase();
    int site = bim::xstring(poss.substr(4, 2)).toInt();
    //channel
    int wi = bim::uint8(poss.substr(0, 1)[0]) - 96;
    int wj = bim::xstring(poss.substr(1, 2)).toInt();
    
    hash->set_value(bim::DOCUMENT_PLATE, plate);
    hash->set_value(bim::DOCUMENT_WELL_NAME, well);
    hash->set_value(bim::DOCUMENT_WELL_SITE, site);
    hash->set_value(bim::DOCUMENT_WELL_ROW, wi);
    hash->set_value(bim::DOCUMENT_WELL_COL, wj);
    hash->set_value(bim::DOCUMENT_WELL_IJ, bim::xstring::xprintf("%d,%d", wi, wj));
}

bim::uint raw_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    //----------------------------------------------------------------------------
    // Resolution
    //----------------------------------------------------------------------------
    hash->set_value(bim::PIXEL_RESOLUTION_X, par->res[0]);
    hash->set_value(bim::PIXEL_RESOLUTION_Y, par->res[1]);
    hash->set_value(bim::PIXEL_RESOLUTION_Z, par->res[2]);
    hash->set_value(bim::PIXEL_RESOLUTION_T, par->res[3]);

    if (par->units.size() > 0) hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, par->units[0]);
    if (par->units.size() > 1) hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, par->units[1]);
    if (par->units.size() > 2) hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, par->units[2]);
    if (par->units.size() > 3) hash->set_value(bim::PIXEL_RESOLUTION_UNIT_T, par->units[3]);

    //-------------------------------------------
    // channel names
    //-------------------------------------------
    for (unsigned int i = 0; i < info->samples; ++i) {
        hash->set_value(xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i), xstring::xprintf("Channel %d", i));
    }

    xstring root("RAW/");
    if (fmtHndl->subFormat == BIM_RAW_FORMAT_MHD) {
        root = "MHD/";
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_NRRD) {
        root = "NRRD/";
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_BIMR || fmtHndl->subFormat == BIM_RAW_FORMAT_BIMZ) {
        root = "BIMR/";
    } else if (fmtHndl->subFormat == BIM_RAW_FORMAT_CELLOMICS_DIB || fmtHndl->subFormat == BIM_RAW_FORMAT_CELLOMICS_C01) {
        root = "Cellomics/";
        cellomics_append_metadata(fmtHndl, hash);
    }

    bim::TagMap::const_iterator it;
    for (it = par->header.begin(); it != par->header.end(); ++it) {
        hash->set_value(root + (*it).first, (*it).second.as_string());
    }

    return 0;
}


//----------------------------------------------------------------------------
// write ITK MediaHeader
//----------------------------------------------------------------------------

std::string mhdPixelFormat(ImageInfo *info) {
    if (info->depth == 8 && info->pixelType == bim::FMT_UNSIGNED) return "MET_UCHAR";
    if (info->depth == 16 && info->pixelType == bim::FMT_UNSIGNED) return "MET_USHORT";
    if (info->depth == 32 && info->pixelType == bim::FMT_UNSIGNED) return "MET_ULONG";
    if (info->depth == 64 && info->pixelType == bim::FMT_UNSIGNED) return "MET_ULONG_LONG";
    if (info->depth == 8 && info->pixelType == bim::FMT_SIGNED) return "MET_CHAR";
    if (info->depth == 16 && info->pixelType == bim::FMT_SIGNED) return "MET_SHORT";
    if (info->depth == 32 && info->pixelType == bim::FMT_SIGNED) return "MET_LONG";
    if (info->depth == 64 && info->pixelType == bim::FMT_SIGNED) return "MET_LONG_LONG";
    if (info->depth == 32 && info->pixelType == bim::FMT_FLOAT) return "MET_FLOAT";
    if (info->depth == 64 && info->pixelType == bim::FMT_FLOAT) return "MET_DOUBLE";
    return "";
}

int mhdWriteImageHeader(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageBitmap *img = fmtHndl->image;
    if (!img) return 1;
    ImageInfo *info = &img->i;

    bim::xstring ofname = fmtHndl->fileName;
    ofname = ofname.replace(".mhd", ".raw");

    bim::xstring header = "ObjectType = Image\n";
    int nd = 2;
    if (info->number_z > 1) ++nd;
    if (info->number_t > 1) ++nd;
    header += xstring::xprintf("NDims = %d\n", nd);
    header += xstring::xprintf("DimSize = %d %d %d %d\n", info->width, info->height, info->number_z, info->number_t);
    if (bim::bigendian)
        header += "ElementByteOrderMSB = True\n";
    else
        header += "ElementByteOrderMSB = False\n";

    // need to read meta
    //header += xstring::xprintf("ElementSize = %d %d %d %d\n", info->xRes, info->yRes, info->number_z, info->number_t);

    header += "ElementType = " + mhdPixelFormat(info) + "\n";
    header += xstring::xprintf("ElementNumberOfChannels = %d\n", info->samples);

    // very last element
    header += "HeaderSize = 0\n";
    header += xstring::xprintf("ElementDataFile = %s\n", ofname.c_str());

    if (xzwrite(fmtHndl, (void *)header.c_str(), 1, header.size()) != header.size()) return 1;
    xzflush(fmtHndl);

    return 0;
}

//----------------------------------------------------------------------------
// write NRRD Header
//----------------------------------------------------------------------------

std::string nrrdPixelFormat(const ImageInfo *info) {
    if (info->depth == 8 && info->pixelType == bim::FMT_UNSIGNED) return "uint8";
    if (info->depth == 16 && info->pixelType == bim::FMT_UNSIGNED) return "uint16";
    if (info->depth == 32 && info->pixelType == bim::FMT_UNSIGNED) return "uint32";
    if (info->depth == 64 && info->pixelType == bim::FMT_UNSIGNED) return "uint64";
    if (info->depth == 8 && info->pixelType == bim::FMT_SIGNED) return "int8";
    if (info->depth == 16 && info->pixelType == bim::FMT_SIGNED) return "int16";
    if (info->depth == 32 && info->pixelType == bim::FMT_SIGNED) return "int32";
    if (info->depth == 64 && info->pixelType == bim::FMT_SIGNED) return "int64";
    if (info->depth == 32 && info->pixelType == bim::FMT_FLOAT) return "float";
    if (info->depth == 64 && info->pixelType == bim::FMT_FLOAT) return "double";
    return "";
}

int nrrdWriteImageHeader(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageBitmap *img = fmtHndl->image;
    if (!img) return 1;
    ImageInfo *info = &img->i;

    bim::xstring header = "NRRD0004\n";
    int nd = 2;
    if (info->samples > 1) ++nd;
    if (info->number_z > 1) ++nd;
    if (info->number_t > 1) ++nd;
    //info->samples
    header += xstring::xprintf("dimension: %d\n", nd);
    if (info->samples > 1)
        header += xstring::xprintf("sizes: %d %d %d %d %d\n", info->samples, info->width, info->height, info->number_z, info->number_t);
    else
        header += xstring::xprintf("sizes: %d %d %d %d\n", info->width, info->height, info->number_z, info->number_t);
    header += "type: " + nrrdPixelFormat(info) + "\n";
    if (bim::bigendian) header += "endian: big\n";
    header += "encoding: raw\n";

    //header += xstring::xprintf("spacings: %d %d %d %d\n", info->xRes, info->yRes, info->number_z, info->number_t);
    //header += xstring::xprintf("ElementNumberOfChannels = %d\n", info->samples);

    // very last element - empty line
    header += "\n";

    if (xzwrite(fmtHndl, (void *)header.c_str(), 1, header.size()) != header.size()) return 1;
    xzflush(fmtHndl);

    return 0;
}

//----------------------------------------------------------------------------
// write BIMR Header
//----------------------------------------------------------------------------

int bimrWriteImageHeader(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageBitmap *img = fmtHndl->image;
    if (!img) return 1;
    ImageInfo *info = &img->i;

    par->header_offset = sizeof(BIMR_HEADER);
    par->big_endian = bim::bigendian;
    par->interleaved = false;

    BIMR_HEADER header;
    header.magic = 0;
    memcpy((void *)&header.magic, BIMR_mgk, 4);
    header.width = info->width;
    header.height = info->height;
    header.samples = info->samples;
    header.depth = info->depth;
    header.format = info->pixelType;
    header.endian = bim::bigendian;
    header.planes = fmtHndl->numberOfPages;

    if (xzwrite(fmtHndl, (void *)&header, 1, sizeof(header)) != sizeof(header)) return 1;
    xzflush(fmtHndl);

    return 0;
}


//----------------------------------------------------------------------------
// write raw
//----------------------------------------------------------------------------

template<typename T>
void write_channel(bim::uint64 W, bim::uint64 H, int samples, int sample, const void *in, void *out) {
    const T *BIM_RESTRICT raw = (const T *)in;
    T *BIM_RESTRICT p = (T *)out;
    raw += sample;
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W*H>BIM_OMP_FOR1)
    for (uint64 x = 0; x < W * H; ++x) {
        T *BIM_RESTRICT pp = p + x;
        const T *BIM_RESTRICT rr = raw + x * samples;
        *pp = *rr;
    } // for x
}

static int write_raw_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    RawParams *par = (RawParams *)fmtHndl->internalParams;
    ImageBitmap *img = fmtHndl->image;

    //-------------------------------------------------
    // write image data
    //-------------------------------------------------
    const uint64 plane_size = getImgSizeInBytes(img);

    if (!par->interleaved || img->i.samples == 1) {
        std::vector<unsigned char> buffer(plane_size);
        unsigned char *buf = (unsigned char *)&buffer[0];

        for (uint64 sample = 0; sample < img->i.samples; ++sample) {
            memcpy(buf, img->bits[sample], plane_size);

            if (bim::bigendian != (int)par->big_endian) {
                if (img->i.depth == 16)
                    swapArrayOfShort((uint16 *)buf, plane_size / 2);
                else if (img->i.depth == 32)
                    swapArrayOfLong((uint32 *)buf, plane_size / 4);
                else if (img->i.depth == 64)
                    swapArrayOfDouble((float64 *)buf, plane_size / 8);
            }

            if (xzwrite(fmtHndl, buf, 1, plane_size) != plane_size) return 1;
        }
    } else {
        bim::uint64 buffer_sz = plane_size * img->i.samples;
        std::vector<unsigned char> buffer(buffer_sz);
        unsigned char *buf = (unsigned char *)&buffer[0];

        for (uint64 s = 0; s < img->i.samples; ++s) {
            // here we only care about the size of pixel and not its format
            if (img->i.depth == 8)
                write_channel<bim::uint8>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
            else if (img->i.depth == 16)
                write_channel<bim::uint16>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
            else if (img->i.depth == 32)
                write_channel<bim::uint32>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);
            else if (img->i.depth == 64)
                write_channel<bim::uint64>(img->i.width, img->i.height, img->i.samples, s, buf, img->bits[s]);

            if (bim::bigendian != (int)par->big_endian) {
                if (img->i.depth == 16)
                    swapArrayOfShort((uint16 *)buf, buffer_sz / 2);
                else if (img->i.depth == 32)
                    swapArrayOfLong((uint32 *)buf, buffer_sz / 4);
                else if (img->i.depth == 64)
                    swapArrayOfDouble((float64 *)buf, buffer_sz / 8);
            }

            if (xzwrite(fmtHndl, buf, 1, buffer_sz) != buffer_sz) return 1;
        } // for sample
    }

    xzflush(fmtHndl);
    return 0;
}

bim::uint rawWriteImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;

    if (fmtHndl->subFormat == BIM_RAW_FORMAT_MHD && fmtHndl->pageNumber == 0)
        return mhdWriteImageHeader(fmtHndl);

    if (fmtHndl->subFormat == BIM_RAW_FORMAT_NRRD && fmtHndl->pageNumber == 0)
        nrrdWriteImageHeader(fmtHndl);
    else if ((fmtHndl->subFormat == BIM_RAW_FORMAT_BIMR || fmtHndl->subFormat == BIM_RAW_FORMAT_BIMZ) && fmtHndl->pageNumber == 0)
        bimrWriteImageHeader(fmtHndl);

    return write_raw_image(fmtHndl);
}


//****************************************************************************
// EXPORTED
//****************************************************************************

#define BIM_RAW_NUM_FORMATS 7

FormatItem rawItems[BIM_RAW_NUM_FORMATS] = {{ 
    "RAW",               // short name, no spaces
    "RAW: image pixels", // Long format name
    "raw",               // pipe "|" separated supported extension list
    1,                   //canRead;      // 0 - NO, 1 - YES
    1,                   //canWrite;     // 0 - NO, 1 - YES
    0,                   //canReadMeta;  // 0 - NO, 1 - YES
    0,                   //canWriteMeta; // 0 - NO, 1 - YES
    1,                   //canWriteMultiPage;   // 0 - NO, 1 - YES
    //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
    { 0, 0, 0, 0, 0, 0, 0, 0 } 
}, { 
    "NRRD",      // short name, no spaces
    "RAW: NRRD", // Long format name
    "nrrd",      // pipe "|" separated supported extension list
    1,           //canRead;      // 0 - NO, 1 - YES
    1,           //canWrite;     // 0 - NO, 1 - YES
    1,           //canReadMeta;  // 0 - NO, 1 - YES
    0,           //canWriteMeta; // 0 - NO, 1 - YES
    1,           //canWriteMultiPage;   // 0 - NO, 1 - YES
    //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
    { 0, 0, 0, 0, 0, 0, 0, 0 } 
}, { 
    "MHD",      // short name, no spaces
    "RAW: MHD", // Long format name
    "mhd",      // pipe "|" separated supported extension list
    1,          //canRead;      // 0 - NO, 1 - YES
    1,          //canWrite;     // 0 - NO, 1 - YES
    1,          //canReadMeta;  // 0 - NO, 1 - YES
    0,          //canWriteMeta; // 0 - NO, 1 - YES
    1,          //canWriteMultiPage;   // 0 - NO, 1 - YES
    //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
    { 0, 0, 0, 0, 0, 0, 0, 0 } 
}, { 
    "BIMR",           // short name, no spaces
    "ViQi Micro RAW", // Long format name
    "bimr",           // pipe "|" separated supported extension list
    1,                //canRead;      // 0 - NO, 1 - YES
    1,                //canWrite;     // 0 - NO, 1 - YES
    1,                //canReadMeta;  // 0 - NO, 1 - YES
    0,                //canWriteMeta; // 0 - NO, 1 - YES
    1,                //canWriteMultiPage;   // 0 - NO, 1 - YES
    //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
    { 0, 0, 0, 0, 0, 0, 0, 0 } 
}, { 
    "BIMR.GZ",                   // short name, no spaces
    "ViQi Micro RAW Compressed", // Long format name
    "bimr.gz|bimrz|bimz",        // pipe "|" separated supported extension list
    1,                           //canRead;      // 0 - NO, 1 - YES
    1,                           //canWrite;     // 0 - NO, 1 - YES
    1,                           //canReadMeta;  // 0 - NO, 1 - YES
    0,                           //canWriteMeta; // 0 - NO, 1 - YES
    1,                           //canWriteMultiPage;   // 0 - NO, 1 - YES
    //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
    { 0, 0, 0, 0, 0, 0, 0, 0 } 
}, {
    "CELLOMICS.DIB",                   // short name, no spaces
    "Cellomics DIB", // Long format name
    "dib",        // pipe "|" separated supported extension list
    1,                           //canRead;      // 0 - NO, 1 - YES
    0,                           //canWrite;     // 0 - NO, 1 - YES
    1,                           //canReadMeta;  // 0 - NO, 1 - YES
    0,                           //canWriteMeta; // 0 - NO, 1 - YES
    0,                           //canWriteMultiPage;   // 0 - NO, 1 - YES
    {0, 0, 0, 0, 0, 0, 0, 0} //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
}, {
    "CELLOMICS.C01",                   // short name, no spaces
    "Cellomics C01", // Long format name
    "c01",        // pipe "|" separated supported extension list
    1,                           //canRead;      // 0 - NO, 1 - YES
    0,                           //canWrite;     // 0 - NO, 1 - YES
    1,                           //canReadMeta;  // 0 - NO, 1 - YES
    0,                           //canWriteMeta; // 0 - NO, 1 - YES
    0,                           //canWriteMultiPage;   // 0 - NO, 1 - YES
    {0, 0, 0, 0, 0, 0, 0, 0} //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
}};

FormatHeader rawHeader = {
    sizeof(FormatHeader),
    "5.0.0",
    "RAW CODEC",
    "RAW CODEC",

    BIM_FORMAT_RAW_MAGIC_SIZE,            // 0 or more, specify number of bytes needed to identify the file
    { 1, BIM_RAW_NUM_FORMATS, rawItems }, // dimJpegSupported,

    rawValidateFormatProc,
    // begin
    rawAquireFormatProc, //AquireFormatProc
    // end
    rawReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    rawOpenImageProc,  //OpenImageProc
    rawCloseImageProc, //CloseImageProc

    // info
    rawGetNumPagesProc,  //GetNumPagesProc
    rawGetImageInfoProc, //GetImageInfoProc

    // read/write
    rawReadImageProc,  //ReadImageProc
    rawWriteImageProc, //WriteImageProc
    NULL,              //ReadImageTileProc
    NULL,              //WriteImageTileProc
    NULL,              //ReadImageLineProc
    NULL,              //WriteImageLineProc
    NULL,              //ReadImageThumbProc
    NULL,              //WriteImageThumbProc
    NULL,              //dimJpegReadImagePreviewProc, //ReadImagePreviewProc
    NULL,              //ReadImageRegionProc
    NULL,              //WriteImageRegionProc

    // meta data
    NULL, //ReadMetaDataAsTextProc

    raw_append_metadata,
    NULL,
    NULL,
    ""
};

extern "C" {

FormatHeader *rawGetFormatHeader(void) {
    return &rawHeader;
}

} // extern C
