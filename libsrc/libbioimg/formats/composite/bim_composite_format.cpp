/*****************************************************************************
Composite file support - some simple header describing multiple image files
Copyright (c) 2020, ViQi Inc

Author: Dima Fedorov <mailto:dima@viqi.org> <http://viqi.org>

History:
2020-03-25 15:48:00 - First creation

Ver : 1
*****************************************************************************/

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <limits>
#include <fstream>
#include <set>
#include <string>

#include <bim_metatags.h>
#include <meta_format_manager.h>
#include <tag_map.h>
#include <xstring.h>

#include "bim_composite_format.h"

#include <pugixml.hpp>

// Disables Visual Studio 2005 warnings for deprecated code
#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
#pragma warning(disable : 4996)
#endif

using namespace bim;

#define BIM_FORMAT_COMPOSITE_MAGIC_SIZE 0

#define BIM_COMPOSITE_FORMAT_VQCX 0       // ViQi XML
#define BIM_COMPOSITE_FORMAT_PVSCAN 1     // PrairieView Scan
#define BIM_COMPOSITE_FORMAT_INCELL2000 2 // InCell2000 XML
#define BIM_COMPOSITE_FORMAT_CELLWORX 3   // CellWorX/MolDev HTD
// Yokogawa XML
// InCell3000 - does not seem to have a master file
// Cellomics - does not seem to have a master file

//#define BIM_COMPOSITE_FORMAT_DICOMDIR 200  // DICOM DIR

static const std::vector<bim::xstring> well_indices_letters = {
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
};

static const std::vector<bim::xstring> well_indices_letters_ext = {
    "Aa",
    "Ab",
    "Ac",
    "Ad",
    "Ba",
    "Bb",
    "Bc",
    "Bd",
    "Ca",
    "Cb",
    "Cc",
    "Cd",
    "Da",
    "Db",
    "Dc",
    "Dd",
    "Ea",
    "Eb",
    "Ec",
    "Ed",
    "Fa",
    "Fb",
    "Fc",
    "Fd",
    "Ga",
    "Gb",
    "Gc",
    "Gd",
    "Ha",
    "Hb",
    "Hc",
    "Hd",
    "Ia",
    "Ib",
    "Ic",
    "Id",
    "Ja",
    "Jb",
    "Jc",
    "Jd",
    "Ka",
    "Kb",
    "Kc",
    "Kd",
    "La",
    "Lb",
    "Lc",
    "Ld",
    "Ma",
    "Mb",
    "Mc",
    "Md",
    "Na",
    "Nb",
    "Nc",
    "Nd",
    "Oa",
    "Ob",
    "Oc",
    "Od",
    "Pa",
    "Pb",
    "Pc",
    "Pd",
    "Qa",
    "Qb",
    "Qc",
    "Qd",
    "Ra",
    "Rb",
    "Rc",
    "Rd",
    "Sa",
    "Sb",
    "Sc",
    "Sd",
    "Ta",
    "Tb",
    "Tc",
    "Td",
    "Ua",
    "Ub",
    "Uc",
    "Ud",
    "Va",
    "Vb",
    "Vc",
    "Vd",
    "Wa",
    "Wb",
    "Wc",
    "Wd",
    "Xa",
    "Xb",
    "Xc",
    "Xd",
    "Ya",
    "Yb",
    "Yc",
    "Yd",
    "Za",
    "Zb",
    "Zc",
    "Zd",
};

//****************************************************************************
// ViQi XML
//****************************************************************************

bool ViQiXMLValidate(const bim::xstring &filename);
void ViQiXMLOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl);
void ViQiXMLParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f);
void ViQiXMLParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl);

//****************************************************************************
// PrairieView XML
//****************************************************************************

bool PrairieViewXMLValidate(const bim::xstring &filename);
void PrairieViewXMLOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl);
void PrairieViewXMLParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f);
void PrairieViewXMLParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl);

//****************************************************************************
// InCell 2000
//****************************************************************************

bool InCell2KValidate(const bim::xstring &filename);
void InCell2KOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl);
void InCell2KParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f);
void InCell2KParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl);

//****************************************************************************
// CellWorX / MolDev
//****************************************************************************

bool CellWorXValidate(const bim::xstring &filename);
void CellWorXOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl);
void CellWorXParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f);
void CellWorXParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl);

//****************************************************************************
// composite_subformats
//****************************************************************************

static const std::vector<bim::CompositeFormat::SubFormatPlugin> composite_subformats = {
    { ViQiXMLValidate, ViQiXMLOpen, ViQiXMLParseMetaPerFile, ViQiXMLParseMeta },
    { PrairieViewXMLValidate, PrairieViewXMLOpen, PrairieViewXMLParseMetaPerFile, PrairieViewXMLParseMeta },
    { InCell2KValidate, InCell2KOpen, InCell2KParseMetaPerFile, InCell2KParseMeta },
    { CellWorXValidate, CellWorXOpen, CellWorXParseMetaPerFile, CellWorXParseMeta },
};

//****************************************************************************
// bim::CompositeFormat
//****************************************************************************

bim::CompositeFormat::CompositeFormat() {
    this->i = initImageInfo();
    this->dimensions.resize(BIM_MAX_DIMS, 1);
    this->positions.resize(BIM_MAX_DIMS, 0);
}

bim::CompositeFormat::~CompositeFormat() {
    // close stuff here
}

int bim::CompositeFormat::validate(const bim::xstring &filename) {
    for (size_t i = 0; i < composite_subformats.size(); ++i) {
        if (composite_subformats[i].validate(filename) == true) return i;
    }
    return -1;
}

void bim::CompositeFormat::open(const bim::xstring &filename, bim::ImageIOModes io_mode, FormatHandle *fmtHndl) {
    this->path = bim::fspath_get_path(filename);
    this->sub_format = bim::CompositeFormat::validate(filename);
    if (this->sub_format < 0) return;
    composite_subformats[this->sub_format].open(filename, io_mode, this, fmtHndl);
}

int bim::CompositeFormat::validate(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (fileName) {
        return bim::CompositeFormat::validate(fileName);
    }
    return -1;
}

void bim::CompositeFormat::read_image_info(FormatHandle *fmtHndl) {
    this->i.number_pages = 1;
    this->i.samples = 1;
    this->i.imageMode = IM_MULTI;

    uint64 num_samples = this->channels_in_files ? this->dimensions[bim::DIM_C] : 1;
    num_samples = bim::min<int>(num_samples, this->files.size());
    for (uint64 i = 0; i < num_samples; ++i) {
        // read file i, parse metadata and adjust
        bim::FormatManager fm;
        if (fm.sessionStartRead((bim::Filename)this->files[i].c_str()) == 0) {
            fm.sessionParseMetaData(0);
            bim::TagMap meta = fm.get_metadata();
            // rename channel_0 to proper channel
            meta.rename_key_startsWith(bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0), bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i));
            meta.rename_key_startsWith("MetaMorph/PlaneInfo/", bim::xstring::xprintf("MetaMorph/PlaneInfo:%d/", i));
            meta.rename_key("MetaMorph/Description/Exposure", bim::xstring::xprintf("MetaMorph/Description/Exposure:%d", i));

            // per-file parsing
            if (this->sub_format >= 0)
                composite_subformats[this->sub_format].parseMetaPerFile(meta, i, this->files[i], this);

            this->dimensions[bim::DIM_X] = meta.get_value_unsigned("image_num_x", 0);
            this->dimensions[bim::DIM_Y] = meta.get_value_unsigned("image_num_y", 0);

            this->metadata.append_tags(meta);
        }
        fm.sessionEnd();
    }

    // update dims
    this->i.width = this->dimensions[bim::DIM_X];
    this->i.height = this->dimensions[bim::DIM_Y];
    this->i.number_z = this->dimensions[bim::DIM_Z];
    this->i.number_t = this->dimensions[bim::DIM_T];
    this->i.number_pages = this->dimensions[bim::DIM_Z] * this->dimensions[bim::DIM_T];
    this->i.number_levels = this->metadata.get_value_unsigned("image_num_resolution_levels", 1);
    this->i.tileWidth = this->metadata.get_value_unsigned("tile_num_x", 0);
    this->i.tileHeight = this->metadata.get_value_unsigned("tile_num_y", 0);

    this->i.number_dims = 2;
    if (this->i.number_z > 1) ++this->i.number_dims;
    if (this->i.number_t > 1) ++this->i.number_dims;

    if (this->channels_in_files) {
        this->i.samples = this->dimensions[bim::DIM_C];
        this->i.imageMode = bim::IM_MULTI;
    } else {
        this->i.samples = this->metadata.get_value_int(bim::IMAGE_NUM_C, 1);
    }

    // final parse
    if (this->sub_format >= 0)
        composite_subformats[this->sub_format].parseMeta(this->metadata, this, fmtHndl);
}

void bim::CompositeFormat::read_metadata(bim::TagMap *hash, FormatHandle *fmtHndl) {
    hash->append_tags(this->metadata);

    // update dims
    hash->set_value(bim::IMAGE_NUM_SERIES, static_cast<unsigned int>(this->dimensions[bim::DIM_SERIE] * this->dimensions[bim::DIM_SCENE]));
    hash->set_value(bim::IMAGE_NUM_Z, static_cast<unsigned int>(this->dimensions[bim::DIM_Z]));
    hash->set_value(bim::IMAGE_NUM_T, static_cast<unsigned int>(this->dimensions[bim::DIM_T]));
    hash->set_value(bim::IMAGE_NUM_P, static_cast<unsigned int>(this->dimensions[bim::DIM_Z] * this->dimensions[bim::DIM_T]));
    if (this->channels_in_files) {
        hash->set_value(bim::IMAGE_NUM_C, static_cast<unsigned int>(this->dimensions[bim::DIM_C]));
        hash->set_value(bim::IMAGE_MODE, bim::ICC_TAGS_COLORSPACE_MULTICHANNEL);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_MULTICHANNEL);
    }
}

bim::uint64 bim::CompositeFormat::get_file_pos(FormatHandle *fmtHndl) const {
    XConf *conf = fmtHndl->arguments;
    uint64 page = this->channels_in_files ? fmtHndl->pageNumber * this->dimensions[bim::DIM_C] : fmtHndl->pageNumber;

    // add series dimension
    if (conf && conf->keyExists("-slice-serie")) {
        int64 serie = conf->getValueInt("-slice-serie", 0);
        uint64 block = this->dimensions[bim::DIM_Z] * this->dimensions[bim::DIM_T];
        if (this->channels_in_files) block *= this->dimensions[bim::DIM_C];
        page += block * serie;
    }

    return page;
}

void bim::CompositeFormat::read_level(Image &img, const uint64 level, FormatHandle *fmtHndl) {
    const uint64 page = this->get_file_pos(fmtHndl);
    if (page >= this->files.size()) return;

    img.fromPyramidFile(this->files[page], 0, level);
    if (!this->channels_in_files) return;

    for (uint64 i = 1; i < this->dimensions[bim::DIM_C]; ++i) {
        Image img2(this->files[page + i], 0, level);
        img = img.appendChannels(img2);
    }
}

void bim::CompositeFormat::read_tile(Image &img, uint64 xid, uint64 yid, uint64 level, FormatHandle *fmtHndl) {
    const uint64 page = this->get_file_pos(fmtHndl);
    if (page > this->files.size()) return;
    const uint64 tilesize = 512;

    img.fromPyramidFile(this->files[page], 0, level, xid, yid, tilesize);
    if (!this->channels_in_files) return;

    for (uint64 i = 1; i < this->dimensions[bim::DIM_C]; ++i) {
        Image img2(this->files[page + i], 0, level, xid, yid, tilesize);
        img = img.appendChannels(img2);
    }
}

void bim::CompositeFormat::read_region(bim::Image &img, uint64 x1, uint64 y1, uint64 x2, uint64 y2, uint64 level, FormatHandle *fmtHndl) {
    const uint64 page = this->get_file_pos(fmtHndl);
    if (page > this->files.size()) return;

    img.fromPyramidRegion(this->files[page], 0, level, x1, y1, x2, y2);
    if (!this->channels_in_files) return;

    for (uint64 i = 1; i < this->dimensions[bim::DIM_C]; ++i) {
        Image img2(this->files[page + i], 0, level, x1, y1, x2, y2);
        img = img.appendChannels(img2);
    }
}

//****************************************************************************
// format
//****************************************************************************

int compositeValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    return bim::CompositeFormat::validate(magic, length, fileName);
}

FormatHandle compositeAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void compositeCloseImageProc(FormatHandle *fmtHndl);
void compositeReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    compositeCloseImageProc(fmtHndl);
}

//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------

bool compositeGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    par->read_image_info(fmtHndl);
    return true;
}

void compositeCloseImageProc(FormatHandle *fmtHndl) {
    if (!fmtHndl) return;
    bim::CompositeFormat *par = (bim::CompositeFormat *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint compositeOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) compositeCloseImageProc(fmtHndl);
    bim::CompositeFormat *par = new bim::CompositeFormat();
    fmtHndl->internalParams = (void *)par;

    fmtHndl->io_mode = io_mode;
    par->open(fmtHndl->fileName, io_mode, fmtHndl);

    if (io_mode == IO_READ) {
        if (!compositeGetImageInfo(fmtHndl)) {
            compositeCloseImageProc(fmtHndl);
            return 1;
        };
    }
    return 0;
}

//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint compositeGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    return par->getInfo()->number_pages;
}

ImageInfo compositeGetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    ImageInfo ii = initImageInfo();
    if (fmtHndl == NULL) return ii;
    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    return *par->getInfo();
}

//----------------------------------------------------------------------------
// metadata
//----------------------------------------------------------------------------

bim::uint composite_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    par->read_metadata(hash, fmtHndl);
    return 0;
}

//----------------------------------------------------------------------------
// read
//----------------------------------------------------------------------------

bim::uint compositeReadLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    fmtHndl->pageNumber = page;

    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    bim::Image image;
    par->read_level(image, level, fmtHndl);

    // move data to the output image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, &image.imageBitmap()->i, bmp) != 0) return 1;
    bim::Image image_out(bmp);
    image_out.copy(image);

    return 0;
}

bim::uint compositeReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    return compositeReadLevelProc(fmtHndl, page, 0);
}

bim::uint compositeReadTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    fmtHndl->pageNumber = page;

    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    bim::Image image;
    par->read_tile(image, xid, yid, level, fmtHndl);

    // move data to the output image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, &image.imageBitmap()->i, bmp) != 0) return 1;
    bim::Image image_out(bmp);
    image_out.copy(image);
    return 0;
}

bim::uint compositeReadRegionProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    fmtHndl->pageNumber = page;

    CompositeFormat *par = (CompositeFormat *)fmtHndl->internalParams;
    bim::Image image;
    par->read_region(image, x1, y1, x2, y2, level, fmtHndl);

    // move data to the output image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, &image.imageBitmap()->i, bmp) != 0) return 1;
    bim::Image image_out(bmp);
    image_out.copy(image);
    return 0;
}

//----------------------------------------------------------------------------
// write
//----------------------------------------------------------------------------

bim::uint compositeWriteImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;

    return 1;
}



//****************************************************************************
// ViQi XML
//****************************************************************************

bool ViQiXMLValidate(const bim::xstring &filename) {
    if (filename.toLowerCase().endsWith(".vqcx")) return true;
    return false;
}


/*
<image name="INMAC384-DAPI-CM-eGFP_59223_1_A01">
    <tag name="image_meta" type="image_meta">
        <tag name="storage" value="multi_file_series" />
        <tag name="dimensions" value="XYCZT" />
        <tag name="image_num_z" type="number" value="1" />
        <tag name="image_num_t" type="number" value="1" />
        <tag name="image_num_c" type="number" value="3" />
        <tag name="pixel_resolution_x" type="number" value="0.74" />
        <tag name="pixel_resolution_y" type="number" value="0.74" />
        <tag name="pixel_resolution_unit_x" value="&#181;m" />
        <tag name="pixel_resolution_unit_y" value="&#181;m" />
        <tag name="objectives/objective:0/focal_length" value="20.0" />
        <tag name="objectives/objective:0/magnification" value="10.0" />
        <tag name="objectives/objective:0/name" value="Nikon 10X/0.45, Plan Apo, CFI/60" />
        <tag name="objectives/objective:0/numerical_aperture" value="0.45" />
        <tag name="objectives/objective:0/refractive_index" value="0.0" />
        <tag name="channels">
            <tag name="channel_00000">
                <tag name="name" value="DAPI (DAPI)" />
                <tag name="fluor" value="DAPI" />
                <tag name="filter" value="DAPI" />
                <tag name="modality" value="Fluorescence" />
                <tag name="exposure" value="1000" />
                <tag name="exposure_units" value="ms" />
                <tag name="emission_wavelength" value="455" />
                <tag name="excitation_wavelength" value="350" />
            </tag>
            <tag name="channel_00001">
                <tag name="name" value="CM (Cy5)" />
                <tag name="fluor" value="CM" />
                <tag name="filter" value="Cy5" />
                <tag name="modality" value="Fluorescence" />
                <tag name="exposure" value="3000" />
                <tag name="exposure_units" value="ms" />
                <tag name="emission_wavelength" value="705" />
                <tag name="excitation_wavelength" value="645" />
            </tag>
            <tag name="channel_00002">
                <tag name="name" value="eGFP (FITC)" />
                <tag name="fluor" value="eGFP" />
                <tag name="filter" value="FITC" />
                <tag name="modality" value="Fluorescence" />
                <tag name="exposure" value="3000" />
                <tag name="exposure_units" value="ms" />
                <tag name="emission_wavelength" value="525" />
                <tag name="excitation_wavelength" value="490" />
            </tag>
        </tag>
        <tag name="channel_0_name" value="DAPI (DAPI)" />
        <tag name="channel_1_name" value="CM (Cy5)" />
        <tag name="channel_2_name" value="eGFP (FITC)" />
    </tag>
    <value index="0">INMAC384-DAPI-CM-eGFP_59223_1/A - 1(fld 1 wv DAPI - DAPI).tif</value>
    <value index="1">INMAC384-DAPI-CM-eGFP_59223_1/A - 1(fld 1 wv Cy5 - Cy5).tif</value>
    <value index="2">INMAC384-DAPI-CM-eGFP_59223_1/A - 1(fld 1 wv FITC - FITC).tif</value>
</image>
*/

void ViQiXMLParseNode(const pugi::xml_node &node, bim::TagMap &meta, const xstring &path) {
    // iterate over children
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        xstring tag = child.name();
        xstring name = child.attribute("name").as_string();
        if (tag == "tag") {
            xstring value = child.attribute("value").as_string();
            if (value.size() > 0)
                meta.set_value(path + name, value);
        }

        ViQiXMLParseNode(child, meta, path + name + "/");
    }
}

void ViQiXMLParse(pugi::xml_document *doc, CompositeFormat *f, FormatHandle *fmtHndl) {
    try {
        // read files
        pugi::xpath_node_set nodes = doc->select_nodes("image/value");
        for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
            pugi::xml_node node = it->node();
            unsigned int idx = node.attribute("index").as_uint();
            xstring fn = f->path + bim::fspath_fix_slashes(node.text().as_string());
            f->files.push_back(fn);
        }

        // read meta
        pugi::xml_node tag_num_z = doc->select_node("image/tag[@type='image_meta']/tag[@name='image_num_z']").node();
        if (tag_num_z) f->dimensions[bim::DIM_Z] = tag_num_z.attribute("value").as_uint();

        pugi::xml_node tag_num_t = doc->select_node("image/tag[@type='image_meta']/tag[@name='image_num_t']").node();
        if (tag_num_t) f->dimensions[bim::DIM_T] = tag_num_t.attribute("value").as_uint();

        pugi::xml_node tag_num_c = doc->select_node("image/tag[@type='image_meta']/tag[@name='image_num_c']").node();
        if (tag_num_c) f->dimensions[bim::DIM_C] = tag_num_c.attribute("value").as_uint();

        // read all the tags
        pugi::xml_node tag_meta = doc->select_node("image/tag[@type='image_meta']").node();
        ViQiXMLParseNode(tag_meta, f->metadata, "");

    } catch (...) {
        // do nothing
    }

    if (f->dimensions[bim::DIM_C] > 1)
        f->channels_in_files = true;
}

void ViQiXMLOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl) {
    pugi::xml_document doc;
#if defined(BIM_WIN)
    if (doc.load_file(filename.toUTF16().c_str())) {
#else
    if (doc.load_file(filename.c_str())) {
#endif
        ViQiXMLParse(&doc, f, fmtHndl);
    }
}

void ViQiXMLParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f) {
    meta.eraseKeysStartingWith("Exif/");
}

void ViQiXMLParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl) {
}

//****************************************************************************
// PrairieView XML
//****************************************************************************

bool PrairieViewXMLValidate(const bim::xstring &filename) {
    if (filename.toLowerCase().endsWith(".pvscan")) return true;
    if (filename.toLowerCase().endsWith(".env")) return true;
    return false;
}

void PrairieViewXMLParseTag(const pugi::xml_node &node, bim::TagMap &meta) {
    /*
    <PVStateValue key="activeMode" value="Galvo"/>
    <PVStateValue key="bitDepth" value="12"/>
    <PVStateValue key="currentScanAmplitude">
    <IndexedValue index="XAxis" value="7.8"/>
    <IndexedValue index="YAxis" value="-7.8"/>
    </PVStateValue>
    <PVStateValue key="daq">
    <IndexedValue index="0" value="Gain20" description="20x"/>
    <IndexedValue index="1" value="Gain20" description="20x"/>
    <IndexedValue index="2" value="Gain20" description="20x"/>
    <IndexedValue index="3" value="Gain20" description="20x"/>
    </PVStateValue>
    <PVStateValue key="micronsPerPixel">
    <IndexedValue index="XAxis" value="1.6326884217898"/>
    <IndexedValue index="YAxis" value="1.6326884217898"/>
    <IndexedValue index="ZAxis" value="1.5"/>
    </PVStateValue>
    <PVStateValue key="positionCurrent">
    <SubindexedValues index="XAxis">
    <SubindexedValue subindex="0" value="-228.99"/>
    </SubindexedValues>
    <SubindexedValues index="YAxis">
    <SubindexedValue subindex="0" value="66.38"/>
    </SubindexedValues>
    <SubindexedValues index="ZAxis">
    <SubindexedValue subindex="0" value="25.1"/>
    </SubindexedValues>
    </PVStateValue>
    <PVStateValue key="scanLinePeriod" value="0.00147399217932631"/>
    <PVStateValue key="useInterlacedScanPattern" value="False"/>
    */

    bim::xstring n = node.attribute("key").as_string();
    bim::xstring v = node.attribute("value").as_string();
    if (v.size() > 0) { // single value
                        // parse types here
        meta.set_value("PrairieView/" + n, v);
    } else { // vector of values
        std::vector<bim::xstring> vv;
        std::vector<bim::xstring> vd;
        pugi::xpath_node_set nodes = node.select_nodes("IndexedValue");
        for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
            pugi::xml_node iv = it->node();
            bim::xstring idx = iv.attribute("index").as_string();
            bim::xstring vvv = iv.attribute("value").as_string();
            bim::xstring vvd = iv.attribute("description").as_string();
            if (idx.toInt(-1) == -1) { // index is a string
                meta.set_value("PrairieView/" + n + "/" + idx, vvv);
            } else { // index is an int
                vv.push_back(vvv);
                if (vvd.size() > 0) vd.push_back(vvd);
            }
        }

        if (vv.size() > 0) { // found IndexedValue
            meta.set_value("PrairieView/" + n, vv);
            if (vd.size() > 0)
                meta.set_value("PrairieView/" + n + "_description", vd);

        } else { // no IndexedValue found, try SubindexedValues
            nodes = node.select_nodes("SubindexedValues");
            for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
                pugi::xml_node iv = it->node();
                bim::xstring sub_name = n + "/" + iv.attribute("index").as_string();
                vv.resize(0);
                vd.resize(0);

                pugi::xpath_node_set sub_nodes = iv.select_nodes("SubindexedValue");
                for (pugi::xpath_node_set::const_iterator itt = sub_nodes.begin(); itt != sub_nodes.end(); ++itt) {
                    pugi::xml_node subn = itt->node();
                    bim::xstring vvv = subn.attribute("value").as_string();
                    bim::xstring vvd = subn.attribute("description").as_string();
                    vv.push_back(vvv);
                    if (vvd.size() > 0) vd.push_back(vvd);
                }

                meta.set_value("PrairieView/" + sub_name, vv);
                if (vd.size() > 0)
                    meta.set_value("PrairieView/" + sub_name + "_description", vd);
            }
        }
    }
}

void PrairieViewXMLParse(pugi::xml_document *doc, CompositeFormat *f, FormatHandle *fmtHndl) {
    /*
    METADATA:

    <PVScan version="5.4.64.400" date="9/3/2019 7:50:37 PM" notes="">
    <SystemIDs SystemID="B6F3-A39D-53D9-7101-9EE0-2AB6-009E-74DD">
    <SystemID SystemID="4380" Description="scope 3"/>
    </SystemIDs>
    <PVStateShard>
    <PVStateValue key="activeMode" value="Galvo"/>
    <PVStateValue key="bitDepth" value="12"/>
    <PVStateValue key="currentScanAmplitude">
    <IndexedValue index="XAxis" value="7.8"/>
    <IndexedValue index="YAxis" value="-7.8"/>
    </PVStateValue>
    <PVStateValue key="daq">
    <IndexedValue index="0" value="Gain20" description="20x"/>
    <IndexedValue index="1" value="Gain20" description="20x"/>
    <IndexedValue index="2" value="Gain20" description="20x"/>
    <IndexedValue index="3" value="Gain20" description="20x"/>
    </PVStateValue>
    <PVStateValue key="dwellTime" value="1.6"/>
    <PVStateValue key="framePeriod" value="0.756157987994399"/>
    <PVStateValue key="interlacedScanChannelMapping">
    <IndexedValue index="0" value="0"/>
    <IndexedValue index="1" value="3"/>
    <IndexedValue index="2" value="2"/>
    <IndexedValue index="3" value="3"/>
    </PVStateValue>
    <PVStateValue key="interlacedScanTrackCount" value="2"/>
    <PVStateValue key="interlacedScanTrackLasers">
    <IndexedValue index="0" value="0"/>
    <IndexedValue index="1" value="1"/>
    </PVStateValue>
    <PVStateValue key="laserPower">
    <IndexedValue index="0" value="37" description="Primary"/>
    <IndexedValue index="1" value="44" description="Secondary"/>
    </PVStateValue>
    <PVStateValue key="linesPerFrame" value="512"/>
    <PVStateValue key="pixelsPerLine" value="512"/>
    <PVStateValue key="micronsPerPixel">
    <IndexedValue index="XAxis" value="1.6326884217898"/>
    <IndexedValue index="YAxis" value="1.6326884217898"/>
    <IndexedValue index="ZAxis" value="1.5"/>
    </PVStateValue>
    <PVStateValue key="objectiveLens" value="Nikon 16X/0.8W"/>
    <PVStateValue key="objectiveLensMag" value="16"/>
    <PVStateValue key="objectiveLensNA" value="0.8"/>
    <PVStateValue key="opticalZoom" value="1"/>
    <PVStateValue key="positionCurrent">
    <SubindexedValues index="XAxis">
    <SubindexedValue subindex="0" value="-228.99"/>
    </SubindexedValues>
    <SubindexedValues index="YAxis">
    <SubindexedValue subindex="0" value="66.38"/>
    </SubindexedValues>
    <SubindexedValues index="ZAxis">
    <SubindexedValue subindex="0" value="25.1"/>
    </SubindexedValues>
    </PVStateValue>
    <PVStateValue key="rastersPerFrame" value="1"/>
    <PVStateValue key="rotation" value="0"/>
    <PVStateValue key="scanLinePeriod" value="0.00147399217932631"/>
    <PVStateValue key="useInterlacedScanPattern" value="False"/>
    </PVStateShard>

    IMAGE PLANES:

    <Sequence type="TSeries ZSeries Element" cycle="5" time="19:51:40.2665216" bidirectionalZ="False"
    xYStageGridDefined="False" xYStageGridNumXPositions="3" xYStageGridNumYPositions="2"
    xYStageGridOverlapPercentage="15" xYStageGridXOverlap="241.666666666666" xYStageGridYOverlap="180.555555555555">
    <PVStateShard/>
    <Frame relativeTime="0" absoluteTime="62.6629999999986" index="1" parameterSet="CurrentSettings">
    <File channel="1" channelName="Ch1" filename="TSeries-001_Cycle00005_Ch1_000001.ome.tif"/>
    <File channel="2" channelName="Ch2" filename="TSeries-001_Cycle00005_Ch2_000001.ome.tif"/>
    <File channel="3" channelName="Ch3" filename="TSeries-001_Cycle00005_Ch3_000001.ome.tif"/>
    <File channel="4" channelName="Ch4" filename="TSeries-001_Cycle00005_Ch4_000001.ome.tif"/>
    <ExtraParameters lastGoodFrame="0"/>
    <PVStateShard>
    <PVStateValue key="positionCurrent">
    <SubindexedValues index="XAxis">
    <SubindexedValue subindex="0" value="-228.99"/>
    </SubindexedValues>
    <SubindexedValues index="YAxis">
    <SubindexedValue subindex="0" value="66.38"/>
    </SubindexedValues>
    <SubindexedValues index="ZAxis">
    <SubindexedValue subindex="0" value="2.85"/>
    </SubindexedValues>
    </PVStateValue>
    </PVStateShard>
    </Frame>
    ...
    ...
    */

    try {
        // read files
        std::set<unsigned int> C;
        std::set<unsigned int> Z;
        std::set<unsigned int> T;

        pugi::xpath_node_set sequences = doc->select_nodes("PVScan/Sequence"); // sequence describes T
        for (pugi::xpath_node_set::const_iterator it = sequences.begin(); it != sequences.end(); ++it) {
            pugi::xml_node sequence = it->node();
            unsigned int t = sequence.attribute("cycle").as_uint();
            T.insert(t);

            pugi::xpath_node_set frames = sequence.select_nodes("Frame"); // Frame describes Z
            for (pugi::xpath_node_set::const_iterator itf = frames.begin(); itf != frames.end(); ++itf) {
                pugi::xml_node frame = itf->node();
                unsigned int z = frame.attribute("index").as_uint();
                Z.insert(z);

                pugi::xpath_node_set files = frame.select_nodes("File"); // Frame describes Z
                for (pugi::xpath_node_set::const_iterator itff = files.begin(); itff != files.end(); ++itff) {
                    pugi::xml_node file = itff->node();
                    unsigned int c = file.attribute("channel").as_uint();
                    C.insert(c);

                    xstring fn = f->path + bim::fspath_fix_slashes(file.attribute("filename").as_string());
                    f->files.push_back(fn);
                }
            }
        }

        f->dimensions[bim::DIM_Z] = Z.size();
        f->dimensions[bim::DIM_T] = T.size();
        f->dimensions[bim::DIM_C] = C.size();

        // read meta
        pugi::xpath_node_set nodes = doc->select_nodes("PVScan/PVStateShard/PVStateValue"); // metadata fields
        for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
            PrairieViewXMLParseTag(it->node(), f->metadata);
        }

    } catch (...) {
        // do nothing
    }

    if (f->dimensions[bim::DIM_C] > 1)
        f->channels_in_files = true;
}

void PrairieViewXMLOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl) {
    xstring fn = filename;
    if (filename.toLowerCase().endsWith(".env"))
        fn = fn.replace(".env", ".xml");
    pugi::xml_document doc;
#if defined(BIM_WIN)
    if (doc.load_file(fn.toUTF16().c_str())) {
#else
    if (doc.load_file(fn.c_str())) {
#endif
        PrairieViewXMLParse(&doc, f, fmtHndl);
    }
}

void PrairieViewXMLParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f) {
    //meta.rename_key("MetaMorph/wavelengths", bim::xstring::xprintf("PrairieView/wavelength:%d", sample));
}

void PrairieViewXMLParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl) {
    meta.set_value_from_old_key("MetaMorph/Name", bim::DOCUMENT_INSTRUMENT);
    meta.set_value_from_old_key("MetaMorph/Software", bim::DOCUMENT_APPLICATION);
    meta.eraseKeysStartingWith("MetaMorph/");

    // parse PraireView tags
    meta.set_value_from_old_key("PrairieView/micronsPerPixel/XAxis", bim::PIXEL_RESOLUTION_X);
    meta.set_value_from_old_key("PrairieView/micronsPerPixel/YAxis", bim::PIXEL_RESOLUTION_Y);
    meta.set_value_from_old_key("PrairieView/micronsPerPixel/ZAxis", bim::PIXEL_RESOLUTION_Z);
    meta.set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    meta.set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    meta.set_value(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);

    meta.set_value_from_old_key("PrairieView/objectiveLens", bim::OBJECTIVE_DESCRIPTION);
    meta.set_value_from_old_key("PrairieView/objectiveLensMag", bim::OBJECTIVE_MAGNIFICATION);
    meta.set_value_from_old_key("PrairieView/objectiveLensNA", bim::OBJECTIVE_NUM_APERTURE);

    //PrairieView/pmtGain: 620,500,420,692,0,0
    //PrairieView/pmtGain_description: Ch1 GaAsP,Ch2 GaAsP,Ch3 GaAsP,Ch4 GaAsP,Sub PMT 1,Sub PMT 2
    for (uint64 i = 1; i < f->dimensions[bim::DIM_C]; ++i) {
        xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
        meta.set_value_from_old_key("PrairieView/opticalZoom", path + bim::CHANNEL_INFO_ZOOM);
        //hash->set_value(path + bim::CHANNEL_INFO_GAIN, wavelength);
        //hash->set_value(path + bim::CHANNEL_INFO_GAIN, wavelength);
    }
}

//****************************************************************************
// InCell2000 XML
//****************************************************************************

bool InCell2KValidate(const bim::xstring &filename) {
    if (filename.toLowerCase().endsWith(".xdce")) return true;
    return false;
}

void InCell2KParseNode(const pugi::xml_node &node, bim::TagMap &meta, const xstring &path) {
    xstring name = node.name();
    if (name == "Wavelengths") return;
    if (name == "Images") return;

    // iterate over attributes
    for (pugi::xml_attribute_iterator ait = node.attributes_begin(); ait != node.attributes_end(); ++ait) {
        meta.set_value(path + "/" + ait->name(), ait->value());
    }

    // iterate over children
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        xstring cname = child.name();
        if (cname == "Wavelengths") continue;
        if (cname == "Images") continue;
        if (cname == "Wavelength") continue;
        if (cname == "Image") continue;
        InCell2KParseNode(child, meta, path + "/" + child.name());
    }
}

void InCell2KParse(pugi::xml_document *doc, CompositeFormat *f, FormatHandle *fmtHndl) {
    /*
<ImageStack>
    <UUID value="89da83d0-dfa2-4425-a43f-40f07937f2d2"/>
    <Application name="IN Cell Analyzer 2000" software_version="9949"/>
    <Creation date="2013-03-26" time="13:26:31"/>
    <Operator login="Administrator"/>
    <Computer name="HP20900172072"/>
    <AutoLeadAcquisitionProtocol fingerprint="9a0e5d441d149fe5d220371fc2aabe03" name="INMAC384-DAPI-CM-eGFP" password="689935b988643b7779f765cf46a32d3b" version="99.9">
        <UserComment/>
        <Microscopy type="Fluorescence"/>
        <ObjectiveCalibration focal_length="20.0" id="12111" lineartype="7" magnification="10.0" numerical_aperture="0.45" objective_name="Nikon 10X/0.45, Plan Apo, CFI/60" pixel_height="0.74" pixel_width="0.74" refractive_index="0.0" unit="µm"/>
        <Polychroic id="X" name="QUAD2"/>
        <Camera apply_ffc="false" name="K4" use_ffc="false" version="1.0">
            <Description/>
            <Binning value="1 X 1"/>
            <Bias value="97.474045"/>
            <Gain value=""/>
            <Size height="2048" width="2048"/>
        </Camera>
        <Wavelengths>
            <Wavelength aperture_rows="0" fusion_wave="false" imaging_mode="2-D" index="0" laser_power="0" open_aperture="false" show="false" z_section="0.0" z_slice="1" z_step="0.0">
                <ExcitationFilter name="DAPI" unit="nm" wavelength="350"/>
                <EmissionFilter name="DAPI" unit="nm" wavelength="455"/>
                <Exposure time="1000" unit="ms"/>
                <HWAFOffset unit="µm" value="0.0"/>
                <FocusOffset unit="µm" value="-5.0"/>
                <FusionData/>
            </Wavelength>
            <Wavelength aperture_rows="0" fusion_wave="false" imaging_mode="2-D" index="1" laser_power="0" open_aperture="false" show="false" z_section="0.0" z_slice="1" z_step="0.0">
                <ExcitationFilter name="Cy5" unit="nm" wavelength="645"/>
                <EmissionFilter name="Cy5" unit="nm" wavelength="705"/>
                <Exposure time="3000" unit="ms"/>
                <HWAFOffset unit="µm" value="0.0"/>
                <FocusOffset unit="µm" value="-2.0"/>
                <FusionData/>
            </Wavelength>
            <Wavelength aperture_rows="0" fusion_wave="false" imaging_mode="2-D" index="2" laser_power="0" open_aperture="false" show="false" z_section="0.0" z_slice="1" z_step="0.0">
                <ExcitationFilter name="FITC" unit="nm" wavelength="490"/>
                <EmissionFilter name="FITC" unit="nm" wavelength="525"/>
                <Exposure time="3000" unit="ms"/>
                <HWAFOffset unit="µm" value="0.0"/>
                <FocusOffset unit="µm" value="-1.0"/>
                <FusionData/>
            </Wavelength>
        </Wavelengths>
        <SoftwareAutoFocus enabled="false" focus_wavelength="first" init_stepping="0.0" max_search_range="5000.0" min_stepping="-1500.0" mode_type="Adaptive" mode_value="Large" refocusEachTimePoint="false" show_live_image="false" skip_field="1" skip_well="1"/>
        <LaserAutoFocus enabled="true"/>
        <Plate columns="24" name="Corning cellbind 384-well plate" rows="16" version="1.0">
            <PlateHeight unit="mm" value="14.2"/>
            <BottomThickness unit="µm" value="566.0"/>
            <BottomInterface name="Plastic"/>
            <BottomHeight value="1.968"/>
            <WellVolume value="112.0"/>
            <WellParameters shape="Square" size="2.67" unit="mm"/>
            <TopLeftWellCenterOffset horizontal="12.2" unit="mm" vertical="8.4"/>
            <WellSpacing horizontal="4.49" unit="mm" vertical="4.5"/>
        </Plate>
        <Specimen expected_thickness="1.0" on_slide="false" unit="µm"/>
        <PlateHeater use="false"/>
        <PlateMap>
            <AcquisitionMode>
                <Layout columns="1" rows="1"/>
                <Spacing spacing_percentage="" spacing_x="0.0" spacing_y="0.0"/>
                <Shifting shift_x="0.0" shift_y="0.0"/>
                <FieldOffsets custom_loc_x="0.0" custom_loc_y="0.0" flex_num_fields="false" margin="0.0" mode="formation" number_of_fields="1" weight="user_defined">
                    <offset_point index="0" x="0.0" y="0.0"/>
                </FieldOffsets>
                <Direction value="Horizontal Snake"/>
            </AcquisitionMode>
            <ZDimensionParameters enabled="false" linked="false"/>
            <TimeSchedule enabled="false" incubate_between_time_points="false" mode="spit and stare" time_unit="sec"/>
        </PlateMap>
        <Output image_format="TIFF" save_3d_image_in_dv="false" write_stack_to_dce="" write_stack_to_xml=""/>
        <StackDestination base_path="system_default" current_path="date_time" image_folder=" "/>
        <AddToBatchAnalysisQueue analysis_protocol_name="Z:\BATCH\Protocols\MDR-dapi-cm-egfp-incell2000-modified-finalv4.xeap"/>
        <Deconvolution camera_intensity_offset="50" method="Enhanced Ratio (aggressive)" normalize_intensity="true" number_cycles="5"/>
        <ReviewScanProtocol activeAnalyses="" reviewScanProtocol="" use="false" useAnalysis="false"/>
    </AutoLeadAcquisitionProtocol>
    <SpecimenHolder label="Corning cellbind 384-well plate" type="Plate"/>
    <CorrectionInfo/>

    <Images image_format="TIFF" number="1152" path="Z:\DNDi Leishmania\Batch Screening\MCT B150\INMAC384-DAPI-CM-eGFP\INMAC384-DAPI-CM-eGFP_59223_1" version="1.0">
        <Image GUID="231b224a-5604-4967-a9fa-034b412fea38" acquisition_time_ms="3580.28" cell_count="0" filename="A - 1(fld 1 wv FITC - FITC).tif" version="1.1">
            <Well label="A - 1">
                <Row number="1"/>
                <Column number="1"/>
            </Well>
            <OffsetFromWellCenter unit="µm" version="1.0" x="0.0" y="0.0"/>
            <MinMaxMean max="309.0" mean="99.363174" min="92.0"/>
            <FocusPosition unit="µm" z="2303.62"/>
            <Exposure time="3000.0" unit="ms"/>
            <UseOZModule state=""/>
            <Identifier field_index="0" time_index="0" version="1.1" wave_index="2" z_index="0"/>
            <ExcitationFilter name="FITC"/>
            <EmissionFilter name="FITC"/>
            <ImageErrorLog acquisition="ACQUISITION"/>
            <FFC applied="false"/>
        </Image>

*/
    int num_images = 0;
    int num_wavelengths = 0;
    std::vector<bim::xstring> keys;
    bim::TagMap files;
    std::set<unsigned int> C;
    std::set<unsigned int> Z;
    std::set<unsigned int> T;
    std::set<unsigned int> S;  // scene
    std::set<bim::xstring> SS; // series
    int max_row = 0;
    xstring current_label;
    try {
        // walk everything except "Wavelengths" and "Images"
        InCell2KParseNode(doc->first_child(), f->metadata, "InCell");

        // walk wavelengths
        pugi::xpath_node_set waves = doc->select_nodes("ImageStack/AutoLeadAcquisitionProtocol/Wavelengths/Wavelength");
        for (pugi::xpath_node_set::const_iterator it = waves.begin(); it != waves.end(); ++it) {
            InCell2KParseNode(it->node(), f->metadata, xstring::xprintf("InCell/Wavelength:%d", num_wavelengths));
            ++num_wavelengths;
        }

        // walk images
        pugi::xpath_node_set images = doc->select_nodes("ImageStack/Images/Image");
        for (pugi::xpath_node_set::const_iterator it = images.begin(); it != images.end(); ++it) {
            bim::TagMap meta;
            InCell2KParseNode(it->node(), meta, "");
            ++num_images;

            xstring w_label = meta.get_value("/Well/label");
            int wi = meta.get_value_int("/Well/Row/number", 0);
            int wj = meta.get_value_int("/Well/Column/number", 0);
            int s = meta.get_value_int("/Identifier/field_index", 0);
            int t = meta.get_value_int("/Identifier/time_index", 0);
            int c = meta.get_value_int("/Identifier/wave_index", 0);
            int z = meta.get_value_int("/Identifier/z_index", 0);
            C.insert(c);
            Z.insert(z);
            T.insert(t);
            S.insert(s);
            SS.insert(w_label);

            xstring filename = meta.get_value("/filename");
            xstring key = xstring::xprintf("%.6d,%.6d,%.6d,%.6d,%.6d,%.6d", wj, wi, s, t, z, c);
            keys.push_back(key);
            files.set_value(key, f->path + bim::fspath_fix_slashes(filename));
            //f->files.push_back(f->path + bim::fspath_fix_slashes(filename));


            /*
            <Image GUID="231b224a-5604-4967-a9fa-034b412fea38" acquisition_time_ms="3580.28" cell_count="0" filename="A - 1(fld 1 wv FITC - FITC).tif" version="1.1">
            <Well label="A - 1">
            <Row number="1"/>
            <Column number="1"/>
            </Well>
            <OffsetFromWellCenter unit="µm" version="1.0" x="0.0" y="0.0"/>
            <MinMaxMean max="309.0" mean="99.363174" min="92.0"/>
            <FocusPosition unit="µm" z="2303.62"/>
            <Exposure time="3000.0" unit="ms"/>
            <UseOZModule state=""/>
            <Identifier field_index="0" time_index="0" version="1.1" wave_index="2" z_index="0"/>
            <ExcitationFilter name="FITC"/>
            <EmissionFilter name="FITC"/>
            <ImageErrorLog acquisition="ACQUISITION"/>
            <FFC applied="false"/>
            </Image>
            */
            int well_id = SS.size();
            xstring path = xstring::xprintf("_wells/well:%d/", well_id);
            f->metadata.set_value(path + "well_label", meta.get_value("/Well/label"));
            f->metadata.set_value(path + "well_row", meta.get_value_int("/Well/Row/number", 0));
            f->metadata.set_value(path + "well_col", meta.get_value_int("/Well/Column/number", 0));
            f->metadata.set_value(path + "focus_position", meta.get_value("/FocusPosition/z"));
            f->metadata.set_value(path + "focus_position_unit", meta.get_value("/FocusPosition/unit"));

            max_row = bim::max<int>(max_row, meta.get_value_int("/Well/Row/number", 0));
        }

    } catch (...) {
        // do nothing
    }

    f->dimensions[bim::DIM_Z] = bim::max<int>(1, Z.size());
    f->dimensions[bim::DIM_T] = bim::max<int>(1, T.size());
    f->dimensions[bim::DIM_C] = bim::max<int>(1, C.size());
    f->dimensions[bim::DIM_SCENE] = bim::max<int>(1, S.size());
    f->dimensions[bim::DIM_SERIE] = bim::max<int>(1, SS.size());

    f->metadata.set_value("_max_row", max_row);

    // set files in the proper order
    std::sort(keys.begin(), keys.end());
    for (const xstring &key : keys) {
        xstring filename = files.get_value(key);
        f->files.push_back(filename);
    }


    // parse meta

    /*
    <ImageStack>
        <AutoLeadAcquisitionProtocol fingerprint="9a0e5d441d149fe5d220371fc2aabe03" name="INMAC384-DAPI-CM-eGFP" password="689935b988643b7779f765cf46a32d3b" version="99.9">
            <Microscopy type="Fluorescence"/>
            <ObjectiveCalibration focal_length="20.0" id="12111" lineartype="7" magnification="10.0" numerical_aperture="0.45" objective_name="Nikon 10X/0.45, Plan Apo, CFI/60"
            pixel_height="0.74" pixel_width="0.74" refractive_index="0.0" unit="µm"/>
    */

    // pixel resolution
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/pixel_width", bim::PIXEL_RESOLUTION_X);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/pixel_height", bim::PIXEL_RESOLUTION_Y);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/unit", bim::PIXEL_RESOLUTION_UNIT_X);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/unit", bim::PIXEL_RESOLUTION_UNIT_Y);

    // objective
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/objective_name", bim::OBJECTIVE_DESCRIPTION);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/magnification", bim::OBJECTIVE_MAGNIFICATION);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/numerical_aperture", bim::OBJECTIVE_NUM_APERTURE);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/refractive_index", bim::OBJECTIVE_REF_INDEX);
    f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/ObjectiveCalibration/focal_length", bim::OBJECTIVE_FOCAL_LENGTH);

    // Wavelengths
    /*
    <Wavelengths>
        <Wavelength aperture_rows="0" fusion_wave="false" imaging_mode="2-D" index="0" laser_power="0" open_aperture="false" show="false" z_section="0.0" z_slice="1" z_step="0.0">
            <ExcitationFilter name="DAPI" unit="nm" wavelength="350"/>
            <EmissionFilter name="DAPI" unit="nm" wavelength="455"/>
            <HWAFOffset unit="µm" value="0.0"/>
            <FocusOffset unit="µm" value="-5.0"/>
            <FusionData/>
        </Wavelength>
    */

    for (int w = 0; w < num_wavelengths; ++w) {
        xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), w);
        xstring inpath = xstring::xprintf("InCell/Wavelength:%d", w);
        f->metadata.set_value_from_old_key(inpath + "/Exposure/time", path + bim::CHANNEL_INFO_EXPOSURE);
        f->metadata.set_value_from_old_key(inpath + "/Exposure/unit", path + bim::CHANNEL_INFO_EXPOSURE_UNITS);
        f->metadata.set_value_from_old_key(inpath + "/ExcitationFilter/name", path + bim::CHANNEL_INFO_NAME);
        f->metadata.set_value_from_old_key(inpath + "/ExcitationFilter/wavelength", path + bim::CHANNEL_INFO_EX_WAVELENGTH);
        f->metadata.set_value_from_old_key(inpath + "/EmissionFilter/name", path + bim::CHANNEL_INFO_NAME);
        f->metadata.set_value_from_old_key(inpath + "/EmissionFilter/wavelength", path + bim::CHANNEL_INFO_EM_WAVELENGTH);
        f->metadata.set_value_from_old_key(inpath + "/EmissionFilter/name", path + bim::CHANNEL_INFO_FILTER_NAME);
        f->metadata.set_value_from_old_key(inpath + "/EmissionFilter/name", path + bim::CHANNEL_INFO_FLUOR);
        f->metadata.set_value_from_old_key("InCell/AutoLeadAcquisitionProtocol/Microscopy/type", path + bim::CHANNEL_INFO_MODALITY);
    }

    if (f->dimensions[bim::DIM_C] > 1)
        f->channels_in_files = true;
}

void InCell2KOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl) {
    pugi::xml_document doc;
#if defined(BIM_WIN)
    if (doc.load_file(filename.toUTF16().c_str())) {
#else
    if (doc.load_file(filename.c_str())) {
#endif
        InCell2KParse(&doc, f, fmtHndl);
    }
}

void InCell2KParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f) {
}

void InCell2KParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl) {
    meta.eraseKeysStartingWith("Exif/");
    meta.eraseKeysStartingWith("TIFF/");

    XConf *conf = fmtHndl->arguments;
    int serie = conf ? conf->getValueInt("-slice-serie", 0) : 0;
    int w = serie, s = 0;
    if (f->dimensions[bim::DIM_SCENE] > 1) {
        w = floor((double)serie / f->dimensions[bim::DIM_SCENE]);
        s = serie % f->dimensions[bim::DIM_SCENE];
    }

    xstring path = xstring::xprintf("_wells/well:%d/", w);
    xstring well_label = f->metadata.get_value(path + "well_label");
    int well_row = f->metadata.get_value_int(path + "well_row", 1);
    int well_col = f->metadata.get_value_int(path + "well_col", 1);
    double focus_position = f->metadata.get_value_double(path + "focus_position", 0.0);
    xstring focus_position_unit = f->metadata.get_value(path + "focus_position_unit");
    bool large_plate = f->metadata.get_value_int("_max_row", 0) > 26;
    meta.eraseKeysStartingWith("_wells/");
    meta.delete_tag("_large_plate");

    xstring label, labeli, labelj;
    xstring labelij = xstring::xprintf("%d,%d", well_row, well_col);
    if (!large_plate) {
        label = xstring::xprintf("%s%.2d", well_indices_letters[well_row - 1].c_str(), well_col);
        labeli = xstring::xprintf("%s", well_indices_letters[well_row - 1].c_str());
        labelj = xstring::xprintf("%d", well_col);
    } else {
        label = xstring::xprintf("%s%.3d", well_indices_letters_ext[well_row - 1].c_str(), well_col);
        labeli = xstring::xprintf("%s", well_indices_letters_ext[well_row - 1].c_str());
        labelj = xstring::xprintf("%d", well_col);
    }

    f->metadata.set_value(bim::DOCUMENT_WELL_NAME, label);
    f->metadata.set_value(bim::DOCUMENT_WELL_ROW, labeli);
    f->metadata.set_value(bim::DOCUMENT_WELL_COL, labelj);
    f->metadata.set_value(bim::DOCUMENT_WELL_IJ, labelij);
    f->metadata.set_value(bim::DOCUMENT_WELL_SITE, s + 1);
}


//****************************************************************************
// CellWorX / MolDev
//****************************************************************************

bool CellWorXValidate(const bim::xstring &filename) {
    if (filename.toLowerCase().endsWith(".htd")) return true;
    return false;
}

void CellWorXParse(const bim::TagMap &header, CompositeFormat *f, FormatHandle *fmtHndl) {
    // create a list of available wells
    std::vector<int> welli;
    std::vector<int> wellj;
    for (int i = 0; i < 1000000; ++i) {
        xstring k = xstring::xprintf("WellsSelection%d", (i + 1));
        if (!header.hasKey(k)) break;
        std::vector<xstring> enabeled = header.get_value_vector_string(k);
        for (size_t j = 0; j < enabeled.size(); ++j) {
            if (enabeled[j].toLowerCase() == "true") {
                welli.push_back(i + 1);
                wellj.push_back(j + 1);
            }
        }
    }
    // dima: should be a set of unique i
    bool large_plate = (!welli.empty() && welli.back() > 26) ? true : false;
    f->metadata.set_value("_welli", welli);
    f->metadata.set_value("_wellj", wellj);
    f->metadata.set_value("_large_plate", large_plate);

    // enumerate available sites, they always begin at 1 and ordered independent of their site location
    //int Sx = header.get_value_int("XSites", 1);
    //int Sy = header.get_value_int("YSites", 1);
    int S = 0;
    for (int i = 0; i < 1000000; ++i) {
        xstring k = xstring::xprintf("SiteSelection%d", (i + 1));
        if (!header.hasKey(k)) break;
        std::vector<xstring> enabeled = header.get_value_vector_string(k);
        for (size_t j = 0; j < enabeled.size(); ++j) {
            if (enabeled[j].toLowerCase() == "true") {
                ++S;
            }
        }
    }
    S = std::max<int>(1, S);

    // compute other dimensions
    int Z = header.get_value_int("ZSteps", 1);
    int T = header.get_value_int("TimePoints", 1);
    int C = header.get_value_int("NWavelengths", 1);
    bool has_time_dir = header.get_value_int("TimePoints", 0) == 0 ? false : true;

    // now create well descriptor files
    xstring folder = header.get_value("_folder");
    for (size_t w = 0; w < welli.size(); ++w) {
        xstring label;
        if (!large_plate) {
            label = xstring::xprintf("%s%.2d", well_indices_letters[welli[w] - 1].c_str(), wellj[w]);
        } else {
            label = xstring::xprintf("%s%.3d", well_indices_letters_ext[welli[w] - 1].c_str(), wellj[w]);
        }

        for (int s = 0; s < S; ++s) {
            for (int t = 0; t < T; ++t) {
                for (int z = 0; z < Z; ++z) {
                    for (int c = 0; c < C; ++c) {
                        xstring filename = f->path;
                        if (has_time_dir)
                            filename += xstring::xprintf("TimePoint_%d/", t + 1);
                        if (Z > 1)
                            filename += xstring::xprintf("ZStep_%d/", z + 1);

                        filename = bim::fspath_fix_slashes(xstring::xprintf("%s%s_%s", filename.c_str(), folder.c_str(), label.c_str()));
                        if (S <= 1 && C <= 1) {
                            f->files.push_back(xstring::xprintf("%s.TIF", filename.c_str()));
                        } else if (S <= 1 && C > 1) {
                            f->files.push_back(xstring::xprintf("%s_w%d.TIF", filename.c_str(), c + 1));
                        } else if (S > 1 && C <= 1) {
                            f->files.push_back(xstring::xprintf("%s_s%d.TIF", filename.c_str(), s + 1));
                        } else if (S > 1 && C > 1) {
                            f->files.push_back(xstring::xprintf("%s_s%d_w%d.TIF", filename.c_str(), s + 1, c + 1));
                        }
                    } // for c
                }     // for z
            }         // for t
        }             // for s
    }                 // for well

    f->dimensions[bim::DIM_Z] = Z;
    f->dimensions[bim::DIM_T] = T;
    f->dimensions[bim::DIM_C] = C;
    f->dimensions[bim::DIM_SCENE] = bim::max<int>(1, S);
    f->dimensions[bim::DIM_SERIE] = bim::max<int>(1, welli.size());
    if (f->dimensions[bim::DIM_C] > 1)
        f->channels_in_files = true;
}

void CellWorXParseLine(const bim::xstring &line, bim::TagMap &header) {
    std::vector<xstring> ll = line.split(", ");
    if (ll.size() < 1) return;
    xstring n = ll[0].removeSpacesBoth().replace("\"", "");
    xstring v = line.replace(ll[0] + ", ", "");
    if (v == "TRUE") {
        header.set_value(n, true);
    } else if (v == "FALSE") {
        header.set_value(n, false);
    } else if (v.contains("\"")) {
        header.set_value(n, v.removeSpacesBoth().replace("\"", ""));
    } else if (v.contains(", ")) {
        std::vector<xstring> vv = v.split(", ");
        header.set_value(n, vv);
    } else {
        int vv = v.toInt(-99999999);
        if (vv != -99999999)
            header.set_value(n, vv);
        else
            header.set_value(n, v);
    }
}

void CellWorXOpen(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl) {
    bim::TagMap header;
    header.set_value("_folder", bim::fspath_get_filename(filename.replace(".HTD", "")));
    std::ifstream fs(filename.c_str());
    if (fs.is_open())
        while (!fs.eof()) {
            std::string line;
            std::getline(fs, line);
            CellWorXParseLine(line, header);
        }
    fs.close();

    CellWorXParse(header, f, fmtHndl);
}

void CellWorXParseMetaPerFile(bim::TagMap &meta, int sample, const bim::xstring &filename, CompositeFormat *f) {
    meta.rename_key_startsWith("MetaMorph/PlaneInfo/", bim::xstring::xprintf("MetaMorph/PlaneInfo:%d/", sample));
    meta.rename_key("MetaMorph/Description/Exposure", bim::xstring::xprintf("MetaMorph/Description/Exposure:%d", sample));
}

void CellWorXParseMeta(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl) {
    std::vector<int> welli = meta.get_value_vector_int("_welli");
    std::vector<int> wellj = meta.get_value_vector_int("_wellj");
    bool large_plate = meta.get_value_bool("_large_plate", false);
    meta.delete_tag("_welli");
    meta.delete_tag("_wellj");
    meta.delete_tag("_large_plate");

    XConf *conf = fmtHndl->arguments;
    bim::uint serie = conf ? conf->getValueInt("-slice-serie", 0) : 0;
    bim::uint w = serie, s = 0;
    if (f->dimensions[bim::DIM_SCENE] > 1) {
        w = floor((double)serie / f->dimensions[bim::DIM_SCENE]);
        s = serie % f->dimensions[bim::DIM_SCENE];
    }

    if ((welli.size() > w) && (wellj.size() > w)) {
        xstring label, labeli, labelj;
        xstring labelij = xstring::xprintf("%d,%d", welli[w], wellj[w]);
        if (!large_plate) {
            label = xstring::xprintf("%s%.2d", well_indices_letters[welli[w] - 1].c_str(), wellj[w]);
            labeli = xstring::xprintf("%s", well_indices_letters[welli[w] - 1].c_str());
            labelj = xstring::xprintf("%d", wellj[w]);
        } else {
            label = xstring::xprintf("%s%.3d", well_indices_letters_ext[welli[w] - 1].c_str(), wellj[w]);
            labeli = xstring::xprintf("%s", well_indices_letters_ext[welli[w] - 1].c_str());
            labelj = xstring::xprintf("%d", wellj[w]);
        }

        f->metadata.set_value(bim::DOCUMENT_WELL_NAME, label);
        f->metadata.set_value(bim::DOCUMENT_WELL_ROW, labeli);
        f->metadata.set_value(bim::DOCUMENT_WELL_COL, labelj);
        f->metadata.set_value(bim::DOCUMENT_WELL_IJ, labelij);
        f->metadata.set_value(bim::DOCUMENT_WELL_SITE, s + 1);
    }
}

//****************************************************************************
// EXPORTED
//****************************************************************************

#define BIM_COMPOSITE_NUM_FORMATS 4

FormatItem compositeItems[BIM_COMPOSITE_NUM_FORMATS] = {
    { "VQCX",               // short name, no spaces
      "ViQi Composite XML", // Long format name
      "vqcx",               // pipe "|" separated supported extension list
      1,                    //canRead;      // 0 - NO, 1 - YES
      0,                    //canWrite;     // 0 - NO, 1 - YES
      1,                    //canReadMeta;  // 0 - NO, 1 - YES
      0,                    //canWriteMeta; // 0 - NO, 1 - YES
      0,                    //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    { "PVXML",           // short name, no spaces
      "PrairieView XML", // Long format name
      "PVScan|env|xml",  // pipe "|" separated supported extension list
      1,                 //canRead;      // 0 - NO, 1 - YES
      0,                 //canWrite;     // 0 - NO, 1 - YES
      1,                 //canReadMeta;  // 0 - NO, 1 - YES
      0,                 //canWriteMeta; // 0 - NO, 1 - YES
      0,                 //canWriteMultiPage;   // 0 - NO, 1 - YES
                         //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    { "INCELL2000",  // short name, no spaces
      "InCell 2000", // Long format name
      "xdce",        // pipe "|" separated supported extension list
      1,             //canRead;      // 0 - NO, 1 - YES
      0,             //canWrite;     // 0 - NO, 1 - YES
      1,             //canReadMeta;  // 0 - NO, 1 - YES
      0,             //canWriteMeta; // 0 - NO, 1 - YES
      0,             //canWriteMultiPage;   // 0 - NO, 1 - YES
                     //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    { "CELLWORX",        // short name, no spaces
      "CellWorX/MolDev", // Long format name
      "htd",             // pipe "|" separated supported extension list
      1,                 //canRead;      // 0 - NO, 1 - YES
      0,                 //canWrite;     // 0 - NO, 1 - YES
      1,                 //canReadMeta;  // 0 - NO, 1 - YES
      0,                 //canWriteMeta; // 0 - NO, 1 - YES
      0,                 //canWriteMultiPage;   // 0 - NO, 1 - YES
                         //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } }
};

FormatHeader compositeHeader = {

    sizeof(FormatHeader),
    "3.0.0",
    "Composite CODEC",
    "COMPOSITE CODEC",

    BIM_FORMAT_COMPOSITE_MAGIC_SIZE,                  // 0 or more, specify number of bytes needed to identify the file
    { 1, BIM_COMPOSITE_NUM_FORMATS, compositeItems }, // dimJpegSupported,

    compositeValidateFormatProc,
    // begin
    compositeAquireFormatProc, //AquireFormatProc
    // end
    compositeReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    compositeOpenImageProc,  //OpenImageProc
    compositeCloseImageProc, //CloseImageProc

    // info
    compositeGetNumPagesProc,  //GetNumPagesProc
    compositeGetImageInfoProc, //GetImageInfoProc

    // read/write
    compositeReadImageProc,  //ReadImageProc
    NULL,                    //WriteImageProc
    compositeReadTileProc,   //ReadImageTileProc
    NULL,                    //WriteImageTileProc
    compositeReadLevelProc,  //ReadImageLevelProc
    NULL,                    //WriteImageLevelProc
    NULL,                    //-ReadImageThumbProc
    NULL,                    //-WriteImageThumbProc
    NULL,                    //-ReadImagePreviewProc
    compositeReadRegionProc, //ReadImageRegionProc
    NULL,                    //WriteImageRegionProc

    // meta data
    NULL, //-ReadMetaDataAsTextProc
    composite_append_metadata,
    NULL,
    NULL,
    ""
};

extern "C" {
FormatHeader *compositeGetFormatHeader(void) {
    return &compositeHeader;
}

} // extern C
