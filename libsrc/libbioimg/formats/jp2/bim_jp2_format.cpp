/*****************************************************************************
JPEG-2000 support
Copyright (c) 2015 by Mario Emmenlauer <mario@emmenlauer.de>
Copyright (c) 2015, Center for Bio-Image Informatics, UCSB
Copyright (c) 2015, Dmitry Fedorov <www.dimin.net> <dima@dimin.net>

Authors:
    Mario Emmenlauer <mario@emmenlauer.de>
    Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
    2015-04-19 14:20:00 - First creation
    2015-07-22 11:21:40 - Rewrite

ver : 2
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <bim_ome_types.h>
#include <tag_map.h>
#include <xstring.h>
#include <xtypes.h>
#ifdef BIM_USE_EXIV2
#include <bim_exiv_parse.h>
#endif

#include "bim_jp2_format.h"

#include <openjpeg.h>

using namespace bim;

//****************************************************************************
// Misc
//****************************************************************************/

// JP2 I/O wrappers

static void jp2_error_callback(const char *msg, void *client_data) {
    std::cerr << "[JP2 codec error] " << msg;
}

static void jp2_warning_callback(const char *msg, void *client_data) {
    std::cerr << "[JP2 codec warning] " << msg;
}

#if defined(WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64) || defined(_MSVC)
#define fseek _fseeki64
#define ftell _ftelli64
#elif (defined(_FILE_OFFSET_BITS) && (_FILE_OFFSET_BITS == 64))
#define fseek fseeko
#define ftell ftello
#endif

static OPJ_UINT64 bim_opj_get_data_length_from_file(bim::JP2Params *par) {
    OPJ_OFF_T file_length = 0;

    fseek(par->file, 0, SEEK_END);
    file_length = (OPJ_OFF_T)ftell(par->file);
    fseek(par->file, 0, SEEK_SET);

    return (OPJ_UINT64)file_length;
}

static OPJ_SIZE_T bim_opj_read_from_file(void *p_buffer, OPJ_SIZE_T p_nb_bytes, bim::JP2Params *par) {
    OPJ_SIZE_T l_nb_read = fread(p_buffer, 1, p_nb_bytes, par->file);
    return l_nb_read ? l_nb_read : (OPJ_SIZE_T)-1;
}

static OPJ_SIZE_T bim_opj_write_from_file(void *p_buffer, OPJ_SIZE_T p_nb_bytes, bim::JP2Params *par) {
    return fwrite(p_buffer, 1, p_nb_bytes, par->file);
}

static OPJ_OFF_T bim_opj_skip_from_file(OPJ_OFF_T p_nb_bytes, bim::JP2Params *par) {
    if (fseek(par->file, p_nb_bytes, SEEK_CUR)) {
        return -1;
    }

    return p_nb_bytes;
}

static OPJ_BOOL bim_opj_seek_from_file(OPJ_OFF_T p_nb_bytes, bim::JP2Params *par) {
    if (fseek(par->file, p_nb_bytes, SEEK_SET)) {
        return OPJ_FALSE;
    }

    return OPJ_TRUE;
}

// JP2 parameters

bim::JP2Params::JP2Params() {
    i = initImageInfo();

    stream = NULL;
    codec = NULL;
    image = NULL;

    num_tiles_x = 0;
    num_tiles_y = 0;

    file = NULL;
}

bim::JP2Params::~JP2Params() {
    close();
}

void bim::JP2Params::open(const char *filename, bim::ImageIOModes io_mode) {
#ifdef BIM_WIN
    bim::xstring fn(filename);
    this->file = _wfopen(fn.toUTF16().c_str(), io_mode == IO_READ ? L"rb" : L"wb");
#else
    this->file = fopen(filename, io_mode == IO_READ ? "rb" : "wb");
#endif

    this->stream = opj_stream_create(OPJ_J2K_STREAM_CHUNK_SIZE, io_mode == IO_READ ? OPJ_TRUE : OPJ_FALSE);

    if (this->stream) {
        opj_stream_set_user_data(this->stream, this, (opj_stream_free_user_data_fn)NULL);
        opj_stream_set_user_data_length(this->stream, bim_opj_get_data_length_from_file(this));
        opj_stream_set_read_function(this->stream, (opj_stream_read_fn)bim_opj_read_from_file);
        opj_stream_set_write_function(this->stream, (opj_stream_write_fn)bim_opj_write_from_file);
        opj_stream_set_skip_function(this->stream, (opj_stream_skip_fn)bim_opj_skip_from_file);
        opj_stream_set_seek_function(this->stream, (opj_stream_seek_fn)bim_opj_seek_from_file);

        if (io_mode == IO_READ)
            codec = opj_create_decompress(OPJ_CODEC_JP2);
        else
            codec = opj_create_compress(OPJ_CODEC_JP2);

        // configure the event callbacks
        opj_set_info_handler(codec, NULL, NULL);
        opj_set_warning_handler(codec, NULL, NULL);
        opj_set_error_handler(codec, jp2_error_callback, NULL);

        if (io_mode == IO_READ) {
            // setup the decoder decoding parameters using user parameters
            opj_dparameters_t parameters;
            opj_set_default_decoder_parameters(&parameters);
            if (!opj_setup_decoder(codec, &parameters)) {
                throw(std::runtime_error("Failed to setup the decoder"));
            }

#if (OPJ_VERSION_MAJOR > 2) || (OPJ_VERSION_MAJOR == 2 && OPJ_VERSION_MINOR >= 3)
            // Allow to set the number of CPU threads. Use 0 for a single thread:
            const int vThreads = 0;
            if (!opj_codec_set_threads(codec, vThreads)) {
                throw(std::runtime_error("Failed to set the number of CPU threads"));
            }
#endif

            // read the main header of the codestream and if necessary the JP2 boxes
            if (!opj_read_header(stream, codec, &image)) {
                throw(std::runtime_error("Failed to read the header"));
            }
        }
    } else {
        throw(std::runtime_error("Failed to open the file stream"));
    }
}

void bim::JP2Params::close() {
    if (image) {
        opj_image_destroy(image);
        image = NULL;
    }
    if (codec) {
        opj_destroy_codec(codec);
        codec = NULL;
    }
    if (stream) {
        opj_stream_destroy(stream);
        stream = NULL;
    }
    if (file) {
        fclose(file);
        file = NULL;
    }
}


//****************************************************************************
// required funcs
//****************************************************************************/

#define BIM_FORMAT_JP2_MAGIC_SIZE 12

#define JP2_RFC3745_MAGIC "\x00\x00\x00\x0c\x6a\x50\x20\x20\x0d\x0a\x87\x0a"
#define JP2_MAGIC "\x0d\x0a\x87\x0a"
// position 45: "\xff\x52"
#define J2K_CODESTREAM_MAGIC "\xff\x4f\xff\x51"

int jp2ValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_FORMAT_JP2_MAGIC_SIZE) return -1;

    // The file is a valid jp2 image (openjpeg magic_format JP2_CFMT, correct extension .jp2):
    if (memcmp(magic, JP2_RFC3745_MAGIC, 12) == 0) return 0;
    if (memcmp(magic, JP2_MAGIC, 4) == 0) return 0;

    // The file is a valid jp2 image (openjpeg magic_format J2K_CFMT, correct extensions .j2k, .jpc, .j2c):
    if (memcmp(magic, J2K_CODESTREAM_MAGIC, 4) == 0) return 0;

    return -1;
}

FormatHandle jp2AquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void jp2CloseImageProc(FormatHandle *fmtHndl);
void jp2ReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    jp2CloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------


void jp2SetWriteParameters(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    fmtHndl->order = 1; // use progressive encoding by default
    fmtHndl->quality = 95;

    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    par->i.tileWidth = par->i.tileHeight = 0;

    if (!fmtHndl->options) return;
    xstring str = fmtHndl->options;
    std::vector<xstring> options = str.split(" ");
    if (options.size() < 1) return;

    for (size_t i = 0; (i + 1) < options.size(); ++i) {
        if (options[i] == "quality") {
            fmtHndl->quality = options[++i].toInt(100);
        } else {
            if (options[i] == "progressive") {
                if (options[++i] == "no")
                    fmtHndl->order = 0;
                else
                    fmtHndl->order = 1;
            } else {
                if (options[i] == "tiles") {
                    par->i.tileWidth = par->i.tileHeight = options[++i].toInt(0);
                }
            }
        }
    } // while
}

void jp2ParseMetadata(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;

    // read ICC profile
    if (par->image && par->image->icc_profile_len > 0) {
        //par->image->icc_profile_buf, par->image->icc_profile_len);
        par->buffer_icc.resize(par->image->icc_profile_len);
        memcpy(&par->buffer_icc[0], par->image->icc_profile_buf, par->image->icc_profile_len);
    }

#ifdef OPJ_HAVE_MULTI_COMMENTS
    // Read the main header of the codestream, and if necessary the JP2 boxes
    if (par->image) {
        const char *bda_cp_comment = par->image->bda_cp_comment;
        if (bda_cp_comment) {
            size_t endpos = 0;
            size_t startpos = 0;
            const char *comment;
            while (true) {
                ++endpos;
                if (bda_cp_comment[endpos] == 0 && bda_cp_comment[endpos + 1] != 0) {
                    // A single zero character followed by a non-zero character indicates the end
                    // of one comment. So here is a break between two concatenated comments:
                    comment = (bda_cp_comment + startpos);
                    par->comments.push_back(comment);
                    startpos = endpos + 1;
                } else if (bda_cp_comment[endpos] == 0 && bda_cp_comment[endpos + 1] == 0 && bda_cp_comment[endpos + 2] == 0) {
                    // Three consecutive zero characters indicate the end of the comments list.
                    // So here is the end of all string comments:
                    comment = (bda_cp_comment + startpos);
                    par->comments.push_back(comment);
                    break;
                }
            }
        }
    }
#endif
}

void jp2GetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    *info = initImageInfo();

    // header should be already loaded in the open

    opj_codestream_info_v2_t *pCodeStreamInfo = opj_get_cstr_info(par->codec);
    info->tileWidth = pCodeStreamInfo->tdx;
    info->tileHeight = pCodeStreamInfo->tdy;
    par->num_tiles_x = pCodeStreamInfo->tw;
    par->num_tiles_y = pCodeStreamInfo->th;
    info->number_levels = pCodeStreamInfo->m_default_tile_info.tccp_info[0].numresolutions;
    opj_destroy_cstr_info(&pCodeStreamInfo);

    info->width = par->image->comps[0].w;
    info->height = par->image->comps[0].h;
    info->samples = par->image->numcomps;
    info->depth = par->image->comps[0].prec;

    info->pixelType = FMT_UNSIGNED;
    if (par->image->comps[0].sgnd == 1)
        info->pixelType = FMT_SIGNED;
    // dima: need to identify when image is floating point
    //info->pixelType = FMT_FLOAT;
    //par->image->comps[0].sgnd
    //par->image->comps[0].bpp

    // dima: here we need to actually define the colorspace
    info->imageMode = IM_GRAYSCALE;
    if (par->image->color_space == OPJ_CLRSPC_SRGB) {
        info->imageMode = IM_RGB;
    } else if (par->image->color_space == OPJ_CLRSPC_SYCC) {
        info->imageMode = IM_YUV;
    } else if (par->image->color_space == OPJ_CLRSPC_EYCC) {
        info->imageMode = IM_YUV; // dima: needs fixing
    } else if (par->image->color_space == OPJ_CLRSPC_CMYK) {
        info->imageMode = IM_CMYK;
    } else if (info->samples > 1) {
        info->imageMode = IM_MULTI;
    }

    for (bim::uint32 c = 0; c + 1 < par->image->numcomps; c++) {
        if ((par->image->comps[c].dx != par->image->comps[c + 1].dx) ||
            (par->image->comps[c].dy != par->image->comps[c + 1].dy) ||
            (par->image->comps[c].prec != par->image->comps[c + 1].prec)) {
            throw(std::runtime_error("Non-uniform componnets are not supported"));
        }
    }

    info->number_z = 1;
    info->number_t = 1;
    info->number_pages = 1;
    info->number_dims = 2;

    /*
    float resX, resY;	// image resolution (in dots per inch)
    par->pDecoder->GetResolution(par->pDecoder, &resX, &resY);
    info->resUnits = RES_IN;
    info->xRes = resX;
    info->yRes = resY;
    */

    jp2ParseMetadata(fmtHndl);
}

void jp2CloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint jp2OpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) jp2CloseImageProc(fmtHndl);
    bim::JP2Params *par = new bim::JP2Params();
    fmtHndl->internalParams = (void *)par;

    try {
        par->open(fmtHndl->fileName, io_mode);
        if (io_mode == IO_READ) {
            jp2GetImageInfo(fmtHndl);
        } else if (io_mode == IO_WRITE) {
            jp2SetWriteParameters(fmtHndl);
        }
    } catch (...) {
        jp2CloseImageProc(fmtHndl);
        return 1;
    }

    return 0;
}


//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint jp2GetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    return info->number_pages;
}

ImageInfo jp2GetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    ImageInfo ii = initImageInfo();
    if (fmtHndl == NULL) return ii;
    fmtHndl->pageNumber = page_num;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    return par->i;
}

//----------------------------------------------------------------------------
// READ
//----------------------------------------------------------------------------

template<typename T>
void copy_component(bim::uint64 W, bim::uint64 H, int sample, const opj_image_comp_t &in, void *out) {
    OPJ_INT32 a = (in.sgnd ? 1 << (in.prec - 1) : 0);
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W > BIM_OMP_FOR2 && H > BIM_OMP_FOR2)
    for (bim::int64 y = 0; y < (bim::int64)H; ++y) {
        T *p = ((T *)out) + y * W;
        bim::uint64 pos = y * W;
        for (bim::int64 x = 0; x < (bim::int64)W; ++x) {
            *p = (T)(in.data[pos] + a);
            p++;
            pos++;
        } // for x
    }     // for y
}

bim::uint jp2ReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;

    ImageBitmap *bmp = fmtHndl->image;
    ImageInfo *info = &par->i;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    try {
        // openjpeg unfortunately has some problem with multiple decodings, have to reset everything from scratch
        if (!par->image)
            par->open(fmtHndl->fileName, fmtHndl->io_mode);

        if (!opj_decode(par->codec, par->stream, par->image)) {
            throw(std::runtime_error("Failed to decode image"));
        }

        if (!opj_end_decompress(par->codec, par->stream)) {
            throw(std::runtime_error("Failed to finish decompression"));
        }

        for (bim::uint64 s = 0; s < info->samples; ++s) {
            if (info->depth == 8)
                copy_component<bim::uint8>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
            else if (info->depth == 16)
                copy_component<bim::uint16>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
            else if (info->depth == 32)
                copy_component<bim::uint32>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
        } // for sample

        par->close();
    } catch (...) {
        par->close();
        return 1;
    }
    return 0;
}

bim::uint jp2ReadImageLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    try {
        // openjpeg unfortunately has some problem with multiple decodings, have to reset everything from scratch
        if (!par->image)
            par->open(fmtHndl->fileName, fmtHndl->io_mode);

        // read a specific resolution level
        if (!opj_set_decoded_resolution_factor(par->codec, level)) {
            throw(std::runtime_error("Failed to set image level"));
        }

        // setting resolution level does not change image size in opj_decode, a little hack to
        // update image size based on the resolution level
        info->width = bim::round<bim::uint64>((double)info->width / pow(2.0, level));
        info->height = bim::round<bim::uint64>((double)info->height / pow(2.0, level));
        for (bim::uint64 i = 0; i < info->samples; ++i) {
            par->image->comps[i].w = info->width;
            par->image->comps[i].h = info->height;
        }

        if (!opj_decode(par->codec, par->stream, par->image)) {
            throw(std::runtime_error("Failed to decode image level"));
        }

        if (!opj_end_decompress(par->codec, par->stream)) {
            throw(std::runtime_error("Failed to finish decompression"));
        }

        // setting resolution level does not change image size in opj_decode
        //info->width = par->image->comps[0].w;
        //info->height = par->image->comps[0].h;
        ImageBitmap *bmp = fmtHndl->image;
        if (allocImg(fmtHndl, info, bmp) != 0) return 1;

        for (bim::uint64 s = 0; s < info->samples; ++s) {
            if (info->depth == 8)
                copy_component<bim::uint8>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
            else if (info->depth == 16)
                copy_component<bim::uint16>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
            else if (info->depth == 32)
                copy_component<bim::uint32>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
        } // for sample

        par->close();
    } catch (...) {
        par->close();
        return 1;
    }
    return 0;
}

bim::uint jp2ReadImageTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    if (xid >= par->num_tiles_x || yid >= par->num_tiles_y) {
        return 1;
    }

    try {
        // openjpeg unfortunately has some problem with multiple decodings, have to reset everything from scratch
        if (!par->image)
            par->open(fmtHndl->fileName, fmtHndl->io_mode);

        // read a specific resolution level
        if (!opj_set_decoded_resolution_factor(par->codec, level)) {
            throw(std::runtime_error("Failed to set image level"));
        }

        OPJ_UINT32 tile_index = yid * par->num_tiles_x + xid;
        if (!opj_get_decoded_tile(par->codec, par->stream, par->image, tile_index)) {
            throw(std::runtime_error("Failed to decode tile"));
        }

        //if (!opj_end_decompress(par->codec, par->stream)) {
        //    throw(std::runtime_error("Failed to finish decompression"));
        //}

        info->width = par->image->comps[0].w;
        info->height = par->image->comps[0].h;
        ImageBitmap *bmp = fmtHndl->image;
        if (allocImg(fmtHndl, info, bmp) != 0) return 1;

        for (bim::uint64 s = 0; s < info->samples; ++s) {
            if (par->image->comps[s].data == NULL) {
                throw(std::runtime_error("No image data for component of sample " + std::to_string(s)));
            }
            if (info->depth == 8)
                copy_component<bim::uint8>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
            else if (info->depth == 16)
                copy_component<bim::uint16>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
            else if (info->depth == 32)
                copy_component<bim::uint32>(info->width, info->height, s, par->image->comps[s], bmp->bits[s]);
        } // for sample

        par->close();
    } catch (...) {
        par->close();
        return 1;
    }
    return 0;
}

//----------------------------------------------------------------------------
// WRITE
//----------------------------------------------------------------------------

static void jp2SetWriteMetadata(opj_image_t *image, TagMap *hash) {
    if (!hash) return;

    // write ICC profile
    if (hash->hasKey(bim::RAW_TAGS_ICC) && hash->get_type(bim::RAW_TAGS_ICC) == bim::RAW_TYPES_ICC) {
        image->icc_profile_len = hash->get_size(bim::RAW_TAGS_ICC);
        image->icc_profile_buf = (OPJ_BYTE *)hash->get_value_bin(bim::RAW_TAGS_ICC);
    }
}

template<typename T>
void copy_from_component(bim::uint64 W, bim::uint64 H, int sample, const void *in, const opj_image_comp_t &out) {
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W > BIM_OMP_FOR2 && H > BIM_OMP_FOR2)
    for (bim::uint64 y = 0; y < H; ++y) {
        T *p = ((T *)in) + y * W;
        bim::uint64 pos = y * W;
        for (bim::uint64 x = 0; x < W; ++x) {
            out.data[pos] = (OPJ_INT32)(*p);
            p++;
            pos++;
        } // for x
    }     // for y
}

std::string jp2ConstructComment(bim::FormatHandle *fmtHndl) {
    bim::TagMap *hash = fmtHndl->metaData;

    // BioDataAnalysis GmbH extension: store OME XML Image Description in JP2:
    std::string vComment;

    if (hash) {
        if (hash->hasKey(bim::RAW_TAGS_OMEXML)) {
            vComment = (hash->get_value_bin(bim::RAW_TAGS_OMEXML));
        } else {
            // Store meta data as key-value-pairs in multiple JP2 COM markers:
            for (bim::TagMap::const_iterator vIter = hash->begin(); vIter != hash->end(); ++vIter) {
                vComment += (vIter->first + '=' + hash->get_value(vIter->first));
                if (vIter != hash->end())
                    vComment += '\0';
            }
        }
    }

    // To support multiple comments, our string is terminated by tripple zeros.
    // This is in sync with a BioDataAnalysis GmbH extension of OpenJPEG::j2k.c
    // where the string length is computed in opj_com_marker_total_comment_length()
    vComment.append(3, '\0');

    return vComment;
}

bim::uint jp2WriteImageProc(FormatHandle *fmtHndl) {
    if (!fmtHndl) return 1;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;
    int tile_size = static_cast<int>(par->i.tileWidth);
    ImageBitmap *bmp = fmtHndl->image;
    ImageInfo *info = &bmp->i;

    try {
        opj_cparameters_t parameters;
        opj_set_default_encoder_parameters(&parameters);
        parameters.cp_disto_alloc = 1;
        parameters.tcp_numlayers = 1;
        parameters.tcp_mct = (info->samples == 3) ? 1 : 0; // decide if MCT should be used
        const std::string vComment = jp2ConstructComment(fmtHndl);
        parameters.cp_comment = (char *)vComment.c_str();

        // set the number of resolution levels
        int min_level_size = 50;
        unsigned int side_sz = bim::max<unsigned int>(info->width, info->height);
        if (tile_size > 0) {
            side_sz = tile_size;
            min_level_size = 16;
        }

        // Note that the smallest legal tile size is 1:
        parameters.numresolution = std::max(1, static_cast<int>(ceil(bim::log2<double>(side_sz)) - ceil(bim::log2<double>(min_level_size))) + 1);

        if (fmtHndl->quality >= 100) { // lossless mode
            parameters.irreversible = 0;
            parameters.tcp_rates[0] = 0;
        } else {
            parameters.irreversible = 1;
            parameters.tcp_rates[0] = 100.0 - fmtHndl->quality;
            //parameters.tcp_distoratio[0] = (double) fmtHndl->quality / 100.0;
            //parameters.cp_fixed_quality = OPJ_TRUE;
        }

        // select tiled writing
        if (tile_size > 0) {
            parameters.tile_size_on = OPJ_TRUE;
            parameters.cp_tx0 = 0;
            parameters.cp_ty0 = 0;
            parameters.cp_tdx = tile_size;
            parameters.cp_tdy = tile_size;
        }

        // initialize image components
        std::vector<opj_image_cmptparm_t> cmptparm(info->samples);
        memset(&cmptparm[0], 0, info->samples * sizeof(opj_image_cmptparm_t));
        for (bim::uint64 i = 0; i < info->samples; i++) {
            cmptparm[i].dx = parameters.subsampling_dx;
            cmptparm[i].dy = parameters.subsampling_dy;
            cmptparm[i].w = info->width;
            cmptparm[i].h = info->height;
            cmptparm[i].prec = info->depth;
            cmptparm[i].bpp = info->depth;
            cmptparm[i].sgnd = (info->pixelType == FMT_SIGNED) ? 1 : 0;
        }

        // dima: we need proper color space management
        OPJ_COLOR_SPACE color_space = OPJ_CLRSPC_UNSPECIFIED;
        if (info->imageMode == IM_RGB) {
            //color_space = OPJ_CLRSPC_SRGB;
        } else if (info->imageMode == IM_YUV) {
            color_space = OPJ_CLRSPC_SYCC;
        } else if (info->imageMode == IM_CMYK) {
            color_space = OPJ_CLRSPC_CMYK;
        }

        par->image = opj_image_create(info->samples, &cmptparm[0], color_space);
        if (!par->image) {
            throw(std::runtime_error("Could not create JP2 image buffer"));
        }

        jp2SetWriteMetadata(par->image, fmtHndl->metaData);

        // set image offset and reference grid
        par->image->x0 = parameters.image_offset_x0;
        par->image->y0 = parameters.image_offset_y0;
        par->image->x1 = parameters.image_offset_x0 + (info->width - 1) * parameters.subsampling_dx + 1;
        par->image->y1 = parameters.image_offset_y0 + (info->height - 1) * parameters.subsampling_dy + 1;

        for (bim::uint64 s = 0; s < info->samples; ++s) {
            if (info->depth == 8)
                copy_from_component<bim::uint8>(info->width, info->height, s, bmp->bits[s], par->image->comps[s]);
            else if (info->depth == 16)
                copy_from_component<bim::uint16>(info->width, info->height, s, bmp->bits[s], par->image->comps[s]);
            else if (info->depth == 32)
                copy_from_component<bim::uint32>(info->width, info->height, s, bmp->bits[s], par->image->comps[s]);
        } // for sample

        // setup the encoder parameters using the current image and using user parameters
        opj_setup_encoder(par->codec, &parameters, par->image);

        // encode the image
        if (!opj_start_compress(par->codec, par->image, par->stream)) {
            throw(std::runtime_error("Failed to init the encoder"));
        }

        if (!opj_encode(par->codec, par->stream)) {
            throw(std::runtime_error("Failed to encode the image"));
        }

        if (!opj_end_compress(par->codec, par->stream)) {
            throw(std::runtime_error("Failed to finilize the compression"));
        }
    } catch (const std::exception &vEx) {
#ifdef DEBUG
        std::cerr
            << "Error in jp2WriteImageProc(): " << vEx.what() << std::endl;
#endif
        return 1;
    }

    return 0;
}

//----------------------------------------------------------------------------
// Metadata hash
//----------------------------------------------------------------------------

bim::uint jp2_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (!hash) return 1;
    if (isCustomReading(fmtHndl)) return 1;
    if (!fmtHndl->fileName) return 1;
    bim::JP2Params *par = (bim::JP2Params *)fmtHndl->internalParams;

    // append image pyramid meta
    ImageInfo *info = &par->i;
    double scale = 1.0;
    std::vector<double> scales;
    double tw = info->tileWidth;
    double th = info->tileHeight;
    std::vector<int> tile_sizes_w;
    std::vector<int> tile_sizes_h;
    for (bim::uint64 i = 0; i < info->number_levels; ++i) {
        scales.push_back(scale);
        tile_sizes_w.push_back(bim::round<bim::uint>(tw));
        tile_sizes_h.push_back(bim::round<bim::uint>(th));
        scale /= 2.0;
        tw /= 2.0;
        th /= 2.0;
    }
    hash->set_value(bim::IMAGE_NUM_RES_L, (int)info->number_levels);
    hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(scales, ","));
    hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_FLAT);
    hash->set_value(bim::TILE_SIZE_X, xstring::join(tile_sizes_w, ","));
    hash->set_value(bim::TILE_SIZE_Y, xstring::join(tile_sizes_h, ","));

    bim::xstring tag_270;
    for (size_t comidx = 0; comidx < par->comments.size(); ++comidx) {
        const xstring &currcomment = par->comments[comidx];
        // NOTE: if comments contain an '=' sign, they are 'key=value' strings and
        // should be split on the '=' sign:
        const size_t pos = currcomment.find('=');
        if (currcomment.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!-- Warning: this comment is an OME-XML metadata block")) {
            tag_270 = currcomment;
        } else if (pos != std::string::npos) {
            const xstring currcomment_key = currcomment.substr(0, pos);
            const xstring currcomment_value = currcomment.substr(pos + 1, std::string::npos);
#ifdef DEBUG
            std::cerr << "jp2_append_metadata(): Will split comment into '" << currcomment_key << "' and '" << currcomment_value << "'." << std::endl;
#endif
            hash->set_value("JP2/" + currcomment_key, currcomment_value);
        } else {
            hash->set_value("JP2/Comment", currcomment);
        }
    }

    if (!tag_270.empty()) {
        // TODO FIXME: channels and number_z are currently hardcoded to 1:
        ome_xml_append_metadata(tag_270, 1, 1, hash);
    }

// Append EXIV2 metadata
#ifdef BIM_USE_EXIV2
    exiv_append_metadata(fmtHndl, hash);
#endif

    // append other metadata
    if (par->buffer_icc.size() > 0) {
        hash->set_value(bim::RAW_TAGS_ICC, par->buffer_icc, bim::RAW_TYPES_ICC);
        lcms_append_metadata(fmtHndl, hash);
    }

    return 0;
}

//****************************************************************************
// exported
//****************************************************************************

#define BIM_JP2_NUM_FORMATS 1

FormatItem jp2Items[BIM_JP2_NUM_FORMATS] = {
    {                         //0
      "JP2",                  // short name, no spaces
      "JPEG2000 File Format", // Long format name
      "jp2|j2k|jpx|jpc|jpt",  // pipe "|" separated supported extension list
      1,                      //canRead;      // 0 - NO, 1 - YES
      1,                      //canWrite;     // 0 - NO, 1 - YES
      1,                      //canReadMeta;  // 0 - NO, 1 - YES
      1,                      //canWriteMeta; // 0 - NO, 1 - YES
      0,                      //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 1, 0, 0, 0, 0, 1 } }
};


FormatHeader jp2Header = {

    sizeof(FormatHeader),
    "2.1.0",
    "JPEG-2000",
    "JP2-JFIF Compliant CODEC",

    BIM_FORMAT_JP2_MAGIC_SIZE,
    { 1, BIM_JP2_NUM_FORMATS, jp2Items },

    jp2ValidateFormatProc,
    // begin
    jp2AquireFormatProc, //AquireFormatProc
    // end
    jp2ReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    jp2OpenImageProc,  //OpenImageProc
    jp2CloseImageProc, //CloseImageProc

    // info
    jp2GetNumPagesProc,  //GetNumPagesProc
    jp2GetImageInfoProc, //GetImageInfoProc


    // read/write
    jp2ReadImageProc,      //ReadImageProc
    jp2WriteImageProc,     //WriteImageProc
    jp2ReadImageTileProc,  //ReadImageTileProc
    NULL,                  //WriteImageTileProc
    jp2ReadImageLevelProc, //ReadImageLevelProc
    NULL,                  //WriteImageLineProc
    NULL,                  //ReadImageThumbProc
    NULL,                  //WriteImageThumbProc
    NULL,                  //, //ReadImagePreviewProc
    NULL,                  //ReadImageRegionProc
    NULL,                  //WriteImageRegionProc

    // meta data
    NULL,                //ReadMetaDataAsTextProc
    jp2_append_metadata, //AppendMetaDataProc

    NULL,
    NULL,
    ""

};

extern "C" {

FormatHeader *jp2GetFormatHeader(void) {
    return &jp2Header;
}

} // extern C
