/*****************************************************************************
  TIFF IO
  Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>

  IMPLEMENTATION

  Programmer: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  TODO:
    4) read preview image in xRGB 8bit

  History:
    03/29/2004 22:23 - First creation

  Ver : 1
*****************************************************************************/

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>

#include <bim_image.h>
#include <bim_img_format_utils.h>
#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>
#ifdef BIM_USE_EXIV2
#include <bim_exiv_parse.h>
#endif

#include "bim_geotiff_parse.h"
#include "bim_tiff_format.h"
#include "bim_tiny_tiff.h"
#include "memio.h"
#include "xtiffio.h"

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

#include <pugixml.hpp>

// Disables Visual Studio 2005 warnings for deprecated code
#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
#pragma warning(disable : 4996)
#endif

bim::uint append_metadata_ometiff(bim::FormatHandle *fmtHndl, bim::TagMap *hash);
bim::uint omeTiffReadPlane(bim::FormatHandle *fmtHndl, bim::TiffParams *par, int plane);
int omeTiffWritePlane(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::ImageBitmap *img = NULL, bool subscale = false);


// must include these guys here if not no access to internal TIFF structs

//bim::uint stkReadMetaMeta(bim::FormatHandle *fmtHndl, int group, int tag, int type);
void stkParseUIC1Tag(bim::TiffParams *pars);
bim::uint append_metadata_stk(bim::FormatHandle *fmtHndl, bim::TagMap *hash);
bim::uint stkReadPlane(bim::TiffParams *tiffParams, int plane, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl);

bim::uint append_metadata_psia(bim::FormatHandle *fmtHndl, bim::TagMap *hash);
bim::uint psiaReadPlane(bim::FormatHandle *fmtHndl, bim::TiffParams *tiffParams, int plane, bim::ImageBitmap *img);

bim::uint append_metadata_fluoview(bim::FormatHandle *fmtHndl, bim::TagMap *hash);
bim::uint fluoviewReadPlane(bim::FormatHandle *fmtHndl, bim::TiffParams *tiffParams, int plane);

bim::uint append_metadata_lsm(bim::FormatHandle *fmtHndl, bim::TagMap *hash);


//****************************************************************************
// color conversion procs
//****************************************************************************

template<typename T>
void invert_buffer(void *buf, const bim::uint64 &size) {
    T maxval = std::numeric_limits<T>::max();
    T *p = (T *)buf;
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (size>BIM_OMP_FOR1)
    for (bim::uint64 i = 0; i < size; i++)
        p[i] = maxval - p[i];
}

static void invert_buffer_1bit(void *buf, const bim::uint64 size) {
    int maxval = 1;
    int rest = size % 8;
    bim::uint64 w = size / 8;
    unsigned char *p = (unsigned char *)buf;
    if (rest > 0) ++w;

    for (bim::uint64 x = 0; x < w; ++x) {
        unsigned char b[8];
        b[0] = maxval - (p[x] >> 7);
        b[1] = maxval - ((p[x] & 0x40) >> 6);
        b[2] = maxval - ((p[x] & 0x20) >> 5);
        b[3] = maxval - ((p[x] & 0x10) >> 4);
        b[4] = maxval - ((p[x] & 0x08) >> 3);
        b[5] = maxval - ((p[x] & 0x04) >> 2);
        b[6] = maxval - ((p[x] & 0x02) >> 1);
        b[7] = maxval - (p[x] & 0x01);
        p[x] = (b[0] << 7) + (b[1] << 6) + (b[2] << 5) + (b[3] << 4) + (b[4] << 3) + (b[5] << 2) + (b[6] << 1) + b[7];
    } // for x
}

static void invert_buffer_4bit(void *buf, const bim::uint64 size) {
    int maxval = 15;
    bool even = (size % 2 == 0);
    bim::uint64 w = size / 2;
    unsigned char *p = (unsigned char *)buf;

    for (bim::uint64 x = 0; x < w; ++x) {
        unsigned char b1 = maxval - (p[x] >> 4);
        unsigned char b2 = maxval - (p[x] & 0x0F);
        p[x] = (b1 << 4) + b2;
    } // for x

    // do the last pixel if the size is not even
    if (!even) {
        unsigned char b1 = maxval - (p[w] >> 4);
        p[w] = (b1 << 4);
    }
}

void invertSample(bim::ImageBitmap *img, const int &sample) {
    bim::uint64 size = img->i.width * img->i.height;

    // all typed will fall here
    if (img->i.depth == 8 && img->i.pixelType == bim::FMT_UNSIGNED) {
        invert_buffer<bim::uint8>(img->bits[sample], size);
    } else if (img->i.depth == 8 && img->i.pixelType == bim::FMT_SIGNED) {
        invert_buffer<bim::int8>(img->bits[sample], size);
    } else if (img->i.depth == 16 && img->i.pixelType == bim::FMT_UNSIGNED) {
        invert_buffer<bim::uint16>(img->bits[sample], size);
    } else if (img->i.depth == 16 && img->i.pixelType == bim::FMT_SIGNED) {
        invert_buffer<bim::int16>(img->bits[sample], size);
    } else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_UNSIGNED) {
        invert_buffer<bim::uint32>(img->bits[sample], size);
    } else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_SIGNED) {
        invert_buffer<bim::int32>(img->bits[sample], size);
    } else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_FLOAT) {
        invert_buffer<bim::float32>(img->bits[sample], size);
    } else if (img->i.depth == 64 && img->i.pixelType == bim::FMT_FLOAT) {
        invert_buffer<bim::float64>(img->bits[sample], size);
    } else if (img->i.depth == 4 && img->i.pixelType == bim::FMT_UNSIGNED) { // we still have 1 and 4 bits
        invert_buffer_4bit(img->bits[sample], size);
    } else if (img->i.depth == 1 && img->i.pixelType == bim::FMT_UNSIGNED) {
        invert_buffer_1bit(img->bits[sample], size);
    }
}

void invertImg(bim::ImageBitmap *img) {
    if (!img) return;
    for (unsigned int sample = 0; sample < img->i.samples; sample++)
        invertSample(img, sample);
}

template<typename T>
void image_ycbcr_to_rgb(bim::ImageBitmap *img, bim::TiffParams *pars) {
#define uint32 bim::uint32
    TIFFYCbCrToRGB *ycbcr = (TIFFYCbCrToRGB *)_TIFFmalloc(
        TIFFroundup_32(sizeof(TIFFYCbCrToRGB), sizeof(long)) + 4 * 256 * sizeof(TIFFRGBValue) + 2 * 256 * sizeof(int) + 3 * 256 * sizeof(bim::int32));
#undef uint32
    if (!ycbcr) return;

    bim::uint64 size = img->i.width * img->i.height;
    float *luma, *refBlackWhite;
    TIFFGetFieldDefaulted(pars->tiff, TIFFTAG_YCBCRCOEFFICIENTS, &luma);
    TIFFGetFieldDefaulted(pars->tiff, TIFFTAG_REFERENCEBLACKWHITE, &refBlackWhite);
    if (TIFFYCbCrToRGBInit(ycbcr, luma, refBlackWhite) >= 0) {
        T *BIM_RESTRICT rp = (T *)img->bits[0];
        T *BIM_RESTRICT gp = (T *)img->bits[1];
        T *BIM_RESTRICT bp = (T *)img->bits[2];
        #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (size > BIM_OMP_FOR1)
        for (bim::int64 i = 0; i < size; i++) {
            bim::uint32 r, g, b;
            bim::uint32 Y = rp[i];
            bim::int32 Cb = gp[i];
            bim::int32 Cr = bp[i];
            TIFFYCbCrtoRGB(ycbcr, Y, Cb, Cr, &r, &g, &b);
            rp[i] = r;
            gp[i] = g;
            bp[i] = b;
        }
    }
    _TIFFfree(ycbcr);
}

void imageYCbCr2RGB(bim::ImageBitmap *img, bim::TiffParams *pars) {
    if (img->i.depth == 8 && img->i.pixelType == bim::FMT_UNSIGNED)
        image_ycbcr_to_rgb<bim::uint8>(img, pars);
    else if (img->i.depth == 8 && img->i.pixelType == bim::FMT_SIGNED)
        image_ycbcr_to_rgb<bim::int8>(img, pars);
    else if (img->i.depth == 16 && img->i.pixelType == bim::FMT_UNSIGNED)
        image_ycbcr_to_rgb<bim::uint16>(img, pars);
    else if (img->i.depth == 16 && img->i.pixelType == bim::FMT_SIGNED)
        image_ycbcr_to_rgb<bim::int16>(img, pars);
    else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_UNSIGNED)
        image_ycbcr_to_rgb<bim::uint32>(img, pars);
    else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_SIGNED)
        image_ycbcr_to_rgb<bim::int32>(img, pars);
}


template<typename T>
void image_cielab_to_rgb(bim::ImageBitmap *img, bim::TiffParams *pars) {
    TIFFCIELabToRGB *cielab = (TIFFCIELabToRGB *)_TIFFmalloc(sizeof(TIFFCIELabToRGB));
    if (!cielab) return;

    TIFFDisplay display_sRGB = {
        { // XYZ -> luminance matrix
          { 3.2410F, -1.5374F, -0.4986F },
          { -0.9692F, 1.8760F, 0.0416F },
          { 0.0556F, -0.2040F, 1.0570F } },
        100.0F,
        100.0F,
        100.0F, // Light o/p for reference white
        255,
        255,
        255, // Pixel values for ref. white
        1.0F,
        1.0F,
        1.0F, // Residual light o/p for black pixel
        2.4F,
        2.4F,
        2.4F, // Gamma values for the three guns
    };

    bim::uint64 size = img->i.width * img->i.height;
    float *whitePoint;
    float refWhite[3];
    TIFFGetFieldDefaulted(pars->tiff, TIFFTAG_WHITEPOINT, &whitePoint);
    refWhite[1] = 100.0F;
    refWhite[0] = whitePoint[0] / whitePoint[1] * refWhite[1];
    refWhite[2] = (1.0F - whitePoint[0] - whitePoint[1]) / whitePoint[1] * refWhite[1];
    if (TIFFCIELabToRGBInit(cielab, &display_sRGB, refWhite) >= 0) {
        T *BIM_RESTRICT rp = (T *)img->bits[0];
        T *BIM_RESTRICT gp = (T *)img->bits[1];
        T *BIM_RESTRICT bp = (T *)img->bits[2];
        #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (size > BIM_OMP_FOR1)
        for (bim::int64 i = 0; i < size; i++) {
            bim::uint32 r, g, b;
            float X, Y, Z;
            bim::uint32 L = rp[i];
            bim::int32 A = gp[i];
            bim::int32 B = bp[i];
            TIFFCIELabToXYZ(cielab, L, A, B, &X, &Y, &Z);
            TIFFXYZToRGB(cielab, X, Y, Z, &r, &g, &b);
            rp[i] = r;
            gp[i] = g;
            bp[i] = b;
        }
    }
    _TIFFfree(cielab);
}

void imageCIELAB2RGB(bim::ImageBitmap *img, bim::TiffParams *pars) {
    if (img->i.depth == 8 && img->i.pixelType == bim::FMT_UNSIGNED)
        image_cielab_to_rgb<bim::uint8>(img, pars);
    else if (img->i.depth == 8 && img->i.pixelType == bim::FMT_SIGNED)
        image_cielab_to_rgb<bim::int8>(img, pars);
    else if (img->i.depth == 16 && img->i.pixelType == bim::FMT_UNSIGNED)
        image_cielab_to_rgb<bim::uint16>(img, pars);
    else if (img->i.depth == 16 && img->i.pixelType == bim::FMT_SIGNED)
        image_cielab_to_rgb<bim::int16>(img, pars);
    else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_UNSIGNED)
        image_cielab_to_rgb<bim::uint32>(img, pars);
    else if (img->i.depth == 32 && img->i.pixelType == bim::FMT_SIGNED)
        image_cielab_to_rgb<bim::int32>(img, pars);
}

void processPhotometric(bim::ImageBitmap *img, bim::TiffParams *pars, const bim::uint16 &photometric) {
    TIFF *tif = pars->tiff;
    if (photometric == PHOTOMETRIC_MINISWHITE) {
        invertImg(img);
    } else if (photometric == PHOTOMETRIC_YCBCR && img->i.depth <= 32 && img->i.samples == 3) {
        imageYCbCr2RGB(img, pars);
    } else if (photometric == PHOTOMETRIC_CIELAB && img->i.depth <= 32 && img->i.samples == 3) {
        //imageCIELAB2RGB(img, pars); // dima: not tested - will be added with test images
    }
}


//****************************************************************************
// MISC
//****************************************************************************

bool areValidParams(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams) {
    if (fmtHndl == NULL) return false;
    if (tifParams == NULL) return false;
    if (tifParams->tiff == NULL) return false;
    if (fmtHndl->image == NULL) return false;

    return true;
}

void init_image_palette(TIFF *tif, bim::ImageInfo *info) {
    if (tif == NULL) return;
    if (info == NULL) return;
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 bitspersample = 1;
    bim::uint16 samplesperpixel = 1;

    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);

    info->lut.count = 0;
    for (bim::uint i = 0; i < 256; i++)
        info->lut.rgba[i] = bim::xRGB(i, i, i);

    if (photometric == PHOTOMETRIC_PALETTE) { // palette
        bim::uint16 *red, *green, *blue;
        bim::uint num_colors = (1L << bitspersample);
        if (num_colors > 256) num_colors = 256;

        TIFFGetField(tif, TIFFTAG_COLORMAP, &red, &green, &blue);
        for (bim::uint i = 0; i < num_colors; i++)
            info->lut.rgba[i] = bim::xRGB(red[i] / 256, green[i] / 256, blue[i] / 256);

        info->lut.count = num_colors;

        if (info->lut.count < 1 && samplesperpixel == 1) {
            info->imageMode = bim::IM_GRAYSCALE;
        }
    } // if paletted
}

//----------------------------------------------------------------------------
// METADATA
//----------------------------------------------------------------------------

void pyramid_append_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return;
    if (isCustomReading(fmtHndl)) return;
    if (!hash) return;
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    bim::PyramidInfo *pyramid = &par->pyramid;

    hash->set_value(bim::IMAGE_NUM_RES_L, pyramid->number_levels);
    if (pyramid->number_levels < 1) return;

    hash->set_value(bim::IMAGE_RES_L_SCALES, bim::xstring::join(pyramid->scales, ","));
    hash->set_value(bim::TILE_SIZE_X, bim::xstring::join(pyramid->tile_sizes_w, ","));
    hash->set_value(bim::TILE_SIZE_Y, bim::xstring::join(pyramid->tile_sizes_h, ","));
}

void generic_append_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return;
    if (isCustomReading(fmtHndl)) return;
    if (!hash) return;
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    TIFF *tif = par->tiff;
    bim::ImageInfo *info = &par->info;

    bim::uint32 sz = 0;
    char *buf = NULL;

    // ICC profile
    if (TIFFGetField(tif, TIFFTAG_ICCPROFILE, &sz, &buf)) {
        hash->set_value(bim::RAW_TAGS_ICC, buf, sz, bim::RAW_TYPES_ICC);
        lcms_append_metadata(fmtHndl, hash);
    }

    // Pallete
    if (info->lut.count > 0) {
        std::vector<int> lut(info->lut.count, 0);
        for (bim::uint i = 0; i < info->lut.count; i++) {
            lut[i] = info->lut.rgba[i];
        }
        hash->set_value(bim::RAW_TAGS_PALETTE, lut);
    }

    // IPTC: libtiff does not seem to be reading????
    sz = 0;
    buf = NULL;
    if (TIFFGetField(tif, TIFFTAG_RICHTIFFIPTC, &sz, &buf)) {
        if (TIFFIsByteSwapped(tif) != 0)
            TIFFSwabArrayOfLong((bim::uint32 *)buf, (unsigned long)sz);
        hash->set_value(bim::RAW_TAGS_IPTC, buf, sz * 4, bim::RAW_TYPES_IPTC);
    } else if (par->ifds.tagPresentInFirstIFD(TIFFTAG_RICHTIFFIPTC)) {
        TinyTiff::IFD *ifd = par->ifds.firstIfd();
        if (ifd) {
            TinyTiff::Entry *e = ifd->getTag(TIFFTAG_RICHTIFFIPTC);
            std::vector<char> v(e->count);
            int r = ifd->readBufNoAlloc(e->offset, e->count, TIFF_BYTE, (unsigned char *)&v[0]);
            if (TIFFIsByteSwapped(tif) != 0)
                TIFFSwabArrayOfLong((bim::uint32 *)&v[0], (unsigned long)v.size() / 4);
            hash->set_value(bim::RAW_TAGS_IPTC, v, bim::RAW_TYPES_IPTC);
        }
    }

    // XMP
    sz = 0;
    buf = NULL;
    if (TIFFGetField(tif, TIFFTAG_XMLPACKET, &sz, &buf)) {
        hash->set_value(bim::RAW_TAGS_XMP, buf, sz, bim::RAW_TYPES_XMP);
    }

    // PHOTOSHOP
    sz = 0;
    buf = NULL;
    if (TIFFGetField(tif, TIFFTAG_PHOTOSHOP, &sz, &buf)) {
        hash->set_value(bim::RAW_TAGS_PHOTOSHOP, buf, sz, bim::RAW_TYPES_PHOTOSHOP);
    }

// EXIF requires constructing a tiny tiff stream with embedded EXIF and GPS IFDs
#ifdef BIM_USE_EXIV2
    tiff_exif_to_buffer(tif, hash);
#endif

    // GEOTIFF
    if (isGeoTiff(tif)) {
        std::vector<char> buffer;
        BufferFromGTIF(tif, buffer);
        hash->set_value(bim::RAW_TAGS_GEOTIFF, buffer, bim::RAW_TYPES_GEOTIFF);
    }
}

void generic_write_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return;
    if (isCustomReading(fmtHndl)) return;
    if (!hash) return;
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    TIFF *tif = par->tiff;

    // ICC profile
    if (hash->hasKey(bim::RAW_TAGS_ICC) && hash->get_type(bim::RAW_TAGS_ICC) == bim::RAW_TYPES_ICC) {
        TIFFSetField(tif, TIFFTAG_ICCPROFILE, hash->get_size(bim::RAW_TAGS_ICC), hash->get_value_bin(bim::RAW_TAGS_ICC));
    }

    // IPTC
    if (hash->hasKey(bim::RAW_TAGS_IPTC) && hash->get_type(bim::RAW_TAGS_IPTC) == bim::RAW_TYPES_IPTC) {
        TIFFSetField(tif, TIFFTAG_RICHTIFFIPTC, hash->get_size(bim::RAW_TAGS_IPTC) / 4, hash->get_value_bin(bim::RAW_TAGS_IPTC));
    }

    // XMP
    if (hash->hasKey(bim::RAW_TAGS_XMP) && hash->get_type(bim::RAW_TAGS_XMP) == bim::RAW_TYPES_XMP) {
        TIFFSetField(tif, TIFFTAG_XMLPACKET, hash->get_size(bim::RAW_TAGS_XMP), hash->get_value_bin(bim::RAW_TAGS_XMP));
    }

    // PHOTOSHOP
    if (hash->hasKey(bim::RAW_TAGS_PHOTOSHOP) && hash->get_type(bim::RAW_TAGS_PHOTOSHOP) == bim::RAW_TYPES_PHOTOSHOP) {
        TIFFSetField(tif, TIFFTAG_PHOTOSHOP, hash->get_size(bim::RAW_TAGS_PHOTOSHOP), hash->get_value_bin(bim::RAW_TAGS_PHOTOSHOP));
    }

    // EXIF requires parsing a tiny tiff stream with embedded EXIF and GPS IFDs
    // dima: so far libtiff can't write exif and gps ifds, we need to use some true internals
    // should we write bigtiff ifds for exif and gps into bigtiff?
    // maybe we should simply consider them as binary data
    if (hash->hasKey(bim::RAW_TAGS_EXIF) && hash->get_type(bim::RAW_TAGS_EXIF) == bim::RAW_TYPES_EXIF) {
        //buffer_to_tiff_exif(hash, tif);
    }

    // GEOTIFF
    if (hash->hasKey(bim::RAW_TAGS_GEOTIFF) && hash->get_type(bim::RAW_TAGS_GEOTIFF) == bim::RAW_TYPES_GEOTIFF) {
        GTIFFromBuffer(hash->get_value_vec(bim::RAW_TAGS_GEOTIFF), tif);
    }

    // OME-TIFF
    if (hash->hasKey(bim::RAW_TAGS_OMEXML)) {
        TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, hash->get_value_bin(bim::RAW_TAGS_OMEXML));
    }
}

bim::uint append_metadata_generic_tiff(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;

    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;

    std::map<int, std::string> hash_tiff_tags;
    hash_tiff_tags[269] = "Document Name";
    hash_tiff_tags[270] = "Image Description";
    hash_tiff_tags[285] = "Page Name";
    hash_tiff_tags[271] = "Make";
    hash_tiff_tags[272] = "Model";
    hash_tiff_tags[305] = "Software";
    hash_tiff_tags[306] = "Date Time";
    hash_tiff_tags[315] = "Artist";
    hash_tiff_tags[316] = "Host Computer";

    std::map<int, std::string>::const_iterator it = hash_tiff_tags.begin();
    while (it != hash_tiff_tags.end()) {
        bim::xstring tag_str = ifd->readTagString(it->first);
        if (tag_str.size() > 0) hash->set_value(bim::xstring("TIFF/") + it->second, tag_str);
        it++;
    }

    geotiff_append_metadata(fmtHndl, hash);
    pyramid_append_metadata(fmtHndl, hash);
    //generic_append_metadata(fmtHndl, hash); // always appaned these generic fields
#ifdef BIM_USE_EXIV2
    exiv_append_metadata(fmtHndl, hash);
#endif

    return 0;
}

bim::uint append_metadata_qimaging_tiff(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;

    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;

    /*
  [Image Description]
  Exposure: 000 : 00 : 00 . 300 : 000
  Binning: 2 x 2
  Gain: 2.000000
  %Accumulated%=0

  [Software]
  QCapture Pro

  [Date Time]
  08/28/2006 04:34:47.000 PM
    */

    // check if it's QImage tiff file
    // should exist private tags 50288 and 50296
    if (!ifd->tagPresent(50288)) return 0;
    if (!ifd->tagPresent(50296)) return 0;

    // tag 305 should be "QCapture Pro"
    bim::xstring tag_software = ifd->readTagString(305);
    if (tag_software != "QCapture Pro") return 0;

    // ok, we're sure it's QImaging
    hash->set_value("qImaging/Software", tag_software);


    bim::xstring tag_description = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_description.size() > 0)
        hash->parse_ini(tag_description, ":", "qImaging/");

    // read tag 306 - Date/Time
    bim::xstring tag_datetime = ifd->readTagString(306);
    if (tag_datetime.size() > 0) {
        int y = 0, m = 0, d = 0, h = 0, mi = 0, s = 0, ms = 0;
        char ampm = 0;
        //08/28/2006 04:34:47.000 PM
        sscanf((char *)tag_datetime.c_str(), "%d/%d/%d %d:%d:%d.%d %c", &m, &d, &y, &h, &mi, &s, &ms, &ampm);
        if (ampm == 'P') h += 12;
        tag_datetime.sprintf("%.4d-%.2d-%.2d %.2d:%.2d:%.2d", y, m, d, h, mi, s);
        hash->set_value(bim::IMAGE_DATE_TIME, tag_datetime);
    }

    //hash->set_value( "pixel_resolution_x", par->pixel_size[0] );
    //hash->set_value( "pixel_resolution_y", par->pixel_size[1] );
    //hash->set_value( "pixel_resolution_z", par->pixel_size[2] );

    return 0;
}


void parse_metaimaging_description(bim::xstring &txt, bim::xstring path, bim::TagMap *hash) {
    bim::xstring descr;
    for (const bim::xstring line : txt.replace("\r", "").split("\n")) {
        bim::xstring l = line.removeSpacesBoth();
        std::vector<bim::xstring> nv = l.split(":");
        if (nv.size() == 2) {
            hash->set_value(path + "/" + nv[0], nv[1]);
        } else if (l.size() > 4) {
            descr += l;
            descr += "\n";
        }
    }
    if (descr.size() > 0) {
        hash->set_value(path + "/Note", descr);
    }
}

void walker_metaimaging(pugi::xml_node node, bim::xstring path, bim::TagMap *hash) {
    // iterate over tags
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        bim::xstring tag = child.name();
        if (tag != "prop" && tag != "custom-prop") continue;

        bim::xstring name = child.attribute("id").value();
        name = name.removeSpacesBoth();
        if (name.size() > 0) {
            // this is a tag
            bim::xstring val = child.attribute("value").value();
            bim::xstring tp = child.attribute("type").value();
            if (tp == "float") {
                hash->set_value(path + "/" + name, val.toDouble());
            } else if (tp == "int") {
                hash->set_value(path + "/" + name, val.toInt());
            } else {
                hash->set_value(path + "/" + name, val);
            }
        }
    }

    // iterate over folders
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        bim::xstring tag = child.name();
        if (tag == "prop" || tag == "custom-prop") continue;
        walker_metaimaging(child, path + "/" + tag, hash);
    }
}

void parse_metaimaging_UIC1(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    stkParseUIC1Tag(par);

    hash->append_tags(par->stkInfo.tags, "MetaMorph/");
    if (par->stkInfo.tags.size() > 0) {
        hash->append_tag("MetaMorph/ApplicationName", "MetaMorph");
    }
}

void parse_metaimaging_xml(bim::FormatHandle *fmtHndl, bim::TagMap *hash, bim::xstring &tag_description) {
    // parse metaimaging XML doc into custom tags
    pugi::xml_document doc;
    if (doc.load_buffer(tag_description.c_str(), tag_description.size())) {
        pugi::xml_node child = doc.first_child();
        bim::xstring path = "MetaMorph";
        walker_metaimaging(doc.first_child(), path, hash);
    }

    // parse Description tags
    if (hash->hasKey("MetaMorph/Description")) {
        bim::xstring txt = hash->get_value("MetaMorph/Description");
        hash->erase_tag("MetaMorph/Description");
        bim::xstring path = "MetaMorph/Description";
        parse_metaimaging_description(txt, path, hash);
    }
}

void parse_metaimaging_old(bim::FormatHandle *fmtHndl, bim::TagMap *hash, bim::xstring &tag_description) {
    // parse description
    parse_metaimaging_description(tag_description, "MetaMorph/Description", hash);

    // read and parse TIFFTAG_STK_UIC1
    parse_metaimaging_UIC1(fmtHndl, hash);
}

bim::uint append_metadata_metaimaging(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;

    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;

    // first detect if the file is not a proper STK format
    // older MetaImaging may contain TIFFTAG_STK_UIC1 but does not contain either of teh other 3 STK tags
    if (ifd->tagPresent(TIFFTAG_STK_UIC2)) return 1;
    if (ifd->tagPresent(TIFFTAG_STK_UIC3)) return 1;
    if (ifd->tagPresent(TIFFTAG_STK_UIC4)) return 1;

    // tag 305 should be "MetaSeries"
    bim::xstring tag_software = ifd->readTagString(TIFFTAG_SOFTWARE);
    //if (!tag_software.startsWith("MetaSeries")) return 0;

    // ok, we're sure it is MetaSeries
    if (tag_software.size() > 0)
        hash->set_value(bim::DOCUMENT_APPLICATION, tag_software);

    bim::xstring tag_description = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_description.size() < 1) return 0;

    if (tag_description.contains("<MetaData")) { // modern XML format
        parse_metaimaging_xml(fmtHndl, hash, tag_description);
    } else if (ifd->tagPresent(TIFFTAG_STK_UIC1)) { // old text + 33628 tag format
        parse_metaimaging_old(fmtHndl, hash, tag_description);
    }

    if (hash->hasKey("MetaMorph/ApplicationName") || hash->hasKey("MetaMorph/Name")) {
        par->subType = bim::tstMetaImaging;
        hash->set_value(bim::DOCUMENT_VENDOR, "Molecular Devices");
    } else {
        return 1;
    }

    //-------------------------------------------------------------------
    // first parse old-style tags
    //-------------------------------------------------------------------

    hash->set_value_from_old_key("MetaMorph/XCalibration", bim::PIXEL_RESOLUTION_X);
    hash->set_value_from_old_key("MetaMorph/YCalibration", bim::PIXEL_RESOLUTION_Y);
    hash->set_value_from_old_key("MetaMorph/Z Step", bim::PIXEL_RESOLUTION_Z);
    hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_X);
    hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_Y);
    hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_Z);

    //hash->set_value_from_old_key("MetaMorph/ApplicationName", bim::DOCUMENT_APPLICATION);
    //hash->set_value_from_old_key("MetaMorph/ApplicationVersion", bim::DOCUMENT_APPLICATION_VERSION);
    //hash->set_value_from_old_key("MetaMorph/MetaDataVersion", bim::DOCUMENT_VERSION);
    hash->set_value_from_old_key("MetaMorph/CreateTime", bim::DOCUMENT_DATETIME);

    // objective
    hash->set_value_from_old_key("MetaMorph/ImageXpress Micro Objective", bim::OBJECTIVE_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/_MagSetting_", bim::OBJECTIVE_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/_MagNA_", bim::OBJECTIVE_NUM_APERTURE);
    hash->set_value_from_old_key("MetaMorph/_MagRI_", bim::OBJECTIVE_REF_INDEX);
    bim::parse_objective_from_string(hash->get_value(bim::OBJECTIVE_DESCRIPTION), hash);

    // channel info
    bim::xstring channel_path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
    hash->set_value_from_old_key("MetaMorph/IXConfocal Module Dichroic Wheel", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key("MetaMorph/IXConfocal Module Emission Wheel", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key("MetaMorph/_IllumSetting_", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key(channel_path + bim::CHANNEL_INFO_NAME, channel_path + bim::CHANNEL_INFO_FLUOR);
    hash->set_value_from_old_key(channel_path + bim::CHANNEL_INFO_NAME, channel_path + bim::CHANNEL_INFO_DYE);
    hash->set_value_from_old_key("MetaMorph/UIC1wavelength", channel_path + bim::CHANNEL_INFO_EM_WAVELENGTH);
    hash->set_value_from_old_key("MetaMorph/Gamma", channel_path + bim::CHANNEL_INFO_GAMMA);
    bim::parse_exposure_from_string(hash->get_value("MetaMorph/Description/Exposure"), channel_path, hash);
    bim::parse_pinhole_from_string(hash->get_value("MetaMorph/IXConfocal Module Disk"), channel_path, hash);

    //-------------------------------------------------------------------
    // parse new style MetaMorph tags into libbioimage schema
    //-------------------------------------------------------------------

    hash->set_value_from_old_key("MetaMorph/PlaneInfo/spatial-calibration-x", bim::PIXEL_RESOLUTION_X);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/spatial-calibration-y", bim::PIXEL_RESOLUTION_Y);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/spatial-calibration-units", bim::PIXEL_RESOLUTION_UNIT_X);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/spatial-calibration-units", bim::PIXEL_RESOLUTION_UNIT_Y);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/Z Thickness", bim::PIXEL_RESOLUTION_Z);

    hash->set_value_from_old_key("MetaMorph/ApplicationName", bim::DOCUMENT_APPLICATION);
    hash->set_value_from_old_key("MetaMorph/ApplicationVersion", bim::DOCUMENT_APPLICATION_VERSION);
    hash->set_value_from_old_key("MetaMorph/MetaDataVersion", bim::DOCUMENT_VERSION);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/acquisition-time-local", bim::DOCUMENT_DATETIME);
    hash->set_value_from_old_key("MetaMorph/Description/Note", bim::DOCUMENT_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/Description/Plate Name", bim::DOCUMENT_ASSAY);
    hash->set_value_from_old_key("MetaMorph/Description/Plate Name", bim::DOCUMENT_PLATE);
    hash->set_value_from_old_key("MetaMorph/Description/Folder Name", bim::DOCUMENT_SCREEN);
    hash->set_value_from_old_key("MetaMorph/Description/Barcode", bim::DOCUMENT_BARCODE);

    // objective
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/ImageXpress Micro Objective", bim::OBJECTIVE_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/_MagSetting_", bim::OBJECTIVE_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/_MagNA_", bim::OBJECTIVE_NUM_APERTURE);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/_MagRI_", bim::OBJECTIVE_REF_INDEX);
    bim::parse_objective_from_string(hash->get_value(bim::OBJECTIVE_DESCRIPTION), hash);

    // channel info
    channel_path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/image-name", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/_IllumSetting_", channel_path + bim::CHANNEL_INFO_FLUOR);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/_IllumSetting_", channel_path + bim::CHANNEL_INFO_DYE);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/wavelength", channel_path + bim::CHANNEL_INFO_EM_WAVELENGTH);
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/gamma", channel_path + bim::CHANNEL_INFO_GAMMA);
    bim::parse_exposure_from_string(hash->get_value("MetaMorph/Description/Exposure"), channel_path, hash);
    bim::parse_pinhole_from_string(hash->get_value("MetaMorph/PlaneInfo/IXConfocal Module Disk"), channel_path, hash);


    // DECLARE_STR(CHANNEL_INFO_COLOR_RANGE, "range")
    //MetaMorph/PlaneInfo/scale-min: 645
    //MetaMorph/PlaneInfo/scale-max: 20160

    // MetaMorph/PlaneInfo/stage-label: D07
    hash->set_value_from_old_key("MetaMorph/PlaneInfo/stage-label", bim::DOCUMENT_WELL_NAME);

    /*
    MetaMorph/PlaneInfo/stage-position-x: 68636.0
    MetaMorph/PlaneInfo/stage-position-y: 37706.0
    MetaMorph/PlaneInfo/z-position: 9196.12
    MetaMorph/PlaneInfo/camera-chip-offset-x: 0.0
    MetaMorph/PlaneInfo/camera-chip-offset-y: 0.0
    MetaMorph/PlaneInfo/_Temperature_: 30.2
    */

    pyramid_append_metadata(fmtHndl, hash);

    return 0;
}

bim::uint append_metadata_cellomics(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;

    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;

    if (!ifd->tagPresent(TIFFTAG_CELLOMICS_ROW)) return 1;

    //Make: Thermo Fisher Scientific, Inc
    //Model : Cellomics HCS Reader    
    bim::xstring tag_make = ifd->readTagString(TIFFTAG_MAKE);
    bim::xstring tag_model = ifd->readTagString(TIFFTAG_MODEL);
    if (tag_make.size() > 0)
        hash->set_value(bim::DOCUMENT_VENDOR, tag_make);
    if (tag_model.size() > 0)
        hash->set_value(bim::DOCUMENT_INSTRUMENT, tag_model);

    hash->set_value(bim::PIXEL_RESOLUTION_X, ifd->readTagDouble(TIFFTAG_CELLOMICS_XRES));
    hash->set_value(bim::PIXEL_RESOLUTION_Y, ifd->readTagDouble(TIFFTAG_CELLOMICS_YRES));
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, "microns");
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, "microns");

    int wi = ifd->readTagInt(TIFFTAG_CELLOMICS_ROW);
    int wj = ifd->readTagInt(TIFFTAG_CELLOMICS_COL);
    hash->set_value(bim::DOCUMENT_WELL_ROW, wi);
    hash->set_value(bim::DOCUMENT_WELL_COL, wj);
    hash->set_value(bim::DOCUMENT_WELL_IJ, bim::xstring::xprintf("%d,%d", wi, wj));
    hash->set_value(bim::DOCUMENT_WELL_NAME, bim::xstring::xprintf("%c%d", wi+65-1, wj));
    // DOCUMENT_WELL_SITE

    // unknown tags
    hash->set_value("Cellomics/tag_1004", ifd->readTagDouble(TIFFTAG_CELLOMICS_T1004));
    hash->set_value("Cellomics/tag_1005", ifd->readTagDouble(TIFFTAG_CELLOMICS_T1005));
    hash->set_value("Cellomics/tag_1006", ifd->readTagDouble(TIFFTAG_CELLOMICS_T1006));
    hash->set_value("Cellomics/tag_1007", ifd->readTagDouble(TIFFTAG_CELLOMICS_T1007));
    hash->set_value("Cellomics/tag_1008", ifd->readTagDouble(TIFFTAG_CELLOMICS_T1008));
    hash->set_value("Cellomics/tag_1009", ifd->readTagDouble(TIFFTAG_CELLOMICS_T1009));

    pyramid_append_metadata(fmtHndl, hash);

    return 0;
}

bim::uint tiff_append_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    bim::TiffParams *pars = (bim::TiffParams *)fmtHndl->internalParams;
    bim::ImageInfo *info = &pars->info;

    // resolution
    if (info->resUnits == bim::RES_um) {
        hash->set_value(bim::PIXEL_RESOLUTION_X, info->xRes);
        hash->set_value(bim::PIXEL_RESOLUTION_Y, info->yRes);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    // spectral info
    if (pars->image_mode == bim::IM_SPECTRAL) {
        hash->set_value(bim::IMAGE_NUM_SPECTRA, pars->image_num_spectra);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_SPECTRAL);
    }

    append_metadata_qimaging_tiff(fmtHndl, hash);
    append_metadata_metaimaging(fmtHndl, hash);
    append_metadata_cellomics(fmtHndl, hash);
    generic_append_metadata(fmtHndl, hash);

    if (pars->subType == bim::tstStk)
        append_metadata_stk(fmtHndl, hash);
    else if (pars->subType == bim::tstPsia)
        append_metadata_psia(fmtHndl, hash);
    else if (pars->subType == bim::tstFluoview || pars->subType == bim::tstAndor)
        append_metadata_fluoview(fmtHndl, hash);
    else if (pars->subType == bim::tstCzLsm)
        append_metadata_lsm(fmtHndl, hash);
    else if (pars->subType == bim::tstOmeTiff || pars->subType == bim::tstOmeBigTiff)
        append_metadata_ometiff(fmtHndl, hash);
    else if (pars->subType == bim::tstMetaImaging) {
        // do nothing really
    } else {
        append_metadata_generic_tiff(fmtHndl, hash);
    }

    return 0;
}

//----------------------------------------------------------------------------
// Write METADATA
//----------------------------------------------------------------------------


bim::uint write_tiff_metadata(bim::FormatHandle *fmtHndl, bim::TiffParams *pars) {
    if (!areValidParams(fmtHndl, pars)) return 1;

    generic_write_metadata(fmtHndl, fmtHndl->metaData);

    return 0;
}


//****************************************************************************
// WRITING LINE SEGMENT FROM BUFFER
//****************************************************************************

template<typename T>
void write_line_segment_t(void *po, const void *bufo, bim::ImageBitmap *img, bim::uint sample, bim::uint64 w) {
    T *p = (T *)po;
    const T *buf = (const T *)bufo;
    bim::uint64 nsamples = img->i.samples;
    for (bim::uint64 x = sample, xi = 0; x < w * nsamples; x += nsamples, ++xi) {
        p[xi] = buf[x];
    }
}

void write_line_segment(void *po, void *bufo, bim::ImageBitmap *img, bim::uint sample, bim::uint64 w) {
    if (img->i.depth == 8) {
        write_line_segment_t<bim::uint8>(po, bufo, img, sample, w);
    } else if (img->i.depth == 16) {
        write_line_segment_t<bim::uint16>(po, bufo, img, sample, w);
    } else if (img->i.depth == 32) {
        write_line_segment_t<bim::uint32>(po, bufo, img, sample, w);
    } else if (img->i.depth == 64) {
        write_line_segment_t<bim::float64>(po, bufo, img, sample, w);
    }
}


//****************************************************************************
// SCANLINE METHOD TIFF
//****************************************************************************

int read_scanline_tiff(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl) {
    if (!tif || !img) return -1;

    bim::uint lineSize = getLineSizeInBytes(img);
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 planarConfig;
    TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &planarConfig);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);

    //TIFFReadEncodedStrip

    if ((planarConfig == PLANARCONFIG_SEPARATE) || (img->i.samples == 1)) {
        for (bim::uint64 sample = 0; sample < img->i.samples; sample++) {
            bim::uchar *p = (bim::uchar *)img->bits[sample];
            for (bim::uint64 y = 0; y < img->i.height; y++) {
                xprogress(fmtHndl, y * (sample + 1), img->i.height * img->i.samples, "Reading TIFF");
                if (xtestAbort(fmtHndl) == 1) break;
                if (!TIFFReadScanline(tif, p, y, sample)) return -1;
                p += lineSize;
            } // for y
        }     // for sample
    } else {  // if image contain several samples in one same plane ex: RGBRGBRGB...
        bim::uchar *buf = (bim::uchar *)_TIFFmalloc(TIFFScanlineSize64(tif));
        for (bim::uint64 y = 0; y < img->i.height; y++) {

            xprogress(fmtHndl, y, img->i.height, "Reading TIFF");
            if (xtestAbort(fmtHndl) == 1) break;

            if (!TIFFReadScanline(tif, buf, y, 0)) {
                _TIFFfree(buf);
                return -1;
            }
            // process YCrCb, etc data

            for (bim::uint64 sample = 0; sample < img->i.samples; ++sample) {
                bim::uchar *p = (bim::uchar *)img->bits[sample] + (lineSize * y);
                write_line_segment(p, buf, img, sample, img->i.width);
            } // for sample

        } // for y
        _TIFFfree(buf);
    }

    return 0;
}

//****************************************************************************
// TILED METHOD TIFF
//****************************************************************************

int read_tiled_tiff(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl) {
    if (!tif || !img) return 1;
    if (!TIFFIsTiled(tif)) return 1;

    bim::uint lineSize = getLineSizeInBytes(img);
    bim::uint bpp = ceil((double)img->i.depth / 8.0);
    bim::uint16 planarConfig;
    bim::uint32 columns, rows;
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &planarConfig);
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &columns);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &rows);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);

    std::vector<bim::uchar> buffer(TIFFTileSize64(tif));
    bim::uchar *buf = &buffer[0];

    for (bim::uint64 y = 0; y < img->i.height; y += rows) {
        xprogress(fmtHndl, y, img->i.height, "Reading tiled TIFF");
        if (xtestAbort(fmtHndl) == 1) break;

        uint64 tile_height = (img->i.height - y >= rows) ? rows : img->i.height - y;
        for (bim::uint64 x = 0; x < img->i.width; x += columns) {
            bim::uint tile_width = (img->i.width - x < columns) ? (bim::uint)img->i.width - x : (bim::uint)columns;

            if ((planarConfig == PLANARCONFIG_SEPARATE) || (img->i.samples == 1)) {
                for (bim::uint64 sample = 0; sample < img->i.samples; sample++) {
                    if (TIFFReadTile(tif, buf, x, y, 0, sample) < 0) break;

                    // now put tile into the image
                    for (bim::uint64 yi = 0; yi < tile_height; yi++) {
                        bim::uchar *p = (bim::uchar *)img->bits[sample] + (lineSize * (y + yi));
                        _TIFFmemcpy(p + (x * bpp), buf + (yi * columns * bpp), tile_width * bpp);
                    }
                }    // for sample
            } else { // if image contains several samples in one same plane ex: RGBRGBRGB...
                if (TIFFReadTile(tif, buf, x, y, 0, 0) < 0) break;
                // dima: process YCrCb, etc data

                for (bim::uint64 sample = 0; sample < img->i.samples; sample++) {
                    // now put tile into the image
                    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (tile_height>BIM_OMP_FOR2)
                    for (bim::uint64 yi = 0; yi < tile_height; yi++) {
                        bim::uchar *p = (bim::uchar *)img->bits[sample] + (lineSize * (y + yi));
                        write_line_segment(p + (x * bpp), buf + (yi * columns * img->i.samples * bpp), img, sample, tile_width);
                    }
                } // for sample
            }     // if not separate planes

        } // for x
    }     // for y

    return 0;
}



//****************************************************************************
//*** TIFF READER
//****************************************************************************

// if the file is LSM then the strip size given in the file is incorrect, fix that
// by simply checking against the file size and adjusting if needed
// dima: in 4.1.0 may have to use TIFFGetStrileOffset and TIFFGetStrileByteCount, currently not using TIFF_LAZYSTRILELOAD
#if (TIFFLIB_VERSION >= 20191103)
void lsmFixStripByteCounts(TIFF *tif, bim::uint32 row, tsample_t sample) {
    TIFFDirectory *td = &tif->tif_dir;
    tiff_strp_t strip = sample * td->td_stripsperimage + row / td->td_rowsperstrip;
    tiff_bcnt_t bytecount = td->td_stripbytecount_p[strip];
    if (tif->tif_size <= 0) tif->tif_size = TIFFGetFileSize(tif);

    if (static_cast<tmsize_t>(td->td_stripoffset_p[strip] + bytecount) > tif->tif_size) {
        bytecount = tif->tif_size - td->td_stripoffset_p[strip];
        td->td_stripbytecount_p[strip] = bytecount;
    }
}
#else
void lsmFixStripByteCounts(TIFF *tif, bim::uint32 row, tsample_t sample) {
    TIFFDirectory *td = &tif->tif_dir;
    tiff_strp_t strip = sample * td->td_stripsperimage + row / td->td_rowsperstrip;
    tiff_bcnt_t bytecount = td->td_stripbytecount[strip];
    if (tif->tif_size <= 0) tif->tif_size = TIFFGetFileSize(tif);

    if (td->td_stripoffset[strip] + bytecount > static_cast<tiff_bcnt_t>(tif->tif_size)) {
        bytecount = tif->tif_size - td->td_stripoffset[strip];
        td->td_stripbytecount[strip] = bytecount;
    }
}
#endif

void getCurrentPageInfo(bim::TiffParams *tiffParams);

int read_tiff_image(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams) {
    if (!areValidParams(fmtHndl, tifParams)) return 1;

    TIFF *tif = tifParams->tiff;
    bim::ImageBitmap *img = fmtHndl->image;

    bim::uint32 height = 0;
    bim::uint32 width = 0;
    bim::uint16 bitspersample = 1;
    bim::uint16 samplesperpixel = 1;
    bim::uint32 rowsperstrip;
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 compression = COMPRESSION_NONE;
    bim::uint16 PlanarConfig;
    bim::uint64 currentDir = 0;
    bool skip_photometric_processing = false;

    currentDir = TIFFCurrentDirectory32(tif);
    bim::uint64 needed_page_num = fmtHndl->pageNumber;
    if (tifParams->subType == bim::tstCzLsm)
        needed_page_num = fmtHndl->pageNumber * 2;

    // now must read correct page and set image parameters
    if (currentDir != needed_page_num)
        if (tifParams->subType != bim::tstStk) {
            TIFFSetDirectory32(tif, needed_page_num);

            currentDir = TIFFCurrentDirectory32(tif);
            if (currentDir != needed_page_num) return 1;

            getCurrentPageInfo(tifParams);
        }

    if (tifParams->subType != bim::tstOmeTiff && tifParams->subType != bim::tstOmeBigTiff)
        img->i = tifParams->info;

    TIFFGetField(tif, TIFFTAG_COMPRESSION, &compression);
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &PlanarConfig); // single image plane

    // this is here due to some OME-TIFF do not conform with the standard and come with all channels in the same IFD
    if (tifParams->subType == bim::tstOmeTiff || tifParams->subType == bim::tstOmeBigTiff) {
        int r = omeTiffReadPlane(fmtHndl, tifParams, fmtHndl->pageNumber);
        if (r != 2) return r;
        img->i = tifParams->info;
    }

    // if image is PSIA then read and init it here
    if (tifParams->subType == bim::tstPsia)
        return psiaReadPlane(fmtHndl, tifParams, fmtHndl->pageNumber, img);

    // if image is Fluoview and contains 1..4 channels
    if ((tifParams->subType == bim::tstFluoview || tifParams->subType == bim::tstAndor) && (tifParams->fluoviewInfo.ch > 1))
        return fluoviewReadPlane(fmtHndl, tifParams, fmtHndl->pageNumber);

    // if the file is LSM then the strip size given in the file is incorrect, fix that
    if (tifParams->subType == bim::tstCzLsm)
        for (bim::uint64 sample = 0; sample < samplesperpixel; ++sample)
            for (bim::uint64 y = 0; y < height; ++y)
                lsmFixStripByteCounts(tif, y, sample);

    if (allocImg(fmtHndl, &img->i, img) != 0) return 1;

    // if image is STK
    if (tifParams->subType == bim::tstStk)
        return stkReadPlane(tifParams, fmtHndl->pageNumber, img, fmtHndl);


    if (!TIFFIsTiled(tif))
        read_scanline_tiff(tif, img, fmtHndl);
    else
        read_tiled_tiff(tif, img, fmtHndl);

    if (tifParams->channels_subsampled == true) { // Subsampled colorspaced image
        skip_photometric_processing = true;
    }

    if (!skip_photometric_processing)
        processPhotometric(img, tifParams, photometric);

    return 0;
}

//****************************************************************************
// TIFF WRITER
//****************************************************************************

int write_striped_tiff(TIFF *out, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl) {
    const bim::uint64 width = img->i.width;
    const bim::uint64 height = img->i.height;
    bim::uint16 bitspersample = img->i.depth;
    bim::uint16 samplesperpixel = img->i.samples;
    bim::uint16 planarConfig = PLANARCONFIG_SEPARATE;
    if (samplesperpixel == 3 && bitspersample == 8) {
        planarConfig = PLANARCONFIG_CONTIG;
    }

    // if separate planes or only one sample
    if ((planarConfig == PLANARCONFIG_SEPARATE) || (samplesperpixel == 1)) {
        bim::uint sample;
        bim::uint line_size = getLineSizeInBytes(img);

        for (sample = 0; sample < img->i.samples; sample++) {
            bim::uchar *bits = (bim::uchar *)img->bits[sample];
            for (bim::uint32 y = 0; y < height; y++) {
                xprogress(fmtHndl, y * (sample + 1), height * img->i.samples, "Writing TIFF");
                if (xtestAbort(fmtHndl) == 1) break;

                TIFFWriteScanline(out, bits, y, sample);
                bits += line_size;
            } // for y
        }     // for samples

    } else { // interleaved 8bit RGB image
        bim::uint Bpp = (unsigned int)ceil(((double)bitspersample) / 8.0);
        std::vector<bim::uchar> buf(width * 3 * Bpp);
        bim::uchar *buffer = &buf[0];

        for (bim::uint64 y = 0; y < height; y++) {
            xprogress(fmtHndl, y, height, "Writing TIFF");
            if (xtestAbort(fmtHndl) == 1) break;

            bim::uchar *bufIn0 = ((bim::uchar *)img->bits[0]) + y * width * Bpp;
            bim::uchar *bufIn1 = ((bim::uchar *)img->bits[1]) + y * width * Bpp;
            bim::uchar *bufIn2 = ((bim::uchar *)img->bits[2]) + y * width * Bpp;
            bim::uchar *p = (bim::uchar *)buffer;
            for (bim::uint64 x = 0; x < width; x++) {
                p[0] = *(bufIn0 + x);
                p[1] = *(bufIn1 + x);
                p[2] = *(bufIn2 + x);
                p += 3;
            }
            TIFFWriteScanline(out, buffer, y, 0);
        }
    }
    return 0;
}

int write_tiled_tiff(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl) {
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;

    bim::uint64 width = (bim::uint64)img->i.width;
    bim::uint64 height = (bim::uint64)img->i.height;
    bim::uint16 bitspersample = img->i.depth;
    bim::uint16 samplesperpixel = img->i.samples;
    bim::uint16 planarConfig = PLANARCONFIG_SEPARATE;
    if (samplesperpixel == 3 && bitspersample == 8) {
        planarConfig = PLANARCONFIG_CONTIG;
    }
    bim::uint32 columns = par->info.tileWidth;
    bim::uint32 rows = par->info.tileHeight;
    bim::uint bpp = ceil((double)img->i.depth / 8.0);

    TIFFSetField(tif, TIFFTAG_TILEWIDTH, columns);
    TIFFSetField(tif, TIFFTAG_TILELENGTH, rows);

    std::vector<bim::uint8> buffer(TIFFTileSize64(tif));
    bim::uint8 *buf = &buffer[0];

    for (bim::uint64 y = 0; y < img->i.height; y += rows) {
        xprogress(fmtHndl, y, img->i.height, "Writing tiled TIFF");
        if (xtestAbort(fmtHndl) == 1) break;

        for (bim::uint64 x = 0; x < img->i.width; x += columns) {
            bim::uint tile_width = (width - x >= columns) ? columns : (bim::uint)width - x;
            bim::uint tile_height = (height - y >= rows) ? rows : (bim::uint)height - y;

            // libtiff tiles will always have columns hight with empty pixels
            // we need to copy only the usable portion
            if ((planarConfig == PLANARCONFIG_SEPARATE) || (img->i.samples == 1)) { // if planar
                for (bim::uint64 sample = 0; sample < img->i.samples; ++sample) {
                    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (tile_height>BIM_OMP_FOR2)
                    for (bim::int64 i = 0; i < tile_height; ++i) {
                        bim::uint8 *BIM_RESTRICT to = buf + i * bpp * columns; // buf + sample*bpp + i*bpp*columns;
                        bim::uint8 *BIM_RESTRICT from = ((bim::uint8 *)img->bits[sample]) + (y + i) * bpp * width + x * bpp;
                        memcpy(to, from, tile_width * bpp);
                    }
                    if (TIFFWriteTile(tif, buf, x, y, 0, sample) < 0) break;
                }    // for sample
            } else { // if image contains interleaved samples: RGBRGBRGB...
                bim::uint64 step = bpp * img->i.samples;
                for (bim::uint64 sample = 0; sample < img->i.samples; ++sample) {
                    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (tile_height>BIM_OMP_FOR2)
                    for (bim::uint64 i = 0; i < tile_height; ++i) {
                        bim::uint8 *BIM_RESTRICT to = buf + sample * bpp + i * step * columns;
                        bim::uint8 *BIM_RESTRICT from = ((bim::uint8 *)img->bits[sample]) + (y + i) * bpp * width + x * bpp;
                        for (bim::uint64 x = 0; x < tile_width; ++x) {
                            memcpy(to, from, bpp);
                            from += bpp;
                            to += step;
                        }
                    }
                } // for sample
                if (TIFFWriteTile(tif, buf, x, y, 0, 0) < 0) break;
            } // if not separate planes
        }     // for x
    }         // for y

    return 0;
}

int tiff_update_subifd_next_pointer(TIFF *tif, bim::uint64 dir_offset, bim::uint64 to_offset) {
    static const char module[] = "tiff_update_subifd_next_pointer";

    if (!(tif->tif_flags & TIFF_BIGTIFF)) {
        bim::uint32 m = to_offset;
        if (tif->tif_flags & TIFF_SWAB)
            TIFFSwabLong(&m);

        bim::uint16 dircount = 0;
        if (!SeekOK(tif, dir_offset) ||
            !ReadOK(tif, &dircount, 2)) {
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Error fetching directory count");
            return false;
        }
        if (tif->tif_flags & TIFF_SWAB)
            TIFFSwabShort(&dircount);

        bim::uint32 nextnextdir;
        TIFFSeekFile(tif, dir_offset + 2 + dircount * 12, SEEK_SET);
        if (!ReadOK(tif, &nextnextdir, 4)) {
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Error fetching directory link");
            return false;
        }
        if (tif->tif_flags & TIFF_SWAB)
            TIFFSwabLong(&nextnextdir);

        if (nextnextdir == 0) {
            TIFFSeekFile(tif, dir_offset + 2 + dircount * 12, SEEK_SET);
            if (!WriteOK(tif, &m, 4)) {
                TIFFErrorExt(tif->tif_clientdata, module,
                             "Error writing directory link");
                return false;
            }
        }
    } else {
        bim::uint64 m = to_offset;
        if (tif->tif_flags & TIFF_SWAB)
            TIFFSwabLong8(&m);

        bim::uint64 dircount;
        if (!SeekOK(tif, dir_offset) ||
            !ReadOK(tif, &dircount, 8)) {
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Error fetching directory count");
            return false;
        }
        if (tif->tif_flags & TIFF_SWAB)
            TIFFSwabLong8(&dircount);
        if (dircount > 0xFFFF) {
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Sanity check on tag count failed, likely corrupt TIFF");
            return false;
        }

        bim::uint64 nextnextdir;
        TIFFSeekFile(tif, dir_offset + 8 + dircount * 20, SEEK_SET);
        if (!ReadOK(tif, &nextnextdir, 8)) {
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Error fetching directory link");
            return false;
        }
        if (tif->tif_flags & TIFF_SWAB)
            TIFFSwabLong8(&nextnextdir);
        if (nextnextdir == 0) {
            TIFFSeekFile(tif, dir_offset + 8 + dircount * 20, SEEK_SET);
            if (!WriteOK(tif, &m, 8)) {
                TIFFErrorExt(tif->tif_clientdata, module,
                             "Error writing directory link");
                return false;
            }
        }
    }

    return true;
}

int write_tiff_image(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::ImageBitmap *img = NULL, bool subscale = false) {
    if (!areValidParams(fmtHndl, par)) return 1;

    if (par->subType == bim::tstOmeTiff || par->subType == bim::tstOmeBigTiff)
        return omeTiffWritePlane(fmtHndl, par);

    TIFF *out = par->tiff;
    if (!img) img = fmtHndl->image;

    const bim::uint64 width = img->i.width;
    const bim::uint64 height = img->i.height;
    bim::uint32 rowsperstrip = (bim::uint32)-1;
    bim::uint16 bitspersample = img->i.depth;
    bim::uint16 samplesperpixel = img->i.samples;
    bim::uint16 photometric = PHOTOMETRIC_MINISBLACK;
    bim::uint16 compression;
    bim::uint16 planarConfig;

    if (img->i.imageMode == bim::IM_RGB) {
        photometric = PHOTOMETRIC_RGB;
    } else if (img->i.imageMode == bim::IM_LAB) {
        photometric = PHOTOMETRIC_ICCLAB;
    } else if (img->i.imageMode == bim::IM_MULTI && img->i.samples > 1) {
        photometric = PHOTOMETRIC_MINISBLACK;
        std::vector<bim::uint16> extra_samples(samplesperpixel, EXTRASAMPLE_UNSPECIFIED);
        //extra_samples[0] = EXTRASAMPLE_UNSPECIFIED;
        TIFFSetField(out, TIFFTAG_EXTRASAMPLES, samplesperpixel, &extra_samples[0]);
    } else if ((img->i.imageMode == bim::IM_INDEXED) && (img->i.lut.count > 0) && (samplesperpixel == 1) && (bitspersample <= 8)) {
        photometric = PHOTOMETRIC_PALETTE;
    }

    // handle standard width/height/bpp stuff
    TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, samplesperpixel);
    TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bitspersample);
    TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photometric);
    TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);


    // set pixel format
    bim::uint16 sampleformat = SAMPLEFORMAT_UINT;
    if (img->i.pixelType == bim::FMT_SIGNED) sampleformat = SAMPLEFORMAT_INT;
    if (img->i.pixelType == bim::FMT_FLOAT) sampleformat = SAMPLEFORMAT_IEEEFP;
    TIFFSetField(out, TIFFTAG_SAMPLEFORMAT, sampleformat);


    // set planar config
    planarConfig = PLANARCONFIG_SEPARATE;
    if (samplesperpixel == 3 && bitspersample == 8)
        planarConfig = PLANARCONFIG_CONTIG;
    TIFFSetField(out, TIFFTAG_PLANARCONFIG, planarConfig);

    TIFFSetField(out, TIFFTAG_SOFTWARE, "libbioimage");

    //if( TIFFGetField( out, TIFFTAG_DOCUMENTNAME, &pszText ) )
    //if( TIFFGetField( out, TIFFTAG_IMAGEDESCRIPTION, &pszText ) )
    //if( TIFFGetField( out, TIFFTAG_DATETIME, &pszText ) )

    //------------------------------------------------------------------------------
    // resolution pyramid
    //------------------------------------------------------------------------------

    const bim::uint64 sz = std::max(width, height);
    if (!subscale && par->pyramid.format != bim::PyramidInfo::pyrFmtNone && sz < bim::PyramidInfo::min_level_size) {
        par->pyramid.format = bim::PyramidInfo::pyrFmtNone;
    }

    if (par->info.tileWidth > 0 && par->pyramid.format != bim::PyramidInfo::pyrFmtNone) {
        TIFFSetField(out, TIFFTAG_SUBFILETYPE, subscale ? FILETYPE_REDUCEDIMAGE : 0);

        if (!subscale) {
            par->pyramid.directory_offsets.resize(0);
            bim::int64 num_levels = ceil(bim::log2<double>(sz)) - ceil(bim::log2<double>(bim::PyramidInfo::min_level_size)) + 1;
            if (par->pyramid.format == bim::PyramidInfo::pyrFmtSubDirs) {
                // if pyramid levels are to be written into SUBIFDs, write the tag and indicate to libtiff how many subifds are coming
                bim::uint16 num_sub_ifds = num_levels - 1; // number of pyramidal levels - 1
                std::vector<bim::uint64> offsets_sub_ifds(num_levels - 1, 0UL);
                TIFFSetField(out, TIFFTAG_SUBIFD, num_sub_ifds, &offsets_sub_ifds[0]);
            }
        }
    }

    //------------------------------------------------------------------------------
    // compression
    //------------------------------------------------------------------------------

    compression = fmtHndl->compression;
    if (compression == 0) compression = COMPRESSION_NONE;

    if (compression == COMPRESSION_CCITTFAX4 && bitspersample != 1) {
        compression = COMPRESSION_NONE;
    }
    if (compression == COMPRESSION_JPEG && (bitspersample != 8 && bitspersample != 16)) {
        compression = COMPRESSION_NONE;
    }
    TIFFSetField(out, TIFFTAG_COMPRESSION, compression);

    // set compression parameters
    bim::uint32 strip_size = bim::max<bim::uint32>(TIFFDefaultStripSize(out, -1), 1);
    if (compression == COMPRESSION_JPEG) {
        // rowsperstrip must be multiple of 8 for JPEG
        TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, strip_size + (8 - (strip_size % 8)));
        TIFFSetField(out, TIFFTAG_JPEGQUALITY, fmtHndl->quality);
    } else if (compression == COMPRESSION_ADOBE_DEFLATE) {
        //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, height );
        if (planarConfig == PLANARCONFIG_SEPARATE || samplesperpixel == 1)
            TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_NONE);
        else
            TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL);
        TIFFSetField(out, TIFFTAG_ZIPQUALITY, 9);
    } else if (compression == COMPRESSION_CCITTFAX4) {
        //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, height );
    } else if (compression == COMPRESSION_LZW) {
        //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, strip_size );
        if (planarConfig == PLANARCONFIG_SEPARATE || samplesperpixel == 1)
            TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_NONE);
        else
            TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL);
    } else {
        //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, strip_size );
    }

    //------------------------------------------------------------------------------
    // Save resolution
    //------------------------------------------------------------------------------

    {
        double rx = img->i.xRes, ry = img->i.yRes;
        bim::uint16 units = img->i.resUnits;

        if (((img->i.xRes == 0) && (img->i.yRes == 0)) || (img->i.resUnits == 1)) {
            // Standard resolution some claim to be 72ppi... why not?
            units = RESUNIT_INCH;
            rx = 72.0;
            ry = 72.0;
        } else if (img->i.resUnits != 2) {
            if (img->i.resUnits == 0) {
                rx = pow(rx, -2);
                ry = pow(ry, -2);
            }
            if (img->i.resUnits == 4) {
                rx = pow(rx, -1);
                ry = pow(ry, -1);
            }
            if (img->i.resUnits == 5) {
                rx = pow(rx, -4);
                ry = pow(ry, -4);
            }
            if (img->i.resUnits == 6) {
                rx = pow(rx, -7);
                ry = pow(ry, -7);
            }
            if (img->i.resUnits == 7) {
                rx = pow(rx, 11);
                ry = pow(ry, 11);
            }
            if (img->i.resUnits == 8) {
                rx = pow(rx, 8);
                ry = pow(ry, 8);
            }
            if (img->i.resUnits == 9) {
                rx = pow(rx, 5);
                ry = pow(ry, 5);
            }
            if (img->i.resUnits == 10) {
                rx = pow(rx, 0);
                ry = pow(ry, 0);
            }
        }

        TIFFSetField(out, TIFFTAG_RESOLUTIONUNIT, units);
        TIFFSetField(out, TIFFTAG_XRESOLUTION, rx);
        TIFFSetField(out, TIFFTAG_YRESOLUTION, ry);
    }

    //------------------------------------------------------------------------------
    // palettes (image colormaps are automatically scaled to 16-bits)
    //------------------------------------------------------------------------------
    bim::uint16 palr[256], palg[256], palb[256];
    if ((photometric == PHOTOMETRIC_PALETTE) && (img->i.lut.count > 0)) {
        bim::uint16 nColors = img->i.lut.count;
        for (bim::uint16 i = 0; i < nColors; i++) {
            palr[i] = (bim::uint16)bim::xR(img->i.lut.rgba[i]) * 256;
            palg[i] = (bim::uint16)bim::xG(img->i.lut.rgba[i]) * 256;
            palb[i] = (bim::uint16)bim::xB(img->i.lut.rgba[i]) * 256;
        }
        TIFFSetField(out, TIFFTAG_COLORMAP, palr, palg, palb);
    }

    //------------------------------------------------------------------------------
    // writing meta data
    //------------------------------------------------------------------------------
    if (fmtHndl->pageNumber == 0 && !subscale) {
        write_tiff_metadata(fmtHndl, par);
    }

    //------------------------------------------------------------------------------
    // writing image
    //------------------------------------------------------------------------------

    if (par->info.tileWidth < 1 || par->pyramid.format == bim::PyramidInfo::pyrFmtNone) {
        write_striped_tiff(out, img, fmtHndl);
    } else {
        write_tiled_tiff(out, img, fmtHndl);
    }

    // correct libtiff writing of subifds by linking sibling ifds through nextifd offset
    if (subscale && par->pyramid.format == bim::PyramidInfo::pyrFmtSubDirs) {
        bim::uint64 dir_offset = (TIFFSeekFile(out, 0, SEEK_END) + 1) & ~1;
        par->pyramid.directory_offsets.push_back(dir_offset);
    }

    //------------------------------------------------------------------------------
    // finish directory
    //------------------------------------------------------------------------------
    TIFFWriteDirectory(out);


    //------------------------------------------------------------------------------
    // writing pyramid levels
    //------------------------------------------------------------------------------

    if (!subscale && par->info.tileWidth > 0 && par->pyramid.format != bim::PyramidInfo::pyrFmtNone) {
        bim::Image image(img);
        int i = 0;
        while (std::max(image.width(), image.height()) > bim::PyramidInfo::min_level_size) {
            image = image.downSampleBy2x();
            if (write_tiff_image(fmtHndl, par, image.imageBitmap(), true) != 0) break;
            ++i;
        }

        // correct libtiff writing of subifds by linking sibling ifds through nextifd offset
        if (par->pyramid.format == bim::PyramidInfo::pyrFmtSubDirs)
            for (ptrdiff_t i = 0; i < ((ptrdiff_t)par->pyramid.directory_offsets.size()) - 1; ++i) {
                if (!tiff_update_subifd_next_pointer(out, par->pyramid.directory_offsets[i], par->pyramid.directory_offsets[i + 1])) break;
            }
    }

    //------------------------------------------------------------------------------
    // finish file
    //------------------------------------------------------------------------------
    if (!subscale) {
        TIFFFlushData(out);
        TIFFFlush(out);
    }

    return 0;
}

//--------------------------------------------------------------------------------------------
// Levels and Tiles functions
//--------------------------------------------------------------------------------------------

int read_tiff_image_level(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams, bim::uint page, bim::uint level) {
    if (!areValidParams(fmtHndl, tifParams)) return 1;
    TIFF *tif = tifParams->tiff;
    bim::PyramidInfo *pyramid = &tifParams->pyramid;
    bim::ImageBitmap *img = fmtHndl->image;
    bim::ImageInfo *info = &tifParams->info;

    // set correct level
    if (pyramid->number_levels > 0 && pyramid->number_levels <= level) return 1;
    bim::uint64 current_dir = TIFFCurrentDirectory32(tif);
    bim::uint64 subdiroffset = pyramid->directory_offsets[level];
    if (TIFFSetSubDirectory(tif, subdiroffset) == 0) return 1;

    // set tile parameters
    bim::uint32 height = 0;
    bim::uint32 width = 0;
    bim::uint16 planarConfig;
    bim::uint32 columns, rows;
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 bitspersample = 1;
    bim::uint16 samplesperpixel = 1;
    bool skip_photometric_processing = false;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &planarConfig);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);

    img->i.samples = samplesperpixel;
    img->i.depth = bitspersample;
    img->i.width = width;
    img->i.height = height;
    img->i.pixelType = info->pixelType;
    img->i.imageMode = info->imageMode;
    bim::uint bpp = ceil((double)img->i.depth / 8.0);
    if (allocImg(fmtHndl, &img->i, img) != 0) return 1;

    if (!TIFFIsTiled(tif))
        read_scanline_tiff(tif, img, fmtHndl);
    else
        read_tiled_tiff(tif, img, fmtHndl);

    if (tifParams->channels_subsampled == true) { // Subsampled colorspaced image
        skip_photometric_processing = true;
    }

    if (!skip_photometric_processing)
        processPhotometric(img, tifParams, photometric);

    TIFFSetDirectory32(tif, current_dir);
    return 0;
}

tmsize_t _tiff_read_block(bim::TiffParams *pars, bim::uint8 *buf, bim::uint64 x, bim::uint64 y, bim::uint sample, bim::uint64 height, bim::uint64 sz_line, bim::uint64 sz_buf) {
    TIFF *tif = pars->tiff;

    if (pars->tiled) {
        return TIFFReadTile(tif, buf, x, y, 0, sample);
    } else {
        // read N scanlines simulating a tile
        for (bim::uint64 i = 0; i < height; ++i) {
            if (!TIFFReadScanline(tif, buf, y, sample)) return 0;
            y += 1;
            buf += sz_line;
        }
    }
    return sz_buf;
}

int read_tiff_image_tile(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (!areValidParams(fmtHndl, tifParams)) return 1;
    TIFF *tif = tifParams->tiff;
    bim::PyramidInfo *pyramid = &tifParams->pyramid;
    bim::ImageBitmap *img = fmtHndl->image;
    bim::ImageInfo *info = &tifParams->info;

    //if (!TIFFIsTiled(tif)) return read_tiff_image(fmtHndl, tifParams);
    if (!TIFFIsTiled(tif)) return 1;

    // set correct level
    if (pyramid->number_levels > 0 && pyramid->number_levels <= level) return 1;
    bim::uint64 current_dir = TIFFCurrentDirectory32(tif);
    bim::uint64 subdiroffset = pyramid->directory_offsets[level];
    if (current_dir != subdiroffset && TIFFSetSubDirectory(tif, subdiroffset) == 0) return 1;

    // set tile parameters
    bool skip_photometric_processing = false;
    bim::uint64 height = 0;
    bim::uint64 width = 0;
    bim::uint16 planarConfig;
    bim::uint32 columns, rows;
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 bitspersample = 1;
    bim::uint16 samplesperpixel = 1;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &columns);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &rows);
    TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &planarConfig);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);

    // tile sizes may be smaller at the border
    bim::uint64 x = xid * columns;
    bim::uint64 y = yid * rows;
    bim::uint64 tile_width = (width - x >= columns) ? columns : width - x;
    bim::uint64 tile_height = (height - y >= rows) ? rows : height - y;

    img->i.samples = samplesperpixel;
    img->i.depth = bitspersample;
    img->i.width = tile_width;
    img->i.height = tile_height;
    img->i.pixelType = info->pixelType;
    img->i.imageMode = info->imageMode;
    bim::uint bpp = ceil((double)img->i.depth / 8.0);
    if (allocImg(fmtHndl, &img->i, img) != 0) return 1;

    const tmsize_t expected_buffer_plane = tile_width * tile_height * bpp;
    tmsize_t expected_buffer_size = expected_buffer_plane * samplesperpixel;

    std::vector<bim::uint8> buffer(TIFFTileSize64(tif));
    bim::uint8 *buf = &buffer[0];

    // libtiff tiles will always have columns hight with empty pixels
    // we need to copy only the usable portion
    if ((planarConfig == PLANARCONFIG_SEPARATE) || (img->i.samples == 1)) { // if planar
        for (bim::uint64 sample = 0; sample < img->i.samples; ++sample) {
            tmsize_t tsize = TIFFReadTile(tif, buf, x, y, 0, sample);
            if (tsize < expected_buffer_plane) {
                // no tile data retrieved, do nothing
                skip_photometric_processing = true;
                bim::Image tile_image(img);
                tile_image.fill(0);
            } else {
                //#pragma omp parallel for default(shared)  BIM_OMP_SCHEDULE if (tile_height>BIM_OMP_FOR2)
                for (bim::uint64 y = 0; y < tile_height; ++y) {
                    bim::uint8 *BIM_RESTRICT from = buf + y * bpp * columns;
                    bim::uint8 *BIM_RESTRICT to = ((bim::uint8 *)img->bits[sample]) + y * bpp * tile_width;
                    memcpy(to, from, bpp * tile_width);
                }
            }
        }                                                 // for sample
    } else if (tifParams->channels_subsampled == false) { // if image contains interleaved samples: RGBRGBRGB...
        tmsize_t tsize = TIFFReadTile(tif, buf, x, y, 0, 0);
        if (tsize < expected_buffer_size) {
            // no tile data retrieved, do nothing
            skip_photometric_processing = true;
            bim::Image tile_image(img);
            tile_image.fill(0);
        } else {
            int step = bpp * img->i.samples;
            for (bim::uint64 sample = 0; sample < img->i.samples; ++sample) {
                //#pragma omp parallel for default(shared)  BIM_OMP_SCHEDULE if (tile_height>BIM_OMP_FOR2)
                for (bim::uint64 y = 0; y < tile_height; ++y) {
                    bim::uint8 *BIM_RESTRICT from = buf + sample * bpp + y * step * columns;
                    bim::uint8 *BIM_RESTRICT to = ((bim::uint8 *)img->bits[sample]) + y * bpp * tile_width;
                    for (bim::uint64 x = 0; x < tile_width; ++x) {
                        memcpy(to, from, bpp);
                        to += bpp;
                        from += step;
                    }
                }
            } // for sample
        }
    } else if (tifParams->channels_subsampled == true) { // Subsampled colorspaced image
        skip_photometric_processing = true;
        bim::Image tile_image(img);
        tile_image.fill(0);

        TIFFRGBAImage rgba_img;
        char emsg[1024] = "unknown error";
        expected_buffer_size = tile_width * tile_height * 4;
        std::vector<bim::uint8> rgba_buffer(expected_buffer_size);
        bim::uint8 *rgba_buf = &rgba_buffer[0];

        if (TIFFRGBAImageOK(tif, emsg)) {
            if (TIFFRGBAImageBegin(&rgba_img, tif, 1, emsg)) {
                rgba_img.req_orientation = ORIENTATION_TOPLEFT;
                rgba_img.col_offset = x;
                rgba_img.row_offset = y;

                if (TIFFRGBAImageGet(&rgba_img, (bim::uint32 *)rgba_buf, tile_width, tile_height)) {
                    // convert the packed ABGR form returned by TIFFReadRGBAImage -> RGB
                    int step = 4;
                    int rgba_sample = 0; //int rgba_sample = 2;
                    //for (bim::uint sample = 0; sample < 3; ++sample) {
                        //#pragma omp parallel for default(shared)  BIM_OMP_SCHEDULE if (tile_height>BIM_OMP_FOR2)
                        for (bim::uint64 y = 0; y < tile_height; ++y) {
                            bim::uint8 *BIM_RESTRICT from = rgba_buf + rgba_sample + y * step * columns;
                            //bim::uint8 *BIM_RESTRICT to = ((bim::uint8 *)img->bits[sample]) + y * bpp * tile_width;
                            bim::uint8 *BIM_RESTRICT toR = ((bim::uint8 *)img->bits[0]) + y * bpp * tile_width;
                            bim::uint8 *BIM_RESTRICT toG = ((bim::uint8 *)img->bits[1]) + y * bpp * tile_width;
                            bim::uint8 *BIM_RESTRICT toB = ((bim::uint8 *)img->bits[2]) + y * bpp * tile_width;
                            for (bim::uint64 x = 0; x < tile_width; ++x) {
                                //memcpy(to, from, bpp);
                                //to += bpp;

                                *toR = TIFFGetR(*(bim::uint32*) from);
                                *toG = TIFFGetG(*(bim::uint32*) from);
                                *toB = TIFFGetB(*(bim::uint32*) from);

                                toR += bpp;
                                toG += bpp;
                                toB += bpp;

                                from += step;
                            }
                        }
                        //rgba_sample -= 1;
                    //} // for sample
                }

                TIFFRGBAImageEnd(&rgba_img);
            }
        }
    }

    if (!skip_photometric_processing)
        processPhotometric(img, tifParams, photometric);
    TIFFSetDirectory32(tif, current_dir);
    return 0;
}
