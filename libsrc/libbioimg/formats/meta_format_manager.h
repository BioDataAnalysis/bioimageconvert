/*******************************************************************************

*******************************************************************************/

#ifndef META_FORMAT_MANAGER_H
#define META_FORMAT_MANAGER_H

#include "bim_format_manager.h"

namespace bim {

// MetaFormatManager folded into FormatManager, deprecated in v3

typedef bim::FormatManager MetaFormatManager;

} // namespace bim

#endif // META_FORMAT_MANAGER_H
