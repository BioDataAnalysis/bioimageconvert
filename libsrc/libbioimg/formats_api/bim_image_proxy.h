/*******************************************************************************

  Image Proxy Class - opens format session and allows reading tiles and levels

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>
  Copyright (c) 2018, ViQi Inc

  History:
    03/23/2014 18:03 - First creation

  ver: 1

*******************************************************************************/

#ifndef BIM_IMAGE_PROXY_H
#define BIM_IMAGE_PROXY_H

#include <bim_image.h>
#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>
#include <meta_format_manager.h>
#include <tag_map.h>

#include <map>
#include <string>
#include <vector>

namespace bim {

//------------------------------------------------------------------------------
// ImageProxy
//------------------------------------------------------------------------------

class ImageProxy {
public:
    ImageProxy();
    ImageProxy(FormatManager *manager) {
        fm = manager;
        external_manager = true;
    }
    ImageProxy(const std::string &fileName, XConf *c = NULL);
    ~ImageProxy();

    void free() { closeFile(); }
    void clear() { closeFile(); }

    bool isReady() const { return fm->sessionActive(); } // true if images can be used

    // I/O
    bool openFile(const std::string &fileName, XConf *c = NULL);
    void closeFile();

    // Operations
    bool read(Image &img, bim::uint page);

    // low level functions reading levels as stored in the file or as powewr-of-two pyramid
    bool readLevel(Image &img, uint page, uint64 level, bool power_two_level = true);
    bool readTile(Image &img, uint page, uint64 xid, uint64 yid, uint64 level, uint64 tile_size, bool power_two_level = true);
    bool readRegion(Image &img, uint page, uint64 x1, uint64 y1, uint64 x2, uint64 y2, uint64 level, bool power_two_level = true);

    // higher level functions reading and interpolating data based on requested scale
    bool readLevel(Image &img, uint page, double scale);
    bool readTile(Image &img, uint page, uint64 xid, uint64 yid, double scale, uint64 tile_size);
    bool readRegion(Image &img, uint page, uint64 x1, uint64 y1, uint64 x2, uint64 y2, double scale);

protected:
    FormatManager *fm;
    XConf *conf = 0;
    bool external_manager;
    std::vector<double> scales;
    uint64 full_width = 0;
    uint64 full_height = 0;
    std::vector<int> tile_sizes_w;
    std::vector<int> tile_sizes_h;

protected:
    void init();

    int getImageLevel(uint64 level, bool power_two_level = true);
    int getImageLevel(double scale);
    int getClosestLevel(double scale);
    void parseScales();

    // callbacks
public:
    ProgressProc progress_proc;
    ErrorProc error_proc;
    TestAbortProc test_abort_proc;

protected:
    void do_progress(bim::uint64 done, bim::uint64 total, char *descr) {
        if (progress_proc) progress_proc(done, total, descr);
    }
    bool progress_abort() {
        if (!test_abort_proc) return false;
        return (test_abort_proc() != 0);
    }
};

} // namespace bim

#endif //BIM_IMAGE_PROXY_H
