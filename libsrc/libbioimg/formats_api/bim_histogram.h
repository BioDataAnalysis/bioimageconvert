/*******************************************************************************

  histogram and lut

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>
  Copyright (c) 2018, ViQi Inc

  History:
    10/20/2006 20:21 - First creation
    2007-06-26 12:18 - added lut class
    2010-01-22 17:06 - changed interface to support floating point data

  ver: 3

*******************************************************************************/

#ifndef BIM_HISTOGRAM_H
#define BIM_HISTOGRAM_H

#include <iostream>
#include <string>
#include <vector>

#include "bim_img_format_interface.h"

//******************************************************************************
// Histogram
//******************************************************************************

namespace bim {

#pragma pack(push, 1)
struct HistogramInternal {
    uint16 data_bpp; // bits per pixel
    uint16 data_fmt; // signed, unsigned, float
    double shift;
    double scale;
    double value_min;
    double value_max;
};
#pragma pack(pop)



class Lut;
class Histogram {
public:
    enum ChannelMode {
        cmSeparate = 0,
        cmCombined = 1
    };

    typedef uint64 StorageType;
    static const unsigned int defaultSize = 256; // 256, 65536

    ~Histogram();
    explicit Histogram(uint64 bpp = 0, DataFormat fmt = FMT_UNSIGNED);
    Histogram(uint64 bpp, const void *data, size_t num_data_points, DataFormat fmt = FMT_UNSIGNED, const uint8 *mask = 0);

    void newData(uint64 bpp, const void *data, size_t num_data_points, DataFormat fmt = FMT_UNSIGNED, const uint8 *mask = 0);
    void clear();

    void init(uint64 bpp, DataFormat fmt = FMT_UNSIGNED);
    void updateStats(const void *data, size_t num_data_points, const uint8 *mask = 0);
    // careful with this function operating on data with 32 bits and above, stats should be properly updated for all data chunks if vary
    // use the updateStats method on each data chunk prior to calling addData on >=32bit data
    void addData(const void *data, size_t num_data_points, const uint8 *mask = 0);

    size_t getDefaultSize() const { return default_size; }
    void setDefaultSize(size_t v) { default_size = v; }

    bool isValid() const { return d.data_bpp > 0; }
    uint64 dataBpp() const { return d.data_bpp; }
    DataFormat dataFormat() const { return (DataFormat)d.data_fmt; }

    uint64 bin_of_max_value() const;
    uint64 bin_of_min_value() const;
    double max_value() const;
    double min_value() const;

    uint64 bin_of_first_nonzero() const;
    uint64 bin_of_last_nonzero() const;
    uint64 bin_number_nonzero() const;
    uint64 first_pos() const { return bin_of_first_nonzero(); }
    uint64 last_pos() const { return bin_of_last_nonzero(); }
    uint64 num_unique() const { return bin_number_nonzero(); }

    uint64 bin_number() const { return size(); }
    uint64 size() const { return hist.size(); }
    const std::vector<StorageType> &get_hist() const { return hist; }

    double average() const;
    double median() const;
    double std() const;
    StorageType cumsum(uint64 bin) const;
    double range() const;

    double get_shift() const { return d.shift; }
    double get_scale() const { return d.scale; }

    StorageType get_value(uint64 bin) const;
    void set_value(uint64 bin, StorageType val);
    void append_value(uint64 bin, StorageType val);

    inline StorageType operator[](uint64 x) const { return hist[x]; }

    template<typename T>
    inline uint64 bin_from(const T &data_point) const;

    void set_stats(double minv, double maxv) {
        d.value_min = minv;
        d.value_max = maxv;
        reversed_min_max = false;
        recompute_shift_scale();
    }

public:
    // I/O
    bool to(const std::string &fileName);
    bool to(std::ostream *s);
    bool from(const std::string &fileName);
    bool from(std::istream *s);
    bool toXML(const std::string &fileName);
    bool toXML(std::ostream *s);

protected:
    size_t default_size;
    bool reversed_min_max;
    bim::HistogramInternal d;
    std::vector<StorageType> hist;

    template<typename T>
    void init_stats();

    void initStats();
    void getStats(const void *data, size_t num_data_points, const uint8 *mask = 0);

    inline void recompute_shift_scale() {
        d.shift = d.value_min;
        d.scale = ((double)bin_number() - 1) / (d.value_max - d.value_min);
    }

    template<typename T>
    void get_data_stats(const T *data, size_t num_data_points, const uint8 *mask = 0);

    template<typename T>
    void update_data_stats(const T *data, size_t num_data_points, const uint8 *mask = 0);

    template<typename T>
    void add_from_data(const T *data, size_t num_data_points, const uint8 *mask = 0);

    template<typename T>
    void add_from_data_scale(const T *data, size_t num_data_points, const uint8 *mask = 0);

    friend class Lut;
};

template<typename T>
inline uint64 Histogram::bin_from(const T &data_point) const {
    return bim::trim<uint64, double>((data_point - d.shift) * d.scale, 0, bin_number());
}

//******************************************************************************
// Lut - single channel enhancement
//******************************************************************************

#pragma pack(push, 1)
struct LutParameters {
    double out_min = 0.0; // must be defined to specify output range
    double out_max = 0.0; // must be defined to specify output range

    double in_min = 0.0;    // may be defined by the user to manually set a specific range
    double in_max = 0.0;    // may be defined by the user to manually set a specific range
    double gamma = 1.0;     // may be defined by the user for gamma enhancement
    double tolerance = 1.0; // may be defined by the user for enhancement

    double shift = 0.0; // generator will define these
    double scale = 0.0; // generator will define these
};
#pragma pack(pop)

class Lut {
public:
    enum LutType {
        ltLinearFullRange = 0,
        ltLinearDataRange = 1,
        ltLinearDataTolerance = 2,
        ltEqualize = 3,
        ltTypecast = 4,
        ltFloat01 = 5,
        ltMinMaxGamma = 6,
        ltCustom = 100,
    };

    typedef double StorageType;
    typedef void (*LutGenerator)(const Histogram &in, std::vector<StorageType> &lut, LutParameters *args);

    Lut();
    Lut(const Histogram &in, const Histogram &out, LutParameters *args = NULL);
    Lut(const Histogram &in, const Histogram &out, LutType type, LutParameters *args = NULL);
    Lut(const Histogram &in, const Histogram &out, LutGenerator custom_generator, LutParameters *args = NULL);
    ~Lut();

    void init(const Histogram &in, const Histogram &out, LutParameters *args = NULL);
    void init(const Histogram &in, const Histogram &out, LutType type, LutParameters *args = NULL);
    void init(const Histogram &in, const Histogram &out, LutGenerator custom_generator, LutParameters *args = NULL);
    void clear();

    size_t size() const { return lut.size(); }
    const std::vector<StorageType> &get_lut() const { return lut; }

    uint64 depthInput() const { return h_in.dataBpp(); }
    uint64 depthOutput() const { return h_out.dataBpp(); }
    DataFormat dataFormatInput() const { return h_in.dataFormat(); }
    DataFormat dataFormatOutput() const { return h_out.dataFormat(); }

    template<typename Tl>
    void set_lut(const std::vector<Tl> &);

    StorageType get_value(size_t pos) const;
    void set_value(size_t pos, StorageType val);
    inline StorageType operator[](size_t x) const { return lut[x]; }

    const LutParameters *parameters() const { return &this->d; }

    //
    void apply(void *ibuf, const void *obuf, size_t num_data_points) const;

    // generates values of output histogram, given the current lut and in histogram
    void apply(const Histogram &in, Histogram &out) const;

protected:
    std::vector<StorageType> lut;
    bim::LutParameters d;
    LutGenerator generator;
    LutType type;
    Histogram h_in, h_out;

    void setType(LutType type);

    template<typename Ti, typename To>
    void apply_lut(const Ti *ibuf, To *obuf, size_t num_data_points) const;

    template<typename Ti, typename To>
    void apply_lut_scale_from(const Ti *ibuf, To *obuf, size_t num_data_points) const;

    template<typename Ti, typename To>
    void apply_typecast(const Ti *ibuf, To *obuf, size_t num_data_points) const;

    template<typename Ti>
    inline void do_apply_lut(const Ti *ibuf, const void *obuf, size_t num_data_points) const;

    template<typename Ti>
    inline void do_apply_lut_scale_from(const Ti *ibuf, const void *obuf, size_t num_data_points) const;
};

//******************************************************************************
// Generators - default
//******************************************************************************

template<typename Tl>
static void linear_full_range_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);

template<typename Tl>
static void linear_data_range_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);

template<typename Tl>
static void linear_data_tolerance_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);

template<typename Tl>
static void equalized_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);

template<typename Tl>
static void typecast_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);

template<typename Tl>
static void linear_float01_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);

// LutParameters will define generation parameters
// if in_min and in_max are not equal to each other then they will be used as input range
// keep gamma at 1.0 to keep original data linearity

template<typename Tl>
static void min_max_gamma_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args);



//******************************************************************************
// Lut2D or palette used to map single channel into RGBA
// 2D luts process data values [0 : range]
//******************************************************************************

struct Lut2DPoint {
    double pos;
    double R;
    double G;
    double B;
    double A;
};

template<typename T>
struct Lut2DColor {
    T R;
    T G;
    T B;
    T A;
};

class Lut2D {
public:
    static Lut2D from_name(const std::string &name);
    static Lut2D from_color(const double &r, const double &g, const double &b, const double &a = 1.0); // in this case RGB weights should be in the 0-1 range
    static Lut2D from_color(const std::string &hex_color);                                             // in this case RGB weights should be in the 0-255 range in html hex notation

    // basic colors
    static Lut2D red();
    static Lut2D green();
    static Lut2D blue();
    static Lut2D yellow();
    static Lut2D magenta();
    static Lut2D cyan();
    static Lut2D gray();
    static Lut2D black();

    // similar to MATLAB
    static Lut2D heatmap();
    static Lut2D colormap();
    static Lut2D jet();   // similar to MATLAB 256 level jet
    static Lut2D jet64(); // similar to MATLAB 64 level jet
    static Lut2D parula();
    static Lut2D hsv();
    static Lut2D hot();
    static Lut2D cool();
    static Lut2D spring();
    static Lut2D summer();
    static Lut2D autumn();
    static Lut2D winter();
    static Lut2D bone();
    static Lut2D copper();
    static Lut2D pink();
    static Lut2D fluorescence();

    // similar to Zeiss Zen
    static Lut2D lsm();
    static Lut2D rainbow();
    static Lut2D greentored();
    static Lut2D bluetored();
    static Lut2D gold();

    // similar to Osirix
    static Lut2D blackbody();
    static Lut2D endoscopy();
    static Lut2D flow();
    static Lut2D ired();
    static Lut2D perfusion();
    static Lut2D pet();
    static Lut2D ratio();
    static Lut2D spectrum();
    static Lut2D vr_bones();
    static Lut2D vr_muscles_bones();
    static Lut2D vr_red_vessels();

public:
    Lut2D();
    ~Lut2D();

    bool is_valid() const { return this->lut.size() > 1; }
    void add_point(const Lut2DPoint &p);

    template<typename T>
    void densify(uint range_from, std::vector<bim::Lut2DColor<T>> &out) const;

protected:
    // interpolation color space : RGB, HSV, YUV, etc...
    // interpolation function : cubic, spline, etc...
    std::vector<Lut2DPoint> lut;
};


} // namespace bim

#endif //BIM_HISTOGRAM_H
