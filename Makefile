prefix=/usr
BINDIR=$(prefix)/bin
LIBDIR=$(prefix)/lib
#MAKEFLAGS += -j
LIBBIM=libsrc/libbioimg
LIBTIFF=libsrc/libtiff
LIBRAW=libsrc/libraw
LIBVPX=libsrc/libvpx
LIBWEBP=libsrc/libwebp
LIBJXR=libsrc/jxrlib
LCMS2=libsrc/lcms2
LIBX264=libsrc/libx264
LIBX265=libsrc/libx265
LIBOPENJPEG=libsrc/openjpeg
FFMPEG=libsrc/ffmpeg
LIBJPEGTURBO=libsrc/libjpeg-turbo
LIBGDCM=libsrc/gdcm
LIBGDCMBIN=libsrc/gdcmbin
LIBCZI=libsrc/libCZI
LIBHDF=libsrc/libhdf5
LIBOPENSLIDE=libsrc/openslide
LIBS=libs/linux
LIBS_PKG=libs/build_libs_2-4-0_linux.zip
PKG_CONFIG_PATH=$LIBVPX:$LIBX264:$LIBX265/build/linux
QMAKEOPTS=
VERSION=2.3.0
QMAKE=qmake-qt4

all : binlibs imgcnv

binlibs:
	python download_build_libs.py  # TODO - this needs to go away - jb
	#python download_deps.py
#	python download_deps.py "https://github.com/dimin/libCZI.git"  # submodule added
	#python download_deps.py "https://bitbucket.org/bioimageconvert/libhdf5.git"
	#python download_deps.py "https://gitlab.com/viqi_public/bioimageconvert/openslide.git"
#	python download_deps.py "https://gitlab.com/viqi_public/bioimageconvert/nifti_clib.git" # submodule added

$(LIBS)/libopenslide.a:
	@echo
	@echo
	@echo "Building openslide 3.4.1 in $(LIBOPENSLIDE)"
#	python download_deps.py "https://gitlab.com/viqi_public/bioimageconvert/openslide.git"  # submodule added
	(cd $(LIBOPENSLIDE) && autoreconf -i)
	(cd $(LIBOPENSLIDE) && export LIBTIFF_CFLAGS=-I../../$(LIBTIFF) )
	(cd $(LIBOPENSLIDE) && export LIBTIFF_LIBS=-ltiff )
	(cd $(LIBOPENSLIDE) && ./configure --with-pic --enable-static )
	#(cd $(LIBOPENSLIDE) && $(MAKE) $(MAKEFLAGS))
	(cd $(LIBOPENSLIDE) && $(MAKE))
	(cp $(LIBOPENSLIDE)/src/.libs/libopenslide.a $(LIBS)/)

buildlibs: binlibs $(LIBS)/libopenslide.a
	@echo "Ensured dependencies"

install: imgcnv
	install -d $(DESTDIR)$(BINDIR)
	install imgcnv $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(LIBDIR)
	install libimgcnv.so.2 $(DESTDIR)$(LIBDIR)

qmake:
	(cd $(LIBBIM) && $(QMAKE) $(QMAKEOPTS) bioimage.pro)
	(cd src && $(QMAKE) $(QMAKEOPTS) imgcnv.pro)
	(cd src_dylib && $(QMAKE) $(QMAKEOPTS) libimgcnv.pro)

imgcnv: buildlibs qmake
	-mkdir -p .generated/obj

	@echo
	@echo
	@echo "Building libbioimage in $(LIBBIM)"
	(cd $(LIBBIM) && $(MAKE))

	@echo
	@echo
	@echo "Building imgcnv"
	(cd src && $(MAKE))

	@echo
	@echo
	@echo "Building libimgcnv"
	(cd src_dylib && $(MAKE))

full: buildlibs qmake
	-mkdir -p .generated/obj
	-mkdir -p $(LIBS)

	@echo
	@echo
	@echo "Building libbioimage in $(LIBBIM)"
	(cd $(LIBBIM) && $(MAKE))

	@echo
	@echo
	@echo "Building libCZI in $(LIBCZI)"
	#add_library(libCZIstatic STATIC ${LIBCZISRCFILES} ${LIBCZISRCEIGENFILES})
	# inserted to line 7 in libsrc/libCZI/Src/libCZI/CMakeLists.txt
	(cd $(LIBCZI)/Src && cmake -G "Unix Makefiles" )
	(cd $(LIBCZI)/Src && $(MAKE) $(MAKEFLAGS))
	#(cp $(LIBCZI)/Src/libCZI/liblibCZI.so $(LIBS)/libCZI.so)
	(cp $(LIBCZI)/Src/libCZI/liblibCZIstatic.a $(LIBS)/libCZI.a)

	@echo
	@echo
	@echo "Building libvpx 1.5.0 in $(LIBVPX)"
	#git clone https://chromium.googlesource.com/webm/libvpx
	#git checkout v1.5.0
	(cd $(LIBVPX) && chmod -f u+x configure)
	(cd $(LIBVPX) && chmod -f u+x build/make/version.sh)
	(cd $(LIBVPX) && chmod -f u+x build/make/rtcd.pl)
	(cd $(LIBVPX) && chmod -f u+x build/make/gen_asm_deps.sh)
	(cd $(LIBVPX) && chmod -f u+x build/make/gen_asm_deps.sh)
	(cd $(LIBVPX) && ./configure --enable-vp8 --enable-vp9 --enable-pic --disable-examples --disable-unit-tests --disable-docs )
	(cd $(LIBVPX) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBVPX)/libvpx.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libwebp 0.4.3 in $(LIBWEBP)"
	(cd $(LIBWEBP) && chmod -f u+x configure)
	(cd $(LIBWEBP) && chmod -f u+x autogen.sh)
	(cd $(LIBWEBP) && chmod -f u+x config.guess)
	(cd $(LIBWEBP) && chmod -f u+x config.sub)
	(cd $(LIBWEBP) && ./configure --with-pic --enable-libwebpmux --enable-libwebpdemux --enable-libwebpdecoder)
	(cd $(LIBWEBP) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBWEBP)/src/.libs/libwebp.a $(LIBS)/)
	(cp $(LIBWEBP)/src/mux/.libs/libwebpmux.a $(LIBS)/)
	(cp $(LIBWEBP)/src/demux/.libs/libwebpdemux.a $(LIBS)/)

	@echo
	@echo
	@echo "Building jxrlib 1.1.0 in $(LIBJXR)"
	(cd $(LIBJXR) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBJXR)/libjpegxr.a $(LIBS)/)
	(cp $(LIBJXR)/libjxrglue.a $(LIBS)/)

	@echo
	@echo
	@echo "Building lcms 2.7.0 in $(LCMS2)"
	(cd $(LCMS2) && chmod -f u+x configure)
	(cd $(LCMS2) && chmod -f u+x autogen.sh)
	(cd $(LCMS2) && chmod -f u+x config.guess)
	(cd $(LCMS2) && chmod -f u+x config.sub)
	(cd $(LCMS2) && ./configure --with-pic --enable-shared=no --without-zlib )
	(cd $(LCMS2) && $(MAKE) $(MAKEFLAGS))
	(cp $(LCMS2)/src/.libs/liblcms2.a $(LIBS)/)

	@echo
	@echo
	@echo "Building openjpeg 2.1.0 in $(LIBOPENJPEG)"
	(cd $(LIBOPENJPEG) && cmake . -DCMAKE_C_FLAGS=-fPIC -DBUILD_SHARED_LIBS:bool=off )
	(cd $(LIBOPENJPEG) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBOPENJPEG)/bin/libopenjp2.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libx264 20150223 in $(LIBX264)"
	(cd $(LIBX264) && chmod -f u+x configure)
	(cd $(LIBX264) && chmod -f u+x version.sh)
	(cd $(LIBX264) && chmod -f u+x config.guess)
	(cd $(LIBX264) && chmod -f u+x config.sub)
	(cd $(LIBX264) && ./configure --enable-pic --enable-static --disable-opencl )
	(cd $(LIBX264) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBX264)/libx264.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libx265 20150509 in $(LIBX265)"
	#(cd $(LIBX265)/build/linux && chmod -f u+x make-Makefiles.bash)
	#(cd $(LIBX265)/build/linux && bash make-Makefiles.bash )
	(cd $(LIBX265)/build/linux && cmake -G "Unix Makefiles" ../../source -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC )
	(cd $(LIBX265)/build/linux && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBX265)/build/linux/libx265.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libjpeg-turbo 1.4.0 in $(LIBJPEGTURBO)"
	(cd $(LIBJPEGTURBO) && chmod -f u+x configure)
	(cd $(LIBJPEGTURBO) && chmod -f u+x config.guess)
	(cd $(LIBJPEGTURBO) && chmod -f u+x config.sub)
	(cd $(LIBJPEGTURBO) && ./configure --enable-pic --enable-static --enable-shared=no )
	(cd $(LIBJPEGTURBO) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBJPEGTURBO)/.libs/libturbojpeg.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libGDCM 2.4.4 in $(LIBGDCM)"
	-mkdir -p $(LIBGDCMBIN)
	(cd $(LIBGDCMBIN) && cmake ../gdcm -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC -DGDCM_USE_OPENJPEG_V2=ON )
	(cd $(LIBGDCMBIN) && $(MAKE) $(MAKEFLAGS))
	(cp $(LIBGDCMBIN)/bin/*.a $(LIBS)/gdcm/)

	@echo
	@echo
	@echo "Building ffmpeg 2.6.3 in $(FFMPEG)"
	(cd $(FFMPEG)/ffmpeg && chmod -f u+x configure)
	(cd $(FFMPEG)/ffmpeg && chmod -f u+x version.sh)
	-mkdir -p $(FFMPEG)/ffmpeg-obj
	(cd $(FFMPEG)/ffmpeg-obj && ../ffmpeg/configure \
		--enable-static --disable-shared --enable-pic --enable-gray \
		--prefix=../ffmpeg-out \
		--extra-cflags="-fPIC -I../../libvpx -I../../libx264 -I../../libx265/source" \
		--extra-cxxflags="-fPIC -I../../libvpx -I../../libx264 -I../../libx265/source" \
		--extra-ldflags="-fPIC -L../../libvpx -L../../libx264 -L../../libx265/build/linux" \
		--enable-gpl --enable-version3 --enable-runtime-cpudetect --enable-pthreads --enable-swscale \
		--disable-ffserver --disable-ffplay --disable-network --disable-ffmpeg --disable-devices \
		--disable-frei0r --disable-libass --disable-libcelt --disable-libopencore-amrnb --disable-libopencore-amrwb \
		--disable-libfreetype --disable-libgsm --disable-libmp3lame --disable-libnut --disable-librtmp \
		--disable-libspeex --disable-libvorbis \
		--enable-bzlib --enable-zlib \
		--disable-libopenjpeg --disable-libschroedinger \
		--enable-libtheora --enable-libvpx --enable-libx264 --enable-encoder=libx264 --enable-libx265 --enable-libxvid \
		--disable-libvo-aacenc --disable-libvo-amrwbenc )

	(cd $(FFMPEG)/ffmpeg-obj && $(MAKE) $(MAKEFLAGS) all install)

	(cp -f $(FFMPEG)/ffmpeg-out/lib/libavcodec.a $(LIBS)/)
	(cp -f $(FFMPEG)/ffmpeg-out/lib/libavformat.a $(LIBS)/)
	(cp -f $(FFMPEG)/ffmpeg-out/lib/libavutil.a $(LIBS)/)
	(cp -f $(FFMPEG)/ffmpeg-out/lib/libswscale.a $(LIBS)/)
	-mkdir -p $(FFMPEG)/include
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libavcodec $(FFMPEG)/include/)
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libavformat $(FFMPEG)/include/)
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libavutil $(FFMPEG)/include/)
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libswscale $(FFMPEG)/include/)


	@echo
	@echo
	@echo "Building imgcnv"
	(cd src && $(MAKE) $(MAKEFLAGS))

	@echo
	@echo
	@echo "Building libimgcnv"
	(cd src_dylib && $(MAKE) $(MAKEFLAGS))

clean: qmake
	(cd src && $(MAKE) clean)
	rm -rf .generated *.o *~
	rm -rf $(LIBBIM)/.generated $(LIBBIM)/*.o $(LIBBIM)/*~ $(LIBBIM)/.qmake.stash
	(cd $(LIBBIM) && $(MAKE) clean)
#	(cd $(LIBVPX) && $(MAKE) clean)
	rm -rf $(LIBBIM)/Makefile src/Makefile src_dylib/Makefile



cleanfull: clean
	(cd $(LIBVPX) && $(MAKE) clean)
	(cd $(LIBX264) && $(MAKE) clean)
	rm -rf $(FFMPEG)/ffmpeg-obj
	rm -rf $(FFMPEG)/ffmpeg-out
#	(cd $(FFMPEG)/ffmpeg && $(MAKE) clean)

distclean: clean
	#rm -rf $(LIBS) $(LIBS_PKG) $(LIBCZI) $(LIBHDF)
	rm -f imgcnv
	rm -f libimgcnv.so*



.FORCE: imgcnv
